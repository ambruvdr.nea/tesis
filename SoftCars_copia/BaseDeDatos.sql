USE [master]
GO
/****** Object:  Database [BaseDeDatos]    Script Date: 03/08/2020 18:46:29 ******/
CREATE DATABASE [BaseDeDatos]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BaseDeDatos', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\BaseDeDatos.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BaseDeDatos_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\BaseDeDatos_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BaseDeDatos] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BaseDeDatos].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BaseDeDatos] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BaseDeDatos] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BaseDeDatos] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BaseDeDatos] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BaseDeDatos] SET ARITHABORT OFF 
GO
ALTER DATABASE [BaseDeDatos] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BaseDeDatos] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BaseDeDatos] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BaseDeDatos] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BaseDeDatos] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BaseDeDatos] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BaseDeDatos] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BaseDeDatos] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BaseDeDatos] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BaseDeDatos] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BaseDeDatos] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BaseDeDatos] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BaseDeDatos] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BaseDeDatos] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BaseDeDatos] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BaseDeDatos] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BaseDeDatos] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BaseDeDatos] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BaseDeDatos] SET  MULTI_USER 
GO
ALTER DATABASE [BaseDeDatos] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BaseDeDatos] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BaseDeDatos] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BaseDeDatos] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BaseDeDatos] SET DELAYED_DURABILITY = DISABLED 
GO
USE [BaseDeDatos]
GO
/****** Object:  Table [dbo].[articulo]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[articulo](
	[id_articulo] [int] IDENTITY(1,1) NOT NULL,
	[nombre_articulo] [varchar](50) NULL,
	[cantidad] [int] NULL,
	[precio] [float] NULL,
	[id_rubro] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_articulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cliente]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cliente](
	[IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[cuilCliente] [varchar](50) NULL,
	[nombreCliente] [varchar](100) NULL,
	[telCliente] [varchar](50) NULL,
	[mailCliente] [varchar](50) NULL,
	[direccionCliente] [varchar](50) NULL,
	[barrioCliente] [varchar](50) NULL,
	[codigoPostal] [varchar](50) NULL,
	[idLocalidad] [int] NULL,
	[idTipoFactura] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[detalle_venta]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[detalle_venta](
	[id_venta] [int] NOT NULL,
	[nombre] [varchar](50) NULL,
	[cantidad] [int] NULL,
	[precio] [float] NULL,
	[acumulado] [float] NULL,
	[descuento] [float] NULL,
	[pdescuento] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_venta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[localidad]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[localidad](
	[idLocalidad] [int] IDENTITY(1,1) NOT NULL,
	[nombreLocalidad] [varchar](50) NULL,
	[idProvincia] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idLocalidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[provincia]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[provincia](
	[idProvincia] [int] IDENTITY(1,1) NOT NULL,
	[nombreProvincia] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[idProvincia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rubro]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rubro](
	[id_rubro] [int] IDENTITY(1,1) NOT NULL,
	[nombre_rubro] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_rubro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tipo_usuario]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tipo_usuario](
	[id_tipo_usuario] [int] IDENTITY(1,1) NOT NULL,
	[tipo_usuario] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_tipo_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tipoCombustible]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tipoCombustible](
	[codCombustible] [int] IDENTITY(1,1) NOT NULL,
	[descripcionCombustible] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[codCombustible] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tipoFactura]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tipoFactura](
	[idTipoFactura] [int] IDENTITY(1,1) NOT NULL,
	[nombreTipo] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[idTipoFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tipoVehiculo]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tipoVehiculo](
	[codigoVehiculo] [int] IDENTITY(1,1) NOT NULL,
	[descripcionTipo] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[codigoVehiculo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuario](
	[id_usuario] [int] IDENTITY(1,1) NOT NULL,
	[nombre_usuario] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[id_tipo_usuario] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vehiculo]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vehiculo](
	[idVehiculo] [int] IDENTITY(1,1) NOT NULL,
	[dominioVehiculo] [varchar](50) NULL,
	[marcaVehiculo] [varchar](50) NULL,
	[modeloVehiculo] [varchar](50) NULL,
	[anioVehiculo] [varchar](50) NULL,
	[idCliente] [int] NULL,
	[codCombustible] [int] NULL,
	[codigoVehiculo] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idVehiculo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[venta]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[venta](
	[id_venta] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_venta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[articulo] ON 

INSERT [dbo].[articulo] ([id_articulo], [nombre_articulo], [cantidad], [precio], [id_rubro]) VALUES (1, N'Martillo', 10, 300, 1)
INSERT [dbo].[articulo] ([id_articulo], [nombre_articulo], [cantidad], [precio], [id_rubro]) VALUES (2, N'Goma', 4, 1200, 1)
INSERT [dbo].[articulo] ([id_articulo], [nombre_articulo], [cantidad], [precio], [id_rubro]) VALUES (3, N'Lapiz', 2, 20, 2)
SET IDENTITY_INSERT [dbo].[articulo] OFF
SET IDENTITY_INSERT [dbo].[cliente] ON 

INSERT [dbo].[cliente] ([IdCliente], [cuilCliente], [nombreCliente], [telCliente], [mailCliente], [direccionCliente], [barrioCliente], [codigoPostal], [idLocalidad], [idTipoFactura]) VALUES (1, N'20275955420', N'RAUL CASTRO', N'03521436196', N'RAUL@GMAIL.COM', N'JAURECHE 22', N'LA FERIA', N'5446', 1, 1)
INSERT [dbo].[cliente] ([IdCliente], [cuilCliente], [nombreCliente], [telCliente], [mailCliente], [direccionCliente], [barrioCliente], [codigoPostal], [idLocalidad], [idTipoFactura]) VALUES (2, N'20256452320', N'ENZO PEREZ', N'03521474196', N'ENZO@GMAIL.COM', N'SAN MARTIN 458', N'OBRA 25', N'5200', 1, 1)
INSERT [dbo].[cliente] ([IdCliente], [cuilCliente], [nombreCliente], [telCliente], [mailCliente], [direccionCliente], [barrioCliente], [codigoPostal], [idLocalidad], [idTipoFactura]) VALUES (3, N'20111859120', N'MARIANO HUENS', N'03521459496', N'MARIANO@GMAIL.COM', N'JOSE TROTA 741', N'CERROS NUEVOS', N'54653', 1, 3)
INSERT [dbo].[cliente] ([IdCliente], [cuilCliente], [nombreCliente], [telCliente], [mailCliente], [direccionCliente], [barrioCliente], [codigoPostal], [idLocalidad], [idTipoFactura]) VALUES (4, N'20295952920', N'PEDRO BAZAN', N'03521467796', N'PEDRO@GMAIL.COM', N'AGUERO 925', N'SAUCE', N'5200', 1, 1)
INSERT [dbo].[cliente] ([IdCliente], [cuilCliente], [nombreCliente], [telCliente], [mailCliente], [direccionCliente], [barrioCliente], [codigoPostal], [idLocalidad], [idTipoFactura]) VALUES (5, N'20166952380', N'JUAN PEREZ', N'03521458896', N'JUAN@GMAIL.COM', N'25 DE MAYO 895', N'OBRA SANITARIA', N'5200', 1, 1)
SET IDENTITY_INSERT [dbo].[cliente] OFF
INSERT [dbo].[detalle_venta] ([id_venta], [nombre], [cantidad], [precio], [acumulado], [descuento], [pdescuento]) VALUES (2, N'Lapiz', 2, 20, 40, 0, 0)
INSERT [dbo].[detalle_venta] ([id_venta], [nombre], [cantidad], [precio], [acumulado], [descuento], [pdescuento]) VALUES (5, N'Goma', 2, 1200, 2400, 0, 0)
SET IDENTITY_INSERT [dbo].[localidad] ON 

INSERT [dbo].[localidad] ([idLocalidad], [nombreLocalidad], [idProvincia]) VALUES (1, N'DEAN FUNES', 1)
INSERT [dbo].[localidad] ([idLocalidad], [nombreLocalidad], [idProvincia]) VALUES (2, N'VILLA MERCEDES', 2)
SET IDENTITY_INSERT [dbo].[localidad] OFF
SET IDENTITY_INSERT [dbo].[provincia] ON 

INSERT [dbo].[provincia] ([idProvincia], [nombreProvincia]) VALUES (1, N'CORDOBA')
INSERT [dbo].[provincia] ([idProvincia], [nombreProvincia]) VALUES (2, N'SAN LUIS')
SET IDENTITY_INSERT [dbo].[provincia] OFF
SET IDENTITY_INSERT [dbo].[rubro] ON 

INSERT [dbo].[rubro] ([id_rubro], [nombre_rubro]) VALUES (1, N'Repuestos')
INSERT [dbo].[rubro] ([id_rubro], [nombre_rubro]) VALUES (2, N'Servicios')
SET IDENTITY_INSERT [dbo].[rubro] OFF
SET IDENTITY_INSERT [dbo].[tipo_usuario] ON 

INSERT [dbo].[tipo_usuario] ([id_tipo_usuario], [tipo_usuario]) VALUES (1, N'Administrador')
INSERT [dbo].[tipo_usuario] ([id_tipo_usuario], [tipo_usuario]) VALUES (2, N'Recepcionista')
INSERT [dbo].[tipo_usuario] ([id_tipo_usuario], [tipo_usuario]) VALUES (3, N'Venta')
INSERT [dbo].[tipo_usuario] ([id_tipo_usuario], [tipo_usuario]) VALUES (4, N'Compra')
SET IDENTITY_INSERT [dbo].[tipo_usuario] OFF
SET IDENTITY_INSERT [dbo].[tipoCombustible] ON 

INSERT [dbo].[tipoCombustible] ([codCombustible], [descripcionCombustible]) VALUES (1, N'NAFTA')
INSERT [dbo].[tipoCombustible] ([codCombustible], [descripcionCombustible]) VALUES (2, N'GASOIL')
INSERT [dbo].[tipoCombustible] ([codCombustible], [descripcionCombustible]) VALUES (3, N'ELECTRICO')
INSERT [dbo].[tipoCombustible] ([codCombustible], [descripcionCombustible]) VALUES (4, N'GNC')
SET IDENTITY_INSERT [dbo].[tipoCombustible] OFF
SET IDENTITY_INSERT [dbo].[tipoFactura] ON 

INSERT [dbo].[tipoFactura] ([idTipoFactura], [nombreTipo]) VALUES (1, N'A')
INSERT [dbo].[tipoFactura] ([idTipoFactura], [nombreTipo]) VALUES (2, N'B')
INSERT [dbo].[tipoFactura] ([idTipoFactura], [nombreTipo]) VALUES (3, N'C')
SET IDENTITY_INSERT [dbo].[tipoFactura] OFF
SET IDENTITY_INSERT [dbo].[tipoVehiculo] ON 

INSERT [dbo].[tipoVehiculo] ([codigoVehiculo], [descripcionTipo]) VALUES (1, N'PARTICULAR')
INSERT [dbo].[tipoVehiculo] ([codigoVehiculo], [descripcionTipo]) VALUES (2, N'PICKUP')
INSERT [dbo].[tipoVehiculo] ([codigoVehiculo], [descripcionTipo]) VALUES (3, N'MOTO')
INSERT [dbo].[tipoVehiculo] ([codigoVehiculo], [descripcionTipo]) VALUES (4, N'OTRO')
SET IDENTITY_INSERT [dbo].[tipoVehiculo] OFF
SET IDENTITY_INSERT [dbo].[usuario] ON 

INSERT [dbo].[usuario] ([id_usuario], [nombre_usuario], [password], [id_tipo_usuario]) VALUES (1, N'Matias', N'123', 1)
INSERT [dbo].[usuario] ([id_usuario], [nombre_usuario], [password], [id_tipo_usuario]) VALUES (2, N'Juan', N'123', 2)
INSERT [dbo].[usuario] ([id_usuario], [nombre_usuario], [password], [id_tipo_usuario]) VALUES (4, N'Romero', N'123', 4)
INSERT [dbo].[usuario] ([id_usuario], [nombre_usuario], [password], [id_tipo_usuario]) VALUES (2002, N'uuuuuuuuuu', N'uuuuuuuuuuuu', 1)
INSERT [dbo].[usuario] ([id_usuario], [nombre_usuario], [password], [id_tipo_usuario]) VALUES (2003, N'Miguel', N'123', 3)
INSERT [dbo].[usuario] ([id_usuario], [nombre_usuario], [password], [id_tipo_usuario]) VALUES (2004, N'Pepe', N'789', 4)
SET IDENTITY_INSERT [dbo].[usuario] OFF
SET IDENTITY_INSERT [dbo].[vehiculo] ON 

INSERT [dbo].[vehiculo] ([idVehiculo], [dominioVehiculo], [marcaVehiculo], [modeloVehiculo], [anioVehiculo], [idCliente], [codCombustible], [codigoVehiculo]) VALUES (1, N'SDJ843', N'FORD', N'KA', N'2010', 1, 1, 1)
INSERT [dbo].[vehiculo] ([idVehiculo], [dominioVehiculo], [marcaVehiculo], [modeloVehiculo], [anioVehiculo], [idCliente], [codCombustible], [codigoVehiculo]) VALUES (2, N'GFF260', N'VOLKSWAGEN', N'GOL', N'2015', 3, 1, 1)
INSERT [dbo].[vehiculo] ([idVehiculo], [dominioVehiculo], [marcaVehiculo], [modeloVehiculo], [anioVehiculo], [idCliente], [codCombustible], [codigoVehiculo]) VALUES (3, N'HRG123', N'FIAT', N'UNO', N'1999', 4, 1, 1)
INSERT [dbo].[vehiculo] ([idVehiculo], [dominioVehiculo], [marcaVehiculo], [modeloVehiculo], [anioVehiculo], [idCliente], [codCombustible], [codigoVehiculo]) VALUES (4, N'LGE151', N'CHEVROLET', N'CORSA', N'2008', 4, 1, 1)
SET IDENTITY_INSERT [dbo].[vehiculo] OFF
SET IDENTITY_INSERT [dbo].[venta] ON 

INSERT [dbo].[venta] ([id_venta], [fecha]) VALUES (1, CAST(N'2020-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[venta] ([id_venta], [fecha]) VALUES (4, CAST(N'2020-05-23 01:37:56.390' AS DateTime))
INSERT [dbo].[venta] ([id_venta], [fecha]) VALUES (5, CAST(N'2020-05-23 01:40:50.730' AS DateTime))
SET IDENTITY_INSERT [dbo].[venta] OFF
/****** Object:  StoredProcedure [dbo].[ClientePorNombre]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ClientePorNombre]
	@cliente varchar(50)
AS
	SELECT c.*, v.*
	FROM cliente as c 
	left join vehiculo as v
	on c.nombreCliente = @cliente

	RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[ConsultaPass]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ConsultaPass]
	@verificado varchar(50)
AS
	SELECT password
	FROM usuario
	WHERE password = @verificado

RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[ListaArticulo1]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListaArticulo1]
	@art1 varchar(50)
AS
	SELECT id_articulo, nombre_articulo, cantidad, precio, id_rubro
	FROM articulo
	WHERE nombre_articulo = @art1

RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[ListaArticulo2]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListaArticulo2]
	@art2 varchar(50)
AS
	SELECT id_articulo, nombre_articulo, precio
	FROM articulo
	WHERE nombre_articulo = @art2

RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[listadoPorLocalidad]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listadoPorLocalidad]
	@idLocalidad int
AS
 SELECT l.nombreLocalidad,
        c.cuilCliente,
		c.nombreCliente,
		c.codigoPostal,
		c.barrioCliente
   FROM cliente c, localidad l
   WHERE c.idLocalidad = l.idLocalidad 
     and c.idLocalidad = @idLocalidad;
GO
/****** Object:  StoredProcedure [dbo].[listadoPorNombreCliente]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listadoPorNombreCliente]
	@idNombreCliente varchar(50)
AS
 SELECT c.IdCliente,
        c.cuilCliente,
		c.nombreCliente,
		c.codigoPostal,
		c.barrioCliente,
		c.direccionCliente,
		c.telCliente,
		c.mailCliente
   FROM cliente c
   WHERE c.nombreCliente = @idNombreCliente;
GO
/****** Object:  StoredProcedure [dbo].[listadoPorPrueba]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listadoPorPrueba]
	@listadoPr int
AS
 SELECT v.idVehiculo,
        v.dominioVehiculo,
		v.marcaVehiculo,
		v.modeloVehiculo,
		v.idVehiculo,
        c.cuilCliente,
		c.nombreCliente,
		c.codigoPostal,
		c.barrioCliente,
		t.nombreTipo
   FROM cliente c, vehiculo v, tipoFactura t
   WHERE c.IdCliente = v.idCliente 
   and c.idTipoFactura = t.idTipoFactura
     and c.idTipoFactura = @listadoPr;
GO
/****** Object:  StoredProcedure [dbo].[listadoPorTipoFactura]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listadoPorTipoFactura]
	@idTipoFactura int
AS
 SELECT t.nombreTipo,
        c.cuilCliente,
		c.nombreCliente,
		c.codigoPostal,
		c.barrioCliente
   FROM cliente c, tipoFactura t
   WHERE c.idTipoFactura = t.idTipoFactura 
     and c.idTipoFactura = @idTipoFactura;
GO
/****** Object:  StoredProcedure [dbo].[ListaUsuario1]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListaUsuario1]
	@lis1 varchar(50)
AS
	SELECT id_usuario, nombre_usuario, password, id_tipo_usuario
	FROM usuario
	WHERE nombre_usuario = @lis1

	RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[PruebaDeConsulta]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PruebaDeConsulta]
	@prueba varchar(50)
AS
	SELECT c.IdCliente as 'ID CLIENTE', c.cuilCliente as 'CUIL-CUIT', c.nombreCliente as 'NOMBRE', v.idVehiculo as 'ID VEHICULO', v.dominioVehiculo as 'DOMINIO', v.marcaVehiculo as 'MARCA'
	FROM vehiculo v, cliente c
	WHERE v.idCliente = c.IdCliente
	and c.nombreCliente = @prueba

RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[PruebaDeConsulta2]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PruebaDeConsulta2]
	@prueba2 varchar(50)
AS
	SELECT c.IdCliente as 'ID CLIENTE', c.cuilCliente as 'CUIL-CUIT', c.nombreCliente as 'NOMBRE', v.idVehiculo as 'ID VEHICULO', v.dominioVehiculo as 'DOMINIO', v.marcaVehiculo as 'MARCA'
	FROM vehiculo v, cliente c
	WHERE v.idCliente = c.IdCliente
	and c.cuilCliente = @prueba2

RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[TomarTipoUs]    Script Date: 03/08/2020 18:46:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TomarTipoUs]
	@tomar int
AS
	SELECT id_tipo_usuario
	FROM usuario
	WHERE id_tipo_usuario = @tomar

RETURN 0
GO
USE [master]
GO
ALTER DATABASE [BaseDeDatos] SET  READ_WRITE 
GO
