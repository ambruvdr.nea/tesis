﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class modificarCliente : Form
    {
        public modificarCliente()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private SqlDataAdapter adaptador2;
        private DataSet datos;
        private DataTable datosTable;

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void modificarCliente_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.provincia' Puede moverla o quitarla según sea necesario.
            this.provinciaTableAdapter.Fill(this.baseDeDatosDataSet10.provincia);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.tipoFactura' Puede moverla o quitarla según sea necesario.
            this.tipoFacturaTableAdapter.Fill(this.baseDeDatosDataSet10.tipoFactura);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.localidad' Puede moverla o quitarla según sea necesario.
            this.localidadTableAdapter.Fill(this.baseDeDatosDataSet10.localidad);

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();


            try
            {
                conexion.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM cliente WHERE IdCliente='" + ConsultarCliente.idClienteModificar + "'", conexion);
                SqlDataReader dr = cmd.ExecuteReader();
                

                if (dr.Read())
                {

                    string col1Value = dr[1].ToString();
                    textBox1.Text = col1Value;
                    string col2Value = dr[2].ToString();
                    textBox2.Text = col2Value;

                    string col3Value = dr[3].ToString();
                    string parte1 = col3Value.Split('-')[0];
                    string parte2 = col3Value.Split('-')[1];
                    textBox8.Text = parte1;
                    textBox3.Text = parte2;

                    string col4Value = dr[4].ToString();
                    textBox5.Text = col4Value;
                    string col5Value = dr[5].ToString();
                    textBox6.Text = col5Value;
                    string col6Value = dr[6].ToString();
                    textBox7.Text = col6Value;
                    string col7Value = dr[7].ToString();
                    textBox4.Text = col7Value;
                    string col8Value = dr[8].ToString();
                    comboBox1.SelectedValue = col8Value;
                    string col9Value = dr[9].ToString();
                    comboBox2.SelectedValue = col9Value;
                }
                else
                {
                    MessageBox.Show("", "ERROR");
                }
            }
            catch
            {
                MessageBox.Show("No","ERROR");
            }
            finally
            {
                conexion.Close();
            }



            adaptador2 = new SqlDataAdapter();
            try
            {

                conexion.Open();
                SqlCommand cmd2 = new SqlCommand("SELECT c.idLocalidad, l.idLocalidad, l.idProvincia, p.idProvincia FROM cliente c, localidad l, provincia p WHERE c.idLocalidad = l.idLocalidad and l.idProvincia = p.idProvincia and IdCliente='" + ConsultarCliente.idClienteModificar + "'", conexion);
                SqlDataReader dr2 = cmd2.ExecuteReader();

                if (dr2.Read())
                {
                    string col3Value = dr2[3].ToString();
                    //label15.Text = col3Value;
                    int valorParaComboProv = int.Parse(col3Value);
                    comboBox3.SelectedValue = valorParaComboProv;
                }

            }
            catch
            {
                MessageBox.Show("Error en la consulta(numero2) a la base de datos", "ERROR");
            }
            finally
            {
                conexion.Close();
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (textBox1.Text != "" & textBox2.Text != "")
            {


                SqlCommand modificacion = new SqlCommand("update cliente set cuilCliente = @cuilCliente, nombreCliente = @nombreCliente, telCliente = @telCliente, mailCliente = @mailCliente, direccionCliente = @direccionCliente, barrioCliente = @barrioCliente, codigoPostal = @codigoPostal, idLocalidad = @idLocalidad, idTipoFactura = @idTipoFactura where IdCliente = @id_a", conexion);

                adaptador.UpdateCommand = modificacion;
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@cuilCliente", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@nombreCliente", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@telCliente", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@mailCliente", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@direccionCliente", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@barrioCliente", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@codigoPostal", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@idLocalidad", SqlDbType.Int));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@idTipoFactura", SqlDbType.Int));

                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@id_a", SqlDbType.Int));


                adaptador.UpdateCommand.Parameters["@id_a"].Value = ConsultarCliente.idClienteModificar;
                adaptador.UpdateCommand.Parameters["@cuilCliente"].Value = textBox1.Text;
                adaptador.UpdateCommand.Parameters["@nombreCliente"].Value = textBox2.Text;
                adaptador.UpdateCommand.Parameters["@telCliente"].Value = textBox3.Text;
                adaptador.UpdateCommand.Parameters["@mailCliente"].Value = textBox5.Text;
                adaptador.UpdateCommand.Parameters["@direccionCliente"].Value = textBox6.Text;
                adaptador.UpdateCommand.Parameters["@barrioCliente"].Value = textBox7.Text;
                adaptador.UpdateCommand.Parameters["@codigoPostal"].Value = textBox4.Text;
                adaptador.UpdateCommand.Parameters["@idLocalidad"].Value = comboBox1.SelectedValue;
                adaptador.UpdateCommand.Parameters["@idTipoFactura"].Value = comboBox2.SelectedValue;


                
                SqlCommand modificacion2 = new SqlCommand("update localidad set idProvincia = @idProvincia where idLocalidad = @id_a", conexion);

                adaptador2.UpdateCommand = modificacion2;
                adaptador2.UpdateCommand.Parameters.Add(new SqlParameter("@idProvincia", SqlDbType.Int));
                adaptador2.UpdateCommand.Parameters.Add(new SqlParameter("@id_a", SqlDbType.Int));

                adaptador2.UpdateCommand.Parameters["@id_a"].Value = comboBox1.SelectedValue;
                adaptador2.UpdateCommand.Parameters["@idProvincia"].Value = comboBox3.SelectedValue;
                

                try
                {
                    conexion.Open();
                    adaptador.UpdateCommand.ExecuteNonQuery();

                    adaptador2.UpdateCommand.ExecuteNonQuery();

                    MessageBox.Show("Se ha modificado el cliente exitosamente.","Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }
                finally
                {
                    conexion.Close();
                }
            }
            else
            {
                MessageBox.Show("Debe completar todos los datos obligatorios.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form aLocalidad = new AgregarLocalidad();
            aLocalidad.Show();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }
    }
}
