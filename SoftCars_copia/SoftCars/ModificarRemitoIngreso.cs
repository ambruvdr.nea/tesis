﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class ModificarRemitoIngreso : Form
    {
        public ModificarRemitoIngreso()
        {
            InitializeComponent();
        }
        private SqlConnection conexion;
        private SqlDataAdapter adaptador, adaptador2, adaptador3, adaptador4;
        private DataSet datos;
        private DataTable datosTable, datosTable2;
        DataTable dt2 = new DataTable();
        int bandera = 0;

        private void ModificarRemitoIngreso_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            adaptador2 = new SqlDataAdapter();
            adaptador3 = new SqlDataAdapter();
            adaptador4 = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;
            dataGridView2.AllowUserToAddRows = false;

            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT r.idRemitoIngreso," +
                " r.numeroRemitoIngreso," +
                " r.fechaRemitoIngreso," +
                " r.idProveedor," +
                " p.cuilProveedor," +
                " p.razonSocialProveedor," +
                " t.nombreTipo" +
                " FROM remito_ingreso r, detalle_remito_ingreso dr, proveedor p, articulo a, tipoFactura t" +
                " WHERE r.idProveedor = p.idProveedor" +
                " and r.idRemitoIngreso = '"+VerRemitoIngreso.idVerRemitoIngresoGrid+ "'" +
                " and p.idTipoFactura = t.idTipoFactura", conexion);

            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                //numeroRemitoIngreso
                string col1Value = dr[0].ToString();
                textBox1.Text = col1Value;
                //numeroRemitoIngreso
                string col7Value = dr[1].ToString();
                string parte1 = col7Value.Split('-')[0];
                string parte2 = col7Value.Split('-')[1];
                textBox2.Text = parte1;
                textBox15.Text = parte2;
                //Fecha
                DateTime col2Value = DateTime.Parse(dr[2].ToString());
                dateTimePicker1.Value = col2Value;
                //idProveedor
                string col3Value = dr[3].ToString();
                label2.Text = col3Value;
                //cuil
                string col4Value = dr[4].ToString();
                textBox8.Text = col4Value;
                //razonSocialProveedor
                string col5Value = dr[5].ToString();
                textBox13.Text = col5Value;
                //nombreTipo
                string col6Value = dr[6].ToString();
                textBox14.Text = col6Value;
            }
            conexion.Close();

            adaptador.SelectCommand = new SqlCommand("ConsultarDetalleRemito", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@idRemito", SqlDbType.VarChar);

            datosTable2 = new DataTable();
            adaptador.SelectCommand.Parameters["@idRemito"].Value = VerRemitoIngreso.idVerRemitoIngresoGrid;
            adaptador.Fill(datosTable2);
            dataGridView1.DataSource = datosTable2;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == string.Empty)
            {
                textBox5.BackColor = Color.IndianRed;
                label3.ForeColor = Color.IndianRed;
                //Validacion.SetHighlightColor(textBox5, DevComponents.DotNetBar.Validator.eHighlightColor.Red);
            }
            else
            {
                int id = int.Parse(label6.Text);
                String articulo = textBox3.Text;
                int cantidad = int.Parse(textBox5.Text);
                DataRow row = datosTable2.NewRow();
                row[0] = id;
                row[1] = articulo;
                row[2] = cantidad;

                datosTable2.Rows.Add(row);
                dataGridView1.DataSource = datosTable2;

                textBox3.Text = "";
                textBox5.Text = "";

                textBox5.Enabled = false;
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (textBox3.Text != "")
            {
                textBox5.Enabled = true;
            }
            textBox5.Enabled = true;
            if (dataGridView2.CurrentRow.Index != -1)
            {
                label6.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
                textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (textBox3.Text != "")
            {
                textBox5.Enabled = true;
            }
            if (dataGridView2.CurrentRow.Index != -1)
            {
                label6.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
                textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();

            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            BuscarProveedorModificarRemito c = new BuscarProveedorModificarRemito();
            c.pasado9 += new BuscarProveedorModificarRemito.pasar9(ejecutar9);
            c.Show();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (textBox5.BackColor == Color.IndianRed)
            {
                textBox5.BackColor = Color.White;
                label3.ForeColor = Color.White;
            }
        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {
            if (textBox15.BackColor == Color.IndianRed)
            {
                textBox15.BackColor = Color.White;
                label16.ForeColor = Color.White;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.BackColor == Color.IndianRed)
            {
                textBox2.BackColor = Color.White;
                label16.ForeColor = Color.White;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
            textBox2.BackColor = Color.White;
            textBox15.BackColor = Color.White;
            label16.ForeColor = Color.White;
        }

        private void textBox15_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
            textBox2.BackColor = Color.White;
            textBox15.BackColor = Color.White;
            label16.ForeColor = Color.White;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        public void ejecutar9(string dato, string dato2, string dato3, string dato4, string dato5)
        {
            //nombre
            textBox13.Text = dato3;
            //cuit
            textBox8.Text = dato2;
            //tipoFactura
            textBox14.Text = dato4;

            //idProveedor
            label2.Text = dato;

            //idTipoFactura
            label9.Text = dato5;



        }

        private void button4_Click(object sender, EventArgs e)
        {
            adaptador.SelectCommand = new SqlCommand("ListaArticulo2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art2", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art2"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView2.DataSource = datosTable;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == string.Empty || textBox15.Text == string.Empty)
            {
                textBox2.BackColor = Color.IndianRed;
                textBox15.BackColor = Color.IndianRed;
                label16.ForeColor = Color.IndianRed;
                //Validacion.SetHighlightColor(textBox2, DevComponents.DotNetBar.Validator.eHighlightColor.Red);
                //Validacion.SetHighlightColor(textBox15, DevComponents.DotNetBar.Validator.eHighlightColor.Red);
            }
            else if (textBox8.Text == string.Empty || textBox13.Text == string.Empty || textBox14.Text == string.Empty)
            {
                textBox8.BackColor = Color.IndianRed;
                textBox13.BackColor = Color.IndianRed;
                textBox14.BackColor = Color.IndianRed;
                label13.ForeColor = Color.IndianRed;
                label14.ForeColor = Color.IndianRed;
                label15.ForeColor = Color.IndianRed;
            }
            else
            {
                conexion.Open();
                SqlCommand baja = new SqlCommand("DELETE " +
                    " FROM detalle_remito_ingreso" +
                    " WHERE idRemitoIngreso = '" + VerRemitoIngreso.idVerRemitoIngresoGrid + "'", conexion);
                adaptador.DeleteCommand = baja;
                adaptador.DeleteCommand.ExecuteNonQuery();
                conexion.Close();

                SqlCommand modificacion = new SqlCommand("update remito_ingreso set" +
                    " numeroRemitoIngreso = @numeroRemitoIngreso," +
                    " fechaRemitoIngreso = @fechaRemitoIngreso," +
                    " idProveedor = idProveedor" +
                    " where idRemitoIngreso = @id_a", conexion);

                adaptador2.UpdateCommand = modificacion;
                adaptador2.UpdateCommand.Parameters.Add(new SqlParameter("@id_a", SqlDbType.Int));
                adaptador2.UpdateCommand.Parameters.Add(new SqlParameter("@numeroRemitoIngreso", SqlDbType.VarChar));
                adaptador2.UpdateCommand.Parameters.Add(new SqlParameter("@fechaRemitoIngreso", SqlDbType.DateTime));
                adaptador2.UpdateCommand.Parameters.Add(new SqlParameter("@idProveedor", SqlDbType.Int));

                adaptador2.UpdateCommand.Parameters["@id_a"].Value = int.Parse(textBox1.Text);
                adaptador2.UpdateCommand.Parameters["@numeroRemitoIngreso"].Value = textBox15.Text;
                adaptador2.UpdateCommand.Parameters["@fechaRemitoIngreso"].Value = dateTimePicker1.Value;
                adaptador2.UpdateCommand.Parameters["@idProveedor"].Value = label2.Text;
                conexion.Open();
                adaptador2.UpdateCommand.ExecuteNonQuery();
                conexion.Close();

                SqlCommand alta2 = new SqlCommand("insert into detalle_remito_ingreso values (@idRemitoIngreso, @idArticulo, @nombre, @cantidad)", conexion);

                adaptador3.InsertCommand = alta2;
                adaptador3.InsertCommand.Parameters.Add(new SqlParameter("@idRemitoIngreso", SqlDbType.Int));
                adaptador3.InsertCommand.Parameters.Add(new SqlParameter("@idArticulo", SqlDbType.Int));
                adaptador3.InsertCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                adaptador3.InsertCommand.Parameters.Add(new SqlParameter("@cantidad", SqlDbType.Int));




                try
                {
                    conexion.Open();

                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {

                        float acumulado = 0;

                        alta2.Parameters.Clear();

                        alta2.Parameters.AddWithValue("@idRemitoIngreso", Convert.ToInt32(textBox1.Text));
                        alta2.Parameters.AddWithValue("@idArticulo", Convert.ToString(row.Cells[0].Value));
                        alta2.Parameters.AddWithValue("@nombre", Convert.ToString(row.Cells["articulo"].Value));
                        alta2.Parameters.AddWithValue("@cantidad", Convert.ToInt32(row.Cells["cantidad"].Value));

                        alta2.ExecuteNonQuery();

                        //TRAIGO LA CANTIDAD QUE HAY EN STOCK DE UN ARTICULO
                        var cant = new SqlCommand("select cantidad from articulo where nombre_articulo ='" + row.Cells["articulo"].Value + "'", conexion);

                        var dat2 = new SqlDataAdapter(cant);

                        dat2.Fill(dt2);

                        int cantidad = (int)dt2.Rows[0][0];
                        int cantNueva = (int)row.Cells["cantidad"].Value;

                        // SUMO LA CANTIDAD DE STOCK MAS LO QUE INGRESÓ EN EL REMITO
                        int cantidadArticuloSum = cantidad + cantNueva;

                        //ACTUALIZO EN LA BASE DE DATOS
                        SqlCommand modificacion2 = new SqlCommand("update articulo set cantidad = @cantidad where nombre_articulo = @nombre", conexion);

                        adaptador4.UpdateCommand = modificacion2;
                        adaptador4.UpdateCommand.Parameters.Add(new SqlParameter("@cantidad", SqlDbType.Int));
                        adaptador4.UpdateCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));

                        adaptador4.UpdateCommand.Parameters["@cantidad"].Value = cantidadArticuloSum;
                        adaptador4.UpdateCommand.Parameters["@nombre"].Value = row.Cells["articulo"].Value;

                        modificacion2.ExecuteNonQuery();

                        modificacion2.Parameters.Clear();

                    }


                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }
                finally
                {
                    MessageBox.Show("Remito Modificado Correctamente", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    dataGridView1.Columns.Clear();
                    dataGridView2.Columns.Clear();
                    //Borro los TextBox
                    textBox1.Text = "";

                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                    textBox5.Text = "";
                    textBox8.Text = "";
                    textBox13.Text = "";
                    textBox14.Text = "";
                    textBox15.Text = "";

                    //Desabilito todo
                    dateTimePicker1.Enabled = false;

                    textBox2.Enabled = false;
                    textBox3.Enabled = false;
                    textBox4.Enabled = false;
                    textBox5.Enabled = false;
                    textBox15.Enabled = false;

                    dataGridView1.Enabled = false;
                    dataGridView2.Enabled = false;

                    button1.Enabled = false;
                    button2.Enabled = false;
                    button3.Enabled = false;
                    button4.Enabled = false;
                    button12.Enabled = false;


                    label3.Enabled = false;
                    label4.Enabled = false;

                    conexion.Close();
                    this.Dispose();
                    //Form f2 = new EncargadoDeCompra();
                    //f2.Show();

                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
            }
        }
    }
}
