﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class ConsultarClienteFactura : Form
    {
        public delegate void pasar(string dato, string dato2, string dato3, string dato4, string dato5);
        public event pasar pasado;

        //public delegate void pasar2(string dato2);
        //public event pasar2 pasado2;
        public ConsultarClienteFactura()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        //String valorRadio;
        public static int idClienteAgregar;
        //private string idClienteAgregarStr;

        private void button4_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                adaptador.SelectCommand = new SqlCommand("PruebaDeConsultaFactura", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@pruebaF", SqlDbType.VarChar);

                datosTable = new DataTable();
                adaptador.SelectCommand.Parameters["@pruebaF"].Value = textBox4.Text;
                adaptador.Fill(datosTable);
                dataGridView1.DataSource = datosTable;

                DataGridViewColumn column = dataGridView1.Columns[0];
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column1 = dataGridView1.Columns[1];
                column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column2 = dataGridView1.Columns[2];
                column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column5 = dataGridView1.Columns[5];
                //column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column6 = dataGridView1.Columns[6];
                column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            }
            else
            {
                adaptador.SelectCommand = new SqlCommand("PruebaDeConsulta2", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@prueba2", SqlDbType.VarChar);

                datosTable = new DataTable();
                adaptador.SelectCommand.Parameters["@prueba2"].Value = textBox4.Text;
                adaptador.Fill(datosTable);
                dataGridView1.DataSource = datosTable;
            }        
            
        }

        private void actualizarDatos()
        {
            datos.Clear();
            adaptador.Fill(datos, "cliente");
        }

        private void ConsultarCliente_Load(object sender, EventArgs e)
        {

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string variable = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string variable2 = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            string variable3 = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            string variable4 = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            string variable5 = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            textBox1.Text = variable;
            textBox2.Text = variable2;
            textBox3.Text = variable3;
            textBox5.Text = variable4;
            textBox6.Text = variable5;
            idClienteAgregar = int.Parse(variable);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            pasado(textBox1.Text, textBox2.Text, textBox3.Text, textBox5.Text, textBox6.Text);
            this.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string variable = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string variable2 = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            string variable3 = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            string variable4 = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            string variable5 = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            textBox1.Text = variable;
            textBox2.Text = variable2;
            textBox3.Text = variable3;
            textBox5.Text = variable4;
            textBox6.Text = variable5;
            idClienteAgregar = int.Parse(variable);
        }
    }
}
