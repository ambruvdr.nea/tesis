﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace SoftCars
{
    public partial class ModificarProveedor : Form
    {
        public ModificarProveedor()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private SqlDataAdapter adaptador2;
        private DataSet datos;
        private DataTable datosTable;

        private void ModificarProveedor_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet35.tipoFactura' Puede moverla o quitarla según sea necesario.
            // this.tipoFacturaTableAdapter1.Fill(this.baseDeDatosDataSet35.tipoFactura);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet34.localidad' Puede moverla o quitarla según sea necesario.
            // this.localidadTableAdapter1.Fill(this.baseDeDatosDataSet34.localidad);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet33.provincia' Puede moverla o quitarla según sea necesario.
            // this.provinciaTableAdapter1.Fill(this.baseDeDatosDataSet33.provincia);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.tipoFactura' Puede moverla o quitarla según sea necesario.
            //this.tipoFacturaTableAdapter.Fill(this.baseDeDatosDataSet10.tipoFactura);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.localidad' Puede moverla o quitarla según sea necesario.
            //this.localidadTableAdapter.Fill(this.baseDeDatosDataSet10.localidad);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.provincia' Puede moverla o quitarla según sea necesario.
            //this.provinciaTableAdapter.Fill(this.baseDeDatosDataSet10.provincia);
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();

            try
            {
                conexion.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM proveedor WHERE idProveedor='" + ConsultarProveedor.idProveedorModificar + "'", conexion);
                SqlDataReader dr = cmd.ExecuteReader();


                if (dr.Read())
                {
                    
                    //Razon Social
                    string col1Value = dr[1].ToString();
                    textBox2.Text = col1Value;
                    //Cuil
                    string col2Value = dr[2].ToString();
                    textBox1.Text = col2Value;
                    //Telefono
                    string col3Value = dr[3].ToString();
                    string parte1 = col3Value.Split('-')[0];
                    string parte2 = col3Value.Split('-')[1];
                    textBox8.Text = parte1;
                    textBox3.Text = parte2;
                    //Mail
                    string col4Value = dr[4].ToString();
                    textBox5.Text = col4Value;
                    //Direccion
                    string col5Value = dr[5].ToString();
                    textBox6.Text = col5Value;
                    //Localidad
                    string col6Value = dr[6].ToString();
                    comboBox1.DisplayMember = "nombreLocalidad";
                    comboBox1.ValueMember = col6Value;
                    //Tipo Factura
                    string col7Value = dr[7].ToString();
                    comboBox1.DisplayMember = "nombreTipo";
                    comboBox1.ValueMember = col7Value;
                    //Codigo Postal
                    string col8Value = dr[8].ToString();
                    textBox4.Text = col8Value;
                    //Provincia
                    string col9Value = dr[9].ToString();
                    comboBox1.DisplayMember = col9Value;
                    comboBox1.ValueMember = "idProvincia";

                    //Barrio
                    string col10Value = dr[10].ToString();
                    textBox7.Text = col10Value;

                }
                else
                {
                    MessageBox.Show("", "ERROR");
                }
            }
            catch
            {
                MessageBox.Show("No", "ERROR");
            }
            finally
            {
                conexion.Close();
            }


            /*
            adaptador2 = new SqlDataAdapter();
            try
            {

                conexion.Open();
                SqlCommand cmd2 = new SqlCommand("SELECT proveedor.idLocalidad," +
                                                " l.idLocalidad," +
                                                " l.idProvincia," +
                                                " p.idProvincia" +
                                                " FROM proveedor as proveedor," +
                                                " localidad l," +
                                                " provincia p" +
                                                " WHERE c.idLocalidad = l.idLocalidad " +
                                                "and l.idProvincia = p.idProvincia " +
                                                "and proveedor.idProveedor='" + ConsultarProveedor.idProveedorModificar + "'", conexion);
                SqlDataReader dr2 = cmd2.ExecuteReader();

                if (dr2.Read())
                {
                    string col3Value = dr2[3].ToString();
                    //label15.Text = col3Value;
                    int valorParaComboProv = int.Parse(col3Value);
                    comboBox3.SelectedValue = valorParaComboProv;
                }

            }
            catch
            {
                MessageBox.Show("Error en la consulta(numero2) a la base de datos", "ERROR");
            }
            finally
            {
                conexion.Close();
            }
            */
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" & textBox2.Text != "")
            {
                SqlCommand modificacion = new SqlCommand("update proveedor set" +     
                    " razonSocialProveedor = @nombreProveedor," +
                    " cuilProveedor = @cuilProveedor," +
                    " telProveedor = @telProveedor," +
                    " mailProveedor = @mailProveedor," +
                    " direccionProveedor = @direccionProveedor," +
                    " idLocalidad = @idLocalidad," +
                    " idTipoFactura = @idTipoFactura," +
                    " codigoPostal = @codigoPostal," +
                    " provinciaProveedor = @idProvincia," +
                    " barrioProveedor = @barrioProveedor" +
                    " where idProveedor = @id_a", conexion);

                adaptador.UpdateCommand = modificacion;
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@cuilProveedor", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@nombreProveedor", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@telProveedor", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@mailProveedor", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@direccionProveedor", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@idLocalidad", SqlDbType.Int));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@idTipoFactura", SqlDbType.Int));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@codigoPostal", SqlDbType.VarChar));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@idProvincia", SqlDbType.Int));
                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@barrioProveedor", SqlDbType.VarChar));

                adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@id_a", SqlDbType.Int));


                adaptador.UpdateCommand.Parameters["@id_a"].Value = ConsultarProveedor.idProveedorModificar;
                adaptador.UpdateCommand.Parameters["@cuilProveedor"].Value = textBox1.Text;
                adaptador.UpdateCommand.Parameters["@nombreProveedor"].Value = textBox2.Text;
                adaptador.UpdateCommand.Parameters["@telProveedor"].Value = textBox3.Text+"-"+textBox8.Text;
                adaptador.UpdateCommand.Parameters["@mailProveedor"].Value = textBox5.Text;
                adaptador.UpdateCommand.Parameters["@direccionProveedor"].Value = textBox6.Text;
                adaptador.UpdateCommand.Parameters["@barrioProveedor"].Value = textBox7.Text;
                adaptador.UpdateCommand.Parameters["@codigoPostal"].Value = textBox4.Text;
                adaptador.UpdateCommand.Parameters["@idLocalidad"].Value = comboBox1.SelectedValue;
                adaptador.UpdateCommand.Parameters["@idProvincia"].Value = comboBox3.SelectedValue;
                adaptador.UpdateCommand.Parameters["@idTipoFactura"].Value = comboBox2.SelectedValue;

                try
                {
                    conexion.Open();
                    adaptador.UpdateCommand.ExecuteNonQuery();
                    MessageBox.Show("El Proveedor se ha modificado con éxito");
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                    textBox5.Text = "";
                    textBox6.Text = "";
                    textBox7.Text = "";
                    textBox8.Text = "";
                    comboBox1.SelectedItem = null;
                    comboBox1.SelectedItem = null;
                    comboBox1.SelectedItem = null;
                    this.Dispose();

                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }
                finally
                {
                    conexion.Close();
                }
            }
            else
            {
                MessageBox.Show("Debe completar todos los datos obligatorios.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form aLocalidad = new AgregarLocalidad();
            aLocalidad.Show();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void FillProvincia()
        {
            SqlCommand cmd2 = new SqlCommand();
            cmd2.Connection = conexion;
            cmd2.CommandType = CommandType.Text;
            cmd2.CommandText = "SELECT idProvincia, nombreProvincia FROM provincia";
            DataSet objDs = new DataSet();
            adaptador2 = new SqlDataAdapter();
            adaptador2.SelectCommand = cmd2;
            conexion.Open();
            adaptador2.Fill(objDs);
            conexion.Close();
            comboBox3.DataSource = objDs.Tables[0];
            comboBox3.ValueMember = "idProvincia";
            comboBox3.DisplayMember = "nombreProvincia";

        }

        private void FillLocalidad(int provinciaID)
        {
            string query = "select nombreLocalidad, idLocalidad from localidad where localidad.idProvincia = '" + provinciaID + "'";
            SqlDataAdapter da = new SqlDataAdapter(query, conexion);
            conexion.Open();
            DataSet ds = new DataSet();
            da.Fill(ds, "Localidad");
            conexion.Close();
            comboBox1.DisplayMember = "nombreLocalidad";
            comboBox1.ValueMember = "idLocalidad";
            comboBox1.DataSource = ds.Tables["Localidad"];

        }
    }
}
