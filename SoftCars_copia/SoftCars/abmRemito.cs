﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class abmRemito : Form
    {
        public abmRemito()
        {
            InitializeComponent();
        }
        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private SqlDataAdapter adaptador2;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        //String valorRadio;
        //DataGridView dgv = new DataGridView();
        int bandera = 0;
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();

        private void button2_Click(object sender, EventArgs e)
        {
            if(textBox2.Text==string.Empty || textBox15.Text == string.Empty)
            {
                textBox2.BackColor = Color.IndianRed;
                textBox15.BackColor = Color.IndianRed;
                label16.ForeColor = Color.IndianRed;
                //Validacion.SetHighlightColor(textBox2, DevComponents.DotNetBar.Validator.eHighlightColor.Red);
                //Validacion.SetHighlightColor(textBox15, DevComponents.DotNetBar.Validator.eHighlightColor.Red);
            } else if(textBox8.Text == string.Empty || textBox13.Text == string.Empty || textBox14.Text == string.Empty)
            {
                textBox8.BackColor = Color.IndianRed;
                textBox13.BackColor = Color.IndianRed;
                textBox14.BackColor = Color.IndianRed;
                label13.ForeColor = Color.IndianRed;
                label14.ForeColor = Color.IndianRed;
                label15.ForeColor = Color.IndianRed;
            }
            else
            {
                int contador = 0;

                SqlCommand alta = new SqlCommand("insert into remito_ingreso values ( @numeroRemito ,@fecha, @idProveedor)", conexion);

                adaptador.InsertCommand = alta;

                string numeroRemito = textBox2.Text + "-" + textBox15.Text;
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@numeroRemito", SqlDbType.VarChar));
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@fecha", SqlDbType.DateTime));
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idProveedor", SqlDbType.Int));



                adaptador.InsertCommand.Parameters["@fecha"].Value = dateTimePicker1.Value;
                adaptador.InsertCommand.Parameters["@numeroRemito"].Value = numeroRemito;
                adaptador.InsertCommand.Parameters["@idProveedor"].Value = int.Parse(label2.Text);

                try
                {
                    conexion.Open();
                    adaptador.InsertCommand.ExecuteNonQuery();
                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }
                finally
                {
                    conexion.Close();
                }

                SqlCommand alta2 = new SqlCommand("insert into detalle_remito_ingreso values (@idRemitoIngreso, @idArticulo, @nombre, @cantidad)", conexion);

                adaptador2.InsertCommand = alta2;
                adaptador2.InsertCommand.Parameters.Add(new SqlParameter("@idRemitoIngreso", SqlDbType.Int));
                adaptador2.InsertCommand.Parameters.Add(new SqlParameter("@idArticulo", SqlDbType.Int));
                adaptador2.InsertCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                adaptador2.InsertCommand.Parameters.Add(new SqlParameter("@cantidad", SqlDbType.Int));




                try
                {
                    conexion.Open();

                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        // int codigo = (int)row.Cells["id"].Value;


                        //String nombre = row.Cells["Articulo"].Value.ToString();
                        //int cantidad = (int)row.Cells["cantidad"].Value;
                        //float precio = (float)row.Cells["Precio"].Value;
                        float acumulado = 0;




                        alta2.Parameters.Clear();

                        alta2.Parameters.AddWithValue("@idRemitoIngreso", Convert.ToInt32(textBox1.Text));
                        alta2.Parameters.AddWithValue("@idArticulo", Convert.ToString(row.Cells[0].Value));
                        alta2.Parameters.AddWithValue("@nombre", Convert.ToString(row.Cells["articulo"].Value));
                        alta2.Parameters.AddWithValue("@cantidad", Convert.ToInt32(row.Cells["cantidad"].Value));

                        alta2.ExecuteNonQuery();

                        //TRAIGO LA CANTIDAD QUE HAY EN STOCK DE UN ARTICULO
                        var cant = new SqlCommand("select cantidad from articulo where nombre_articulo ='" + row.Cells["articulo"].Value + "'", conexion);

                        var dat2 = new SqlDataAdapter(cant);

                        dat2.Fill(dt2);

                        int cantidad = (int)dt2.Rows[0][0];
                        int cantNueva = (int)row.Cells["cantidad"].Value;

                        // SUMO LA CANTIDAD DE STOCK MAS LO QUE INGRESÓ EN EL REMITO
                        int cantidadArticuloSum = cantidad + cantNueva;

                        //ACTUALIZO EN LA BASE DE DATOS
                        SqlCommand modificacion = new SqlCommand("update articulo set cantidad = @cantidad where nombre_articulo = @nombre", conexion);

                        adaptador.UpdateCommand = modificacion;
                        adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@cantidad", SqlDbType.Int));
                        adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));

                        adaptador.UpdateCommand.Parameters["@cantidad"].Value = cantidadArticuloSum;
                        adaptador.UpdateCommand.Parameters["@nombre"].Value = row.Cells["articulo"].Value;

                        modificacion.ExecuteNonQuery();

                        modificacion.Parameters.Clear();



                        //adaptador.InsertCommand.Parameters["@descuento"].Value = textBox2.Text;
                        //adaptador.InsertCommand.Parameters["@pdescuento"].Value = pdescuento;


                        // adaptador.InsertCommand.ExecuteNonQuery();


                        //adaptador.InsertCommand.Parameters["@nombre"].Value = nombre;
                        //adaptador.InsertCommand.Parameters["@cantidad"].Value = cantidad;
                        //adaptador.InsertCommand.Parameters["@precio"].Value = precio;
                        //adaptador.InsertCommand.Parameters["@acumulado"].Value = acumulado;

                    }


                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }
                finally
                {
                    MessageBox.Show("Remito de Ingreso cargado Correctamente", "Confirmación",MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    dataGridView1.Columns.Clear();
                    dataGridView2.Columns.Clear();
                    //Borro los TextBox
                    textBox1.Text = "";

                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                    textBox5.Text = "";
                    textBox8.Text = "";
                    textBox13.Text = "";
                    textBox14.Text = "";
                    textBox15.Text = "";

                    //Desabilito todo
                    dateTimePicker1.Enabled = false;

                    textBox2.Enabled = false;
                    textBox3.Enabled = false;
                    textBox4.Enabled = false;
                    textBox5.Enabled = false;
                    textBox15.Enabled = false;

                    dataGridView1.Enabled = false;
                    dataGridView2.Enabled = false;

                    button1.Enabled = false;
                    button2.Enabled = false;
                    button3.Enabled = false;
                    button4.Enabled = false;
                    button12.Enabled = false;


                    label3.Enabled = false;
                    label4.Enabled = false;

                    conexion.Close();
                }
            }                      
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dateTimePicker1.Enabled = true;

            textBox2.Enabled = true;
            textBox3.Enabled = true;
            textBox4.Enabled = true;
            textBox15.Enabled = true;

            dataGridView1.Enabled = true;
            dataGridView2.Enabled = true;

            button12.Enabled = true;
            button4.Enabled = true;
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;

            label3.Enabled = true;
            label4.Enabled = true;
            label13.Enabled = true;
            label14.Enabled = true;
            label16.Enabled = true;
            label17.Enabled = true;

            conexion.Open();

            var cmd = new SqlCommand("SELECT max(idRemitoIngreso) FROM remito_ingreso", conexion);

            var da = new SqlDataAdapter(cmd);

            da.Fill(dt);

            conexion.Close();

            int padre = (int)dt.Rows[0][0];

            padre++;

            textBox1.Text = padre.ToString();

            conexion.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            adaptador.SelectCommand = new SqlCommand("ListaArticulo2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art2", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art2"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView2.DataSource = datosTable;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == string.Empty)
            {
                textBox5.BackColor = Color.IndianRed;
                label3.ForeColor = Color.IndianRed;
                //Validacion.SetHighlightColor(textBox5, DevComponents.DotNetBar.Validator.eHighlightColor.Red);
            }
            else
            {
                int id = int.Parse(label6.Text);
                String articulo = textBox3.Text;
                int cantidad = int.Parse(textBox5.Text);


                if (bandera < 1)
                {
                    dataGridView1.Columns.Add("id", "N° ARTICULO");
                    dataGridView1.Columns.Add("articulo", "ARTICULO");
                    dataGridView1.Columns.Add("cantidad", "CANTIDAD");

                    bandera++;
                }

                dataGridView1.Rows.Add(id, articulo, cantidad);

                textBox3.Text = "";
                textBox5.Text = "";

                textBox5.Enabled = false;
            }

            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if(textBox8.BackColor == Color.IndianRed)
            {
                textBox8.BackColor = Color.White;
                textBox13.BackColor = Color.White;
                textBox14.BackColor = Color.White;
                label13.ForeColor = Color.White;
                label14.ForeColor = Color.White;
                label15.ForeColor = Color.White;
            }
            BuscarProveedorRemito c = new BuscarProveedorRemito();
            c.pasado8 += new BuscarProveedorRemito.pasar8(ejecutar8);
            c.Show();
        }

        /*
        private void button12_Click(object sender, EventArgs e)
        {
            ConsultarClienteFactura c = new ConsultarClienteFactura();
            c.pasado += new ConsultarClienteFactura.pasar(ejecutar);
            c.Show();
        }

        public void ejecutar(string dato, string dato2, string dato3, string dato4, string dato5)
        {
            //nombre
            textBox13.Text = dato3;
            //cuit
            textBox8.Text = dato2;
            //tipoFactura
            textBox14.Text = dato4;

            //id
            textBox16.Text = dato;

            //idTipoFactura
            textBox17.Text = dato5;


        }
        */

        public void ejecutar8(string dato, string dato2, string dato3, string dato4, string dato5)
        {
            //nombre
            textBox13.Text = dato3;
            //cuit
            textBox8.Text = dato2;
            //tipoFactura
            textBox14.Text = dato4;

            //idProveedor
            label2.Text = dato;

            //idTipoFactura
            label9.Text = dato5;
        }

        private void abmRemito_Load_1(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
                adaptador2 = new SqlDataAdapter();
                dataGridView1.AllowUserToAddRows = false;
                dataGridView2.AllowUserToAddRows = false;

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (textBox3.Text != "")
            {
                textBox5.Enabled = true;
            }
            textBox5.Enabled = true;
            if (dataGridView2.CurrentRow.Index != -1)
            {
                label6.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
                textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (textBox3.Text != "")
            {
                textBox5.Enabled = true;
            }
            if (dataGridView2.CurrentRow.Index != -1)
            {
                label6.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
                textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();

            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
            textBox2.BackColor = Color.White;
            textBox15.BackColor = Color.White;
            label16.ForeColor = Color.White;
        }

        private void textBox15_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
            textBox2.BackColor = Color.White;
            textBox15.BackColor = Color.White;
            label16.ForeColor = Color.White;
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
            textBox5.BackColor = Color.White;
            label3.ForeColor = Color.White;
        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {
            if (textBox15.BackColor == Color.IndianRed)
            {
                textBox15.BackColor = Color.White;
                label16.ForeColor = Color.White;
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if(dataGridView1.Rows.Count > 0)
            {
                dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if(textBox5.BackColor == Color.IndianRed)
            {
                textBox5.BackColor = Color.White;
                label3.ForeColor = Color.White;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.BackColor == Color.IndianRed)
            {
                textBox2.BackColor = Color.White;
                label16.ForeColor = Color.White;
            }
        }
    }
}
