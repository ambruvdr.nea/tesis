﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace SoftCars
{
    public partial class abmClientes : Form
    {
        public abmClientes()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        DataTable dt = new DataTable();
        //String valorRadio;
        public static string variableGlobalCliente;

        private Boolean email_bien_escrito(String email)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" & textBox2.Text != "" & textBox3.Text != "" & textBox4.Text != "" & textBox6.Text != "")
            {



                adaptador.InsertCommand.Parameters["@cuilCliente"].Value = textBox1.Text;
                adaptador.InsertCommand.Parameters["@nombreCliente"].Value = textBox2.Text;
                string telefonoCliente = textBox3.Text + "-" + textBox4.Text;
                adaptador.InsertCommand.Parameters["@telCliente"].Value = telefonoCliente;
                adaptador.InsertCommand.Parameters["@mailCliente"].Value = textBox6.Text;
                adaptador.InsertCommand.Parameters["@direccionCliente"].Value = textBox5.Text;
                adaptador.InsertCommand.Parameters["@barrioCliente"].Value = textBox7.Text;
                adaptador.InsertCommand.Parameters["@codigoPostal"].Value = textBox8.Text;
                adaptador.InsertCommand.Parameters["@idLocalidad"].Value = comboBox3.SelectedValue;
                adaptador.InsertCommand.Parameters["@idTipoFactura"].Value = comboBox1.SelectedValue;

                //adaptador.InsertCommand.Parameters["@idCliente"].Value = int.Parse(textBox11.Text);
                if (email_bien_escrito(textBox6.Text))
                {

                    try
                    {
                        conexion.Open();
                        adaptador.InsertCommand.ExecuteNonQuery();
                        MessageBox.Show("Se ha registrado el Cliente exitosamente.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                        textBox4.Text = "";
                        textBox5.Text = "";
                        textBox6.Text = "";
                        textBox7.Text = "";
                        textBox8.Text = "";
                        Form a = new AltaVehiculo();
                        a.Show();
                    }
                    catch (SqlException excepcion)
                    {
                        MessageBox.Show(excepcion.ToString());
                    }
                    finally
                    {
                        conexion.Close();
                    }

                    //actualizarDatos();
                }
                else
                {
                    MessageBox.Show("EL mail incorrecto");
                }

            }
            else
            {
                MessageBox.Show("Debe completar todos los campos de datos obligatorios.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }


        }



        private void abmClientes_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet9.tipoFactura' Puede moverla o quitarla según sea necesario.
            this.tipoFacturaTableAdapter.Fill(this.baseDeDatosDataSet9.tipoFactura);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet9.localidad' Puede moverla o quitarla según sea necesario.
            this.localidadTableAdapter.Fill(this.baseDeDatosDataSet9.localidad);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet9.provincia' Puede moverla o quitarla según sea necesario.
            this.provinciaTableAdapter.Fill(this.baseDeDatosDataSet9.provincia);
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();

            conexion.Open();

            var cmd = new SqlCommand("SELECT max(idCliente) FROM cliente", conexion);

            var da = new SqlDataAdapter(cmd);



            da.Fill(dt);
            int padre;
            conexion.Close();

            padre = (int)dt.Rows[0][0];

            padre++;

            textBox11.Text = padre.ToString();

            variableGlobalCliente = textBox11.Text;

            SqlCommand alta = new SqlCommand("insert into cliente values (@cuilCliente, @nombreCliente, @telCliente, @mailCliente, @direccionCliente, @barrioCliente ,@codigoPostal ,@idLocalidad ,@idTipoFactura)", conexion);

            adaptador.InsertCommand = alta;
            //adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idCliente", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@cuilCliente", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombreCliente", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@telCliente", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@mailCliente", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@direccionCliente", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@barrioCliente", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@codigoPostal", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idLocalidad", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTipoFactura", SqlDbType.Int));
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verificar que la tecla presionada no sea CTRL u otra tecla no numerica
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form aLocalidad = new AgregarLocalidad();
            aLocalidad.Show();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }
    }
}

