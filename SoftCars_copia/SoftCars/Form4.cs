﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        //String valorRadio;

        private void Form4_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet6.rubro' Puede moverla o quitarla según sea necesario.
            this.rubroTableAdapter1.Fill(this.baseDeDatosDataSet6.rubro);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet3.rubro' Puede moverla o quitarla según sea necesario.
            //this.rubroTableAdapter.Fill(this.baseDeDatosDataSet3.rubro);


            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;

            SqlCommand alta = new SqlCommand("insert into articulo values (@nombre, @cantidad, @precio, @id_rubro)", conexion);

            adaptador.InsertCommand = alta;
            //adaptador.InsertCommand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@cantidad", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@precio", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@id_rubro", SqlDbType.Int));


            /*
            SqlCommand consulta = new SqlCommand("select * from articulo", conexion);
            adaptador.SelectCommand = consulta;
            datos = new DataSet();
            conexion.Open();
            adaptador.Fill(datos, "articulo");
            conexion.Close();
            dataGridView1.DataSource = datos;
            dataGridView1.DataMember = "articulo";
            */

            adaptador.SelectCommand = new SqlCommand("ListaArticulo1", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art1", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art1"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView1.DataSource = datosTable;

        }

        private void actualizarDatos()
        {
            datos.Clear();
            adaptador.Fill(datos, "articulo");
        }



        private void button1_Click(object sender, EventArgs e)
        {

            
            adaptador.InsertCommand.Parameters["@nombre"].Value = textBox1.Text;
            adaptador.InsertCommand.Parameters["@cantidad"].Value = int.Parse(textBox3.Text);
            adaptador.InsertCommand.Parameters["@precio"].Value = float.Parse(textBox2.Text);
            adaptador.InsertCommand.Parameters["@id_rubro"].Value = comboBox1.SelectedValue;


            try
            {
                conexion.Open();
                adaptador.InsertCommand.ExecuteNonQuery();
                MessageBox.Show("Se ha ingresado con exito");
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }

            actualizarDatos();

            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";

        }

        private void button4_Click(object sender, EventArgs e)
        {
            adaptador.SelectCommand = new SqlCommand("ListaArticulo1", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art1", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art1"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView1.DataSource = datosTable;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlCommand baja = new SqlCommand("delete from articulo where id_articulo=@id", conexion);

            adaptador.DeleteCommand = baja;
            adaptador.DeleteCommand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int));

            adaptador.DeleteCommand.Parameters["@id"].Value = int.Parse(textBox5.Text);


            try
            {
                conexion.Open();

                int cantidad = adaptador.DeleteCommand.ExecuteNonQuery();
                if (cantidad == 0)
                {
                    MessageBox.Show("No existe");
                }
                else
                {
                    MessageBox.Show("Se ha eliminado con exito");
                }

            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }

            actualizarDatos();

            textBox4.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox5.Text = "";

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentRow.Index != -1)
            {
                textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                textBox2.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                textBox3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                textBox5.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                comboBox1.SelectedValue = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            }

          
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentRow.Index != -1)
            {
                textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                textBox2.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                textBox3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                textBox5.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                comboBox1.SelectedValue = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlCommand modificacion = new SqlCommand("update articulo set nombre_articulo = @nombre, cantidad = @cantidad, precio = @precio, id_rubro = @rubro where id_articulo = @id_a", conexion);

            adaptador.UpdateCommand = modificacion;
            adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
            adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@cantidad", SqlDbType.Int));
            adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@precio", SqlDbType.Float));
            adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@rubro", SqlDbType.Int));

            adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@id_a", SqlDbType.Int));




            adaptador.UpdateCommand.Parameters["@id_a"].Value = int.Parse(textBox5.Text);
            adaptador.UpdateCommand.Parameters["@nombre"].Value = textBox1.Text;
            adaptador.UpdateCommand.Parameters["@precio"].Value = textBox2.Text;
            adaptador.UpdateCommand.Parameters["@cantidad"].Value = textBox3.Text;
            adaptador.UpdateCommand.Parameters["@rubro"].Value = comboBox1.SelectedValue;

            try
            {
                conexion.Open();
                adaptador.UpdateCommand.ExecuteNonQuery();
                MessageBox.Show("Se ha modificado con exito");
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }

            actualizarDatos();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == false)
            {
                button1.Enabled = true;
                button2.Enabled = false;
                button3.Enabled = false;

                button1.ForeColor = Color.White;
                button2.ForeColor = Color.Black;
                button3.ForeColor = Color.Black;
            }
            else
            {
                button1.Enabled = false;
                button2.Enabled = true;
                button3.Enabled = true;
                button1.ForeColor = Color.Black;
                button2.ForeColor = Color.White;
                button3.ForeColor = Color.White;
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
