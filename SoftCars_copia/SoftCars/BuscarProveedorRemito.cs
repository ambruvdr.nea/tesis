﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class BuscarProveedorRemito : Form
    {
        public delegate void pasar8(string dato, string dato2, string dato3, string dato4, string dato5);
        public event pasar8 pasado8;

        //public delegate void pasar2(string dato2);
        //public event pasar2 pasado2;
        public BuscarProveedorRemito()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        //String valorRadio;
        public static int idProveedorAgregar;
        //private string idClienteAgregarStr;

        private void BuscarProveedorRemito_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                adaptador.SelectCommand = new SqlCommand("BuscarProveedorRemito", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@pruebaF", SqlDbType.VarChar);

                datosTable = new DataTable();
                adaptador.SelectCommand.Parameters["@pruebaF"].Value = textBox4.Text;
                adaptador.Fill(datosTable);
                dataGridView1.DataSource = datosTable;

            }
            else
            {
                adaptador.SelectCommand = new SqlCommand("BuscarProveedorRemito2", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@prueba2", SqlDbType.VarChar);

                datosTable = new DataTable();
                adaptador.SelectCommand.Parameters["@prueba2"].Value = textBox4.Text;
                adaptador.Fill(datosTable);
                dataGridView1.DataSource = datosTable;
            }
            
        }



        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string variable = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string variable2 = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            string variable3 = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            string variable4 = dataGridView1.CurrentRow.Cells[3].Value.ToString();  
            textBox1.Text = variable;
            textBox2.Text = variable2;
            textBox3.Text = variable3;
            textBox5.Text = variable4;
            textBox6.Text = "";
            idProveedorAgregar = int.Parse(variable);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string variable = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string variable2 = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            string variable3 = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            string variable4 = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox1.Text = variable;
            textBox2.Text = variable2;
            textBox3.Text = variable3;
            textBox5.Text = variable4;
            textBox6.Text = "";

            idProveedorAgregar = int.Parse(variable);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string variable = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string variable2 = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            string variable3 = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            string variable4 = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox1.Text = variable;
            textBox2.Text = variable2;
            textBox3.Text = variable3;
            textBox5.Text = variable4;
            textBox6.Text = "";
            idProveedorAgregar = int.Parse(variable);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT idTipoFactura FROM proveedor WHERE idProveedor='" + int.Parse(textBox1.Text) + "'", conexion);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                //Razon Social
                string col1Value = dr[0].ToString();
                textBox2.Text = col1Value;
            }
            pasado8(textBox1.Text, textBox2.Text, textBox3.Text, textBox5.Text, textBox6.Text);

            conexion.Close();
            this.Dispose();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
