﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoftCars
{
    public partial class AgregarDescuentoFactura : Form
    {
        public delegate void pasar3(string dato, string dato2);
        public event pasar3 pasado3;

        public AgregarDescuentoFactura()
        {
            InitializeComponent();
        }

        private void AgregarDescuento_Load(object sender, EventArgs e)
        {

            textBox1.Text = Form5.elTotalGlobalF;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
            label3.Text = "$00.00";
        }

        private void button1_Click(object sender, EventArgs e)
        {      
            if (textBox1.Text != "" & textBox2.Text!="")
            {
                float descuento = float.Parse(textBox2.Text);
                float total = float.Parse(textBox1.Text);
   
                if (descuento > 0 & descuento < 101)
                {
                    float porcentaje = 1 - (descuento / 100);
                    total = total * porcentaje;
                }

                label3.Text = total.ToString();
                button3.Enabled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (label3.Text!="$00.00")
            {
                pasado3(textBox2.Text, label3.Text);
                this.Dispose();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
