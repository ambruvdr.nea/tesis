﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace SoftCars
{
    public partial class abmProveedores : Form
    {
        public abmProveedores()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador, adaptador2, adaptador3;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        DataTable dt = new DataTable();
        //String valorRadio;
        public static string variableGlobalCliente;


        private void abmProveedores_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'datasetTipoFactura.tipoFactura' Puede moverla o quitarla según sea necesario.
            //this.tipoFacturaTableAdapter1.Fill(this.datasetTipoFactura.tipoFactura);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet32.tipoFactura' Puede moverla o quitarla según sea necesario.
            //this.tipoFacturaTableAdapter2.Fill(this.baseDeDatosDataSet32.tipoFactura);
            // TODO: esta línea de código carga datos en la tabla 'comboBoxTipoFactura.tipoFactura' Puede moverla o quitarla según sea necesario.
            //this.tipoFacturaTableAdapter1.Fill(this.comboBoxTipoFactura.tipoFactura);
            // TODO: esta línea de código carga datos en la tabla 'comboBoxLocalidad.localidad' Puede moverla o quitarla según sea necesario.
            //this.localidadTableAdapter1.Fill(this.comboBoxLocalidad.localidad);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet31.provincia' Puede moverla o quitarla según sea necesario.
            //this.provinciaTableAdapter1.Fill(this.baseDeDatosDataSet31.provincia);

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            FillProvincia();

            conexion.Open();

            var cmd = new SqlCommand("SELECT max(idProveedor) FROM proveedor", conexion);
            var da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            int padre;
            conexion.Close();

            padre = (int)dt.Rows[0][0];

            padre++;

            textBox11.Text = padre.ToString();

            variableGlobalCliente = textBox11.Text;

            SqlCommand alta = new SqlCommand("insert into proveedor values (@nombreProveedor, @cuilProveedor, @telProveedor, @mailProveedor, @direccionProveedor, @idLocalidad, @idTipoFactura, @codigoPostal, @provinciaProveedor, @barrioProveedor)", conexion);

            adaptador.InsertCommand = alta;
            //adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idCliente", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@cuilProveedor", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombreProveedor", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@telProveedor", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@mailProveedor", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@direccionProveedor", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@provinciaProveedor", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@barrioProveedor", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@codigoPostal", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idLocalidad", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTipoFactura", SqlDbType.Int));
        }

    private Boolean email_bien_escrito(String email)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if(textBox1.Text == string.Empty || textBox1.Text.Length < 11)
            {
                textBox1.BackColor = Color.IndianRed;                
                label1.ForeColor = Color.IndianRed;                
                label12.Visible = true;                
            }
            else if(textBox2.Text == string.Empty)
            {
                textBox2.BackColor = Color.IndianRed;
                label2.ForeColor = Color.IndianRed;
                label14.Visible = true;
            }
            else if (textBox3.Text == string.Empty || textBox4.Text == string.Empty)
            {
                textBox3.BackColor = Color.IndianRed;
                textBox4.BackColor = Color.IndianRed;
                label3.ForeColor = Color.IndianRed;
                label24.Visible = true;
            }
            else
            {
                String mail = textBox6.Text;
                if (email_bien_escrito(mail) || textBox6.Text != string.Empty)
                {
                    adaptador.InsertCommand.Parameters["@cuilProveedor"].Value = textBox1.Text;
                    adaptador.InsertCommand.Parameters["@nombreProveedor"].Value = textBox2.Text;
                    string telefonoProveedor = textBox3.Text + "-" + textBox4.Text;
                    adaptador.InsertCommand.Parameters["@telProveedor"].Value = telefonoProveedor;
                    adaptador.InsertCommand.Parameters["@mailProveedor"].Value = textBox6.Text;
                    adaptador.InsertCommand.Parameters["@direccionProveedor"].Value = textBox5.Text;
                    adaptador.InsertCommand.Parameters["@provinciaProveedor"].Value = comboBox2.SelectedValue;
                    adaptador.InsertCommand.Parameters["@barrioProveedor"].Value = textBox7.Text;
                    adaptador.InsertCommand.Parameters["@codigoPostal"].Value = textBox8.Text;
                    adaptador.InsertCommand.Parameters["@idLocalidad"].Value = comboBox3.SelectedValue;
                    adaptador.InsertCommand.Parameters["@idTipoFactura"].Value = comboBox1.SelectedValue;

                    try
                    {
                        conexion.Open();
                        adaptador.InsertCommand.ExecuteNonQuery();
                        MessageBox.Show("Se ha ingresado con exito");
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                        textBox4.Text = "";
                        textBox5.Text = "";
                        textBox6.Text = "";
                        textBox7.Text = "";
                        textBox8.Text = "";
                        comboBox1.SelectedItem = null;
                        
                    }
                    catch (SqlException excepcion)
                    {
                        MessageBox.Show(excepcion.ToString());
                    }
                    finally
                    {
                        conexion.Close();
                        if (comboBox2.SelectedValue.ToString() != "")
                        {
                            int provinciaID = Convert.ToInt32(comboBox2.SelectedValue.ToString());
                            FillLocalidad(provinciaID);
                        }
                    }
                }
                else
                {
                    label15.Visible = true;
                    label7.ForeColor = Color.IndianRed;
                    textBox6.BackColor = Color.IndianRed;
                }
            }
            
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verificar que la tecla presionada no sea CTRL u otra tecla no numerica
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form aLocalidad = new AgregarLocalidad();
            aLocalidad.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.BackColor == Color.IndianRed)
            {
                textBox1.BackColor = Color.White;
                label1.ForeColor = Color.White;
                label12.Visible = false;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.BackColor == Color.IndianRed)
            {
                textBox2.BackColor = Color.White;
                label2.ForeColor = Color.White;
                label14.Visible = false;
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.BackColor == Color.IndianRed)
            {
                textBox6.BackColor = Color.White;
                label7.ForeColor = Color.White;
                label15.Visible = false;
            }
        }

        private void textBox1_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }
        private void FillProvincia()
        {
            SqlCommand cmd2 = new SqlCommand();
            cmd2.Connection = conexion;
            cmd2.CommandType = CommandType.Text;
            cmd2.CommandText = "SELECT idProvincia, nombreProvincia FROM provincia";
            DataSet objDs = new DataSet();
            adaptador2 = new SqlDataAdapter();
            adaptador2.SelectCommand = cmd2;
            conexion.Open();
            adaptador2.Fill(objDs);
            conexion.Close();
            comboBox2.DataSource = objDs.Tables[0];
            comboBox2.ValueMember = "idProvincia";
            comboBox2.DisplayMember = "nombreProvincia";
            
        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            textBox3.BackColor = Color.White;
            textBox4.BackColor = Color.White;
            label3.ForeColor = Color.White;
            label24.Visible = false;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            textBox3.BackColor = Color.White;
            textBox4.BackColor = Color.White;
            label3.ForeColor = Color.White;
            label24.Visible = false;
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.SelectedItem != null)
            {
                DataRowView drv = comboBox3.SelectedItem as DataRowView;

                Debug.WriteLine("Item: " + drv.Row["nombreLocalidad"].ToString());
                Debug.WriteLine("Value: " + drv.Row["idLocalidad"].ToString());
                Debug.WriteLine("Value: " + comboBox3.SelectedValue.ToString());
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedValue.ToString() != "")
            {
                int provinciaID = Convert.ToInt32(comboBox2.SelectedValue.ToString());
                FillLocalidad(provinciaID);
            }
        }

        private void FillLocalidad(int provinciaID)
        {
            string query = "select nombreLocalidad, idLocalidad from localidad where localidad.idProvincia = '" + provinciaID + "'";
            SqlDataAdapter da = new SqlDataAdapter(query, conexion);
            conexion.Open();
            DataSet ds = new DataSet();
            da.Fill(ds, "Localidad");
            conexion.Close();
            comboBox3.DisplayMember = "nombreLocalidad";
            comboBox3.ValueMember = "idLocalidad";
            comboBox3.DataSource = ds.Tables["Localidad"];
            
        }
    }
}
