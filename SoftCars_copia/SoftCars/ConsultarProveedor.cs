﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class ConsultarProveedor : Form
    {
        public ConsultarProveedor()
        {
            InitializeComponent();
        }
        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        //String valorRadio;
        public static int idProveedorModificar;
        private int bandera;

        private void button4_Click(object sender, EventArgs e)
        {
            bandera = 0;

            dataGridView1.Columns.Clear();

            if (radioButton1.Checked)
            {
                adaptador.SelectCommand = new SqlCommand("ConsultarProveedor", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@nombre", SqlDbType.VarChar);

                datosTable = new DataTable();
                adaptador.SelectCommand.Parameters["@nombre"].Value = textBox4.Text;
                adaptador.Fill(datosTable);
                dataGridView1.DataSource = datosTable;


                DataGridViewColumn column0 = dataGridView1.Columns[0];
                column0.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column1 = dataGridView1.Columns[1];
                column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column2 = dataGridView1.Columns[2];
                column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column3 = dataGridView1.Columns[3];
                column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column4 = dataGridView1.Columns[4];
                column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column5 = dataGridView1.Columns[5];
                column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column6 = dataGridView1.Columns[6];
                column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column7 = dataGridView1.Columns[7];
                column7.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;



            }
            else
            {
                bandera = 1;

                dataGridView1.Columns.Clear();

                adaptador.SelectCommand = new SqlCommand("ConsultarProveedor2", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@cuil", SqlDbType.VarChar);

                datosTable = new DataTable();
                adaptador.SelectCommand.Parameters["@cuil"].Value = textBox4.Text;
                adaptador.Fill(datosTable);
                dataGridView1.DataSource = datosTable;

                DataGridViewColumn column0 = dataGridView1.Columns[0];
                column0.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column1 = dataGridView1.Columns[1];
                column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column2 = dataGridView1.Columns[2];
                column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column3 = dataGridView1.Columns[3];
                column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column4 = dataGridView1.Columns[4];
                column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column5 = dataGridView1.Columns[5];
                column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column6 = dataGridView1.Columns[6];
                column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column7 = dataGridView1.Columns[7];
                column7.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }
        }

        private void ConsultarProveedor_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;
        }

        private void actualizarDatos()
        {
            datos.Clear();
            adaptador.Fill(datos, "proveedor");
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string Variable = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            idProveedorModificar = int.Parse(Variable);

            Form m = new ModificarProveedor();
            m.Show();
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (bandera == 1)
            {

                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                bandera = 0;
                textBox4.Text = "";
                textBox4.MaxLength = 50;
            }
            if (radioButton2.Checked)
            {
                bandera = 1;
                textBox4.Text = "";
                textBox4.MaxLength = 11;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
