﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class Form3 : Form
    {
        
        public Form3()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        int variable;
        private DataTable datosTable;
        String valorRadio;


        private void Form3_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet5.usuario' Puede moverla o quitarla según sea necesario.
            this.usuarioTableAdapter1.Fill(this.baseDeDatosDataSet5.usuario);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet.usuario' Puede moverla o quitarla según sea necesario.
            //this.usuarioTableAdapter.Fill(this.baseDeDatosDataSet.usuario);


            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();

            SqlCommand alta = new SqlCommand("insert into usuario values (@nombre,@pass,@tipo)", conexion);

            adaptador.InsertCommand = alta;
            //adaptador.InsertCommand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@pass", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@tipo", SqlDbType.Int));

            
            SqlCommand consulta = new SqlCommand("select * from usuario", conexion);
            adaptador.SelectCommand = consulta;
            datos = new DataSet();
            conexion.Open();
            adaptador.Fill(datos, "usuario");
            conexion.Close();
            dataGridView1.DataSource = datos;
            dataGridView1.DataMember = "usuario";
            



        }

        private void actualizarDatos()
        {
            datos.Clear();
            adaptador.Fill(datos, "usuario");
        }


        //AGREGAR --------------------------------------------------------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {

            if (radioButton1.Checked == true)
            {
                variable = 1;
            }
            if (radioButton2.Checked == true)
            {
                variable = 2;

            }
            if (radioButton3.Checked == true)
            {
                variable = 3;
            }
            if (radioButton4.Checked == true)
            {
                variable = 4;
            }


            //adaptador.InsertCommand.Parameters["@id"].Value = textBox1.Text;
            adaptador.InsertCommand.Parameters["@nombre"].Value = textBox1.Text;
            adaptador.InsertCommand.Parameters["@pass"].Value = textBox2.Text;
            adaptador.InsertCommand.Parameters["@tipo"].Value = variable;

            try
            {
                conexion.Open();
                adaptador.InsertCommand.ExecuteNonQuery();
                MessageBox.Show("Se ha ingresado con exito");
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }

            actualizarDatos();

            textBox1.Text = "";
            textBox2.Text = "";
            radioButton1.Checked = true;
        }



        //ELIMINAR--------------------------------------------------------------------------------------------------------------
        private void button2_Click(object sender, EventArgs e)
        {
            SqlCommand baja = new SqlCommand("delete from usuario where id_usuario=@id", conexion);

            adaptador.DeleteCommand = baja;
            adaptador.DeleteCommand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int));

            adaptador.DeleteCommand.Parameters["@id"].Value = int.Parse(textBox4.Text);


            try
            {
                conexion.Open();

                int cantidad = adaptador.DeleteCommand.ExecuteNonQuery();
                if (cantidad == 0)
                {
                    MessageBox.Show("No existe");
                }
                else
                {
                    MessageBox.Show("Se ha eliminado con exito");
                }

            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }

            actualizarDatos();

            textBox4.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
            radioButton1.Checked = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            adaptador.SelectCommand = new SqlCommand("ListaUsuario1", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@lis1", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@lis1"].Value = textBox3.Text;
            adaptador.Fill(datosTable);
            dataGridView1.DataSource = datosTable;
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dataGridView1.CurrentRow.Index != -1)
            {
                textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                textBox2.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                valorRadio = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                textBox4.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            }

            if (valorRadio == "Administrador")
            {
                radioButton1.Checked = true;
            }

            if (valorRadio == "Recepcionista")
            {
                radioButton2.Checked = true;
            }

            if (valorRadio == "Venta")
            {
                radioButton3.Checked = true;
            }

            if (valorRadio == "Compra")
            {
                radioButton4.Checked = true;
            }
        }

        private void checkBox1_CheckStateChanged(object sender, EventArgs e)
        {
           
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBox1.Checked == false)
            {
                button1.Enabled = true;
                button2.Enabled = false;
                button3.Enabled = false;
                button1.BackColor = Color.White;
                button2.BackColor = Color.LightGray;
                button3.BackColor = Color.LightGray;
                button1.ForeColor = Color.Black;
                button2.ForeColor = Color.Gray;
                button3.ForeColor = Color.Gray;
            }
            else
            {
                button1.Enabled = false;
                button2.Enabled = true;
                button3.Enabled = true;
                button1.BackColor = Color.LightGray;
                button2.BackColor = Color.White;
                button3.BackColor = Color.White;
                button1.ForeColor = Color.Gray;
                button2.ForeColor = Color.Black;
                button3.ForeColor = Color.Black;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
           
            SqlCommand modificacion = new SqlCommand("update usuario set nombre_usuario = @nombre, password = @pass, id_tipo_usuario = @tipo where id_usuario = @id_a", conexion);

            adaptador.UpdateCommand = modificacion;
            adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
            adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@pass", SqlDbType.VarChar));
            adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@tipo", SqlDbType.Int));

            adaptador.UpdateCommand.Parameters.Add(new SqlParameter("@id_a", SqlDbType.Int));




            adaptador.UpdateCommand.Parameters["@id_a"].Value = int.Parse(textBox4.Text);
            adaptador.UpdateCommand.Parameters["@nombre"].Value = textBox1.Text;
            adaptador.UpdateCommand.Parameters["@pass"].Value = textBox2.Text;
            adaptador.UpdateCommand.Parameters["@tipo"].Value = valorRadio;

            try
            {
                conexion.Open();
                adaptador.UpdateCommand.ExecuteNonQuery();
                MessageBox.Show("Se ha modificado con exito");
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }

            actualizarDatos();

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dataGridView1.CurrentRow.Index != -1)
            {
                textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                textBox2.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                valorRadio = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                textBox4.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            }

            if (valorRadio == "1")
            {
                radioButton1.Checked = true;
            }

            if (valorRadio == "2")
            {
                radioButton2.Checked = true;
            }

            if (valorRadio == "3")
            {
                radioButton3.Checked = true;
            }

            if (valorRadio == "4")
            {
                radioButton4.Checked = true;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
