﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class TipoPagoTarjeta : Form
    {
        public delegate void pasar4(string dato, string dato2, string dato3, string dato4, string dato5, string dato6);
        public event pasar4 pasado4;

        public TipoPagoTarjeta()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        DataTable dt = new DataTable();
        //String valorRadio;
        public static string variableGlobalIdTarjeta;
        float suma = 0f;

        private void TipoPagoTarjeta_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet31.marca_tarjeta' Puede moverla o quitarla según sea necesario.
            this.marca_tarjetaTableAdapter.Fill(this.baseDeDatosDataSet31.marca_tarjeta);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet31.nombre_tarjeta' Puede moverla o quitarla según sea necesario.
            this.nombre_tarjetaTableAdapter.Fill(this.baseDeDatosDataSet31.nombre_tarjeta);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet17.cantidad_cuota' Puede moverla o quitarla según sea necesario.
            this.cantidad_cuotaTableAdapter.Fill(this.baseDeDatosDataSet17.cantidad_cuota);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet17.tipo_tarjeta' Puede moverla o quitarla según sea necesario.
            this.tipo_tarjetaTableAdapter.Fill(this.baseDeDatosDataSet17.tipo_tarjeta);
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();



            conexion.Open();

            var cmd = new SqlCommand("SELECT max(idTarjeta) FROM tarjeta", conexion);

            var da = new SqlDataAdapter(cmd);

            da.Fill(dt);
            int padre;

            conexion.Close();

            padre = (int)dt.Rows[0][0];

            padre++;

            textBox1.Text = padre.ToString();

            variableGlobalIdTarjeta = textBox1.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            float varLocal = float.Parse(textBox5.Text);
            suma = suma + varLocal;
            label6.Text = suma.ToString();
            
            if (textBox5.Text != "")
            {
                pasado4(textBox1.Text, comboBox3.SelectedValue.ToString(), comboBox4.SelectedValue.ToString(), comboBox1.SelectedValue.ToString(), comboBox2.SelectedValue.ToString(), textBox5.Text);
            }


            /*
            SqlCommand alta = new SqlCommand("insert into tarjeta values (@nombreTarjeta, @marcaTarjeta, @importe, @idTipoTarjeta, @idCuota)", conexion);
            adaptador.InsertCommand = alta;
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombreTarjeta", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@marcaTarjeta", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@importe", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTipoTarjeta", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idCuota", SqlDbType.Int));

            adaptador.InsertCommand.Parameters["@nombreTarjeta"].Value = textBox2.Text;
            adaptador.InsertCommand.Parameters["@marcaTarjeta"].Value = textBox3.Text;
            adaptador.InsertCommand.Parameters["@importe"].Value = float.Parse(textBox5.Text);
            adaptador.InsertCommand.Parameters["@idTipoTarjeta"].Value = comboBox1.SelectedValue;
            adaptador.InsertCommand.Parameters["@idCuota"].Value = comboBox2.SelectedValue;

            try
            {
                conexion.Open();
                adaptador.InsertCommand.ExecuteNonQuery();
                MessageBox.Show("Se inserto Ok");
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }
            */
            this.Dispose();

        }
        

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedText == "CREDITO")
            {
                comboBox2.Enabled = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBox1.SelectedText == "CREDITO")
            {
                comboBox2.Enabled = false;
            }
            else
            {
                comboBox2.Enabled = true;
            }
        }
    }
}
