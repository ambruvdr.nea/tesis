﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class ConsultarArticuloFactura : Form
    {
        public delegate void pasar2(string dato, string dato2, string dato3);
        public event pasar2 pasado2;

        public ConsultarArticuloFactura()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        private DataTable datosTable;
        DataTable dt = new DataTable();

        private void ConsultarArticuloFactura_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            adaptador.SelectCommand = new SqlCommand("ListaArticulo2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art2", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art2"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView1.DataSource = datosTable;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            string variable = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string variable2 = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            string variable3 = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            textBox2.Text = variable;
            textBox3.Text = variable2;
            textBox5.Text = variable3;
            //idClienteAgregar = int.Parse(variable);
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pasado2(textBox2.Text, textBox3.Text, textBox5.Text);
            this.Dispose();
        }
    }
}
