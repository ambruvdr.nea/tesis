﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;


namespace SoftCars
{
    public partial class ReporteVentasPeriodos : Form
    {
        public ReporteVentasPeriodos()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;

        //private SqlDataAdapter adaptador;
        //private DataSet datos;
        //private DataTable datosTable;

        SqlCommand sqlcmd;
        SqlCommand sqlcmd2;
        SqlDataReader sqldr;

        private void ReporteMayorMenor_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            GraficosCategorias();
            CantidadDeVentas();
            TotalDeVentas();

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            string var = label3.Text;
            label3.Text = Decimal.Parse(var).ToString("c");
        }

        ArrayList Precio = new ArrayList();
        ArrayList Mes = new ArrayList();

        private void GraficosCategorias()
        {
            sqlcmd = new SqlCommand("TotalesPorMes", conexion);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            sqldr = sqlcmd.ExecuteReader();
            while (sqldr.Read())
            {
                Precio.Add(sqldr.GetDouble(0));
                Mes.Add(sqldr.GetString(1));
            }
            chart1.Series[0].Points.DataBindXY(Mes,Precio);
            sqldr.Close();
            conexion.Close();

            
            chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            
        }



        private void CantidadDeVentas()
        {
            sqlcmd = new SqlCommand("CantidadDeVentas", conexion);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlParameter total = new SqlParameter("@cantidadVentas", 0);
            total.Direction = ParameterDirection.Output;
            sqlcmd.Parameters.Add(total);
            conexion.Open();
            sqlcmd.ExecuteNonQuery();
            label4.Text = sqlcmd.Parameters["@cantidadVentas"].Value.ToString();
            conexion.Close();
        }

        private void TotalDeVentas()
        {
            sqlcmd2 = new SqlCommand("TotalDeVentas", conexion);
            sqlcmd2.CommandType = CommandType.StoredProcedure;
            SqlParameter total = new SqlParameter("@totalVentas", SqlDbType.Float);
            total.Direction = ParameterDirection.Output;
            sqlcmd2.Parameters.Add(total);
            conexion.Open();
            sqlcmd2.ExecuteNonQuery();
            label3.Text = sqlcmd2.Parameters["@totalVentas"].Value.ToString();
            conexion.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //PrintPreviewDialog.ActiveForm
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.Height = 800;
            printPreviewDialog1.Width = 900;
            printDocument1.DefaultPageSettings.Landscape = true;

            //printPreviewDialog1;
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string fecha = dateTimePicker1.Value.ToString("dd/MM/yyyy");

            Bitmap obj = new Bitmap(this.chart1.Width, this.chart1.Height);
            chart1.DrawToBitmap(obj, new Rectangle(0, 0, this.chart1.Width, this.chart1.Height));

            //1069; 382 this.dataGridView1.Width, this.dataGridView1.Height

            e.Graphics.DrawImage(obj, 100, 100);

            Font font = new Font("Century Gothic", 16);
            e.Graphics.DrawString("Reportes de Ventas del año en curso", font, Brushes.Black, 400, 20);
            e.Graphics.DrawString("Fecha: " + fecha, font, Brushes.Black, 950, 20);

            //printDocument1.DefaultPageSettings.Landscape = true;

            Image newImage = Image.FromFile("\\SoftCars_copia\\IMAGENES\\Logo2.jpg");
            PointF ulCorner = new PointF(10, 12);
            e.Graphics.DrawImage(newImage, ulCorner);
        }

        private void printPreviewDialog1_Load(object sender, EventArgs e)
        {

        }
    }
}
