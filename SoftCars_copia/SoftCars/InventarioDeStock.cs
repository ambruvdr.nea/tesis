﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;



namespace SoftCars
{
    public partial class InventarioDeStock : Form
    {
        public InventarioDeStock()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;

        //private SqlDataAdapter adaptador;
        //private DataSet datos;
        //private DataTable datosTable;

        private SqlCommand sqlcmd;
        private SqlDataReader sqldr;

        private void ReporteMayorMenor_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'BaseDeDatosDataSet33.TablaIventario' Puede moverla o quitarla según sea necesario.
            this.TablaIventarioTableAdapter.Fill(this.BaseDeDatosDataSet33.TablaIventario);
            // TODO: esta línea de código carga datos en la tabla 'BaseDeDatosDataSet30.articulo' Puede moverla o quitarla según sea necesario.
            this.articuloTableAdapter.Fill(this.BaseDeDatosDataSet30.articulo);
            conexion = new SqlConnection(ConexionDB.conexiondb);

            //GraficosTipoPago();
            this.reportViewer1.RefreshReport();
        }

        ArrayList PrecioTotal = new ArrayList();
        ArrayList TipoPago = new ArrayList();
        
        
        private void GraficosTipoPago()
        { /*
            
            sqlcmd = new SqlCommand("TotalPorTipoPago", conexion);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            sqldr = sqlcmd.ExecuteReader();
            while (sqldr.Read())
            {
                PrecioTotal.Add(sqldr.GetDouble(0));
                TipoPago.Add(sqldr.GetString(1));
            }
            chart1.Series[0].Points.DataBindXY(TipoPago, PrecioTotal);
            sqldr.Close();
            conexion.Close();

            /*
            chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            */
            
        }
         

        /*
        private void button4_Click(object sender, EventArgs e)
        {
            //PrintPreviewDialog.ActiveForm
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.Height = 800;
            printPreviewDialog1.Width = 900;
            printDocument1.DefaultPageSettings.Landscape = true;

            //printPreviewDialog1;
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string fecha = dateTimePicker1.Value.ToString("dd/MM/yyyy");

            Bitmap obj = new Bitmap(this.chart1.Width, this.chart1.Height);
            chart1.DrawToBitmap(obj, new Rectangle(0, 0, this.chart1.Width, this.chart1.Height));

            //1069; 382 this.dataGridView1.Width, this.dataGridView1.Height

            e.Graphics.DrawImage(obj, 100, 100);

            Font font = new Font("Century Gothic", 16);
            e.Graphics.DrawString("Reporte", font, Brushes.Black, 500, 20);
            e.Graphics.DrawString("Fecha: " + fecha, font, Brushes.Black, 950, 20);

            //printDocument1.DefaultPageSettings.Landscape = true;

            Image newImage = Image.FromFile("\\SoftCars_copia\\IMAGENES\\Logo2.jpg");
            PointF ulCorner = new PointF(10, 12);
            e.Graphics.DrawImage(newImage, ulCorner);
        }
        */
    }
}
