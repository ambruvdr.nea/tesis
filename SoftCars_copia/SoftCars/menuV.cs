﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace SoftCars
{
    public partial class menuV : Form
    {
        public menuV()
        {
            InitializeComponent();
        }


        //CODIGO PARA MOVER VENTANAS ------------------------------------------------------------------

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        //---------------------------------------------------------------------------------------------



        //BOTON DE MENU (IMAGEN) AGRANDA O ACHICA -----------------------------------------------------
        private void pbSlide_Click(object sender, EventArgs e)
        {
            if (pMenuVertical.Width == 250)
            {
                pMenuVertical.Width = 60;
            }
            else
            {
                pMenuVertical.Width = 250;
            }
        }
        //---------------------------------------------------------------------------------------------



        //BOTONES DE VENTANA (SUPERIORES) IMAGENES-----------------------------------------------------
        private void pbCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pbMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            pbRestaurar.Visible = true;
            pbMaximizar.Visible = false;
        }

        private void pbRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            pbRestaurar.Visible = false;
            pbMaximizar.Visible = true;
        }

        private void pbMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        //---------------------------------------------------------------------------------------------



        //LLAMADA AL CODIGO PARA MOVER VENTANA --------------------------------------------------------
        private void pContenedor_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void pBarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void pMenuVertical_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        //---------------------------------------------------------------------------------------------



        //METODO PARA LLAMAR FORMULARIOS DENTROS DEL PANEL --------------------------------------------

        private void AbrirFormEnPanel(object Formhijo)
        {
            if (this.pContenedor.Controls.Count > 0)
                this.pContenedor.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pContenedor.Controls.Add(fh);
            this.pContenedor.Tag = fh;
            fh.Show();
        }
        //----------------------------------------------------------------------------------------------



        //MENU DEL COSTADO (LOS BOTONES)----------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new Form3());
        }

        private void pMenuVertical_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new Form4());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new Form5());
        }

        private void pBarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new cambiarPassword());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new Form5());
        }

        private void pContenedor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void menuCompra_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            //AbrirFormEnPanel(new Logo());

            string var = FormLogin.global_usuario;
            label3.Text = var;

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new AltaVehiculo());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new ConsultaFactura());
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new ListadoCliente());
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new Presupuesto());
        }

        private void button1_Click_3(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new NotaDebito());
        }
    }
}
