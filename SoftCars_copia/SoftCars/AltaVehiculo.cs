﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class AltaVehiculo : Form
    {
        public AltaVehiculo()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        DataTable dt = new DataTable();
        //String valorRadio;


        private void AltaVehiculo_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.tipoVehiculo' Puede moverla o quitarla según sea necesario.
            this.tipoVehiculoTableAdapter.Fill(this.baseDeDatosDataSet10.tipoVehiculo);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.tipoCombustible' Puede moverla o quitarla según sea necesario.
            this.tipoCombustibleTableAdapter.Fill(this.baseDeDatosDataSet10.tipoCombustible);

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();

           //onexion.Open();

            SqlCommand alta = new SqlCommand("insert into vehiculo values (@dominioVehiculo, @marcaVehiculo, @modeloVehiculo, @anioVehiculo ,@idCliente ,@codCombustible ,@codigoVehiculo)", conexion);

            adaptador.InsertCommand = alta;
            //adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idCliente", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@dominioVehiculo", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@marcaVehiculo", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@modeloVehiculo", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@anioVehiculo", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idCliente", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@codCombustible", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@codigoVehiculo", SqlDbType.Int));

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" & textBox2.Text != "" & textBox3.Text != "")
            {

                adaptador.InsertCommand.Parameters["@dominioVehiculo"].Value = textBox1.Text;
                adaptador.InsertCommand.Parameters["@marcaVehiculo"].Value = textBox2.Text;
                adaptador.InsertCommand.Parameters["@modeloVehiculo"].Value = textBox3.Text;
                adaptador.InsertCommand.Parameters["@anioVehiculo"].Value = textBox6.Text;
                adaptador.InsertCommand.Parameters["@idCliente"].Value = int.Parse(abmClientes.variableGlobalCliente);
                adaptador.InsertCommand.Parameters["@codCombustible"].Value = comboBox2.SelectedValue;
                adaptador.InsertCommand.Parameters["@codigoVehiculo"].Value = comboBox1.SelectedValue;

                //adaptador.InsertCommand.Parameters["@idCliente"].Value = int.Parse(textBox11.Text);

                try
                {
                    conexion.Open();
                    adaptador.InsertCommand.ExecuteNonQuery();
                    MessageBox.Show("Se ha registrado el Vehículo exitosamente.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                    //textBox4.Text = "";
                    //textBox5.Text = "";
                    textBox6.Text = "";
                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }
                finally
                {
                    conexion.Close();
                }
            }
            else
            {
                MessageBox.Show("Debe completar todos los datos obligatorios.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            DialogResult boxResul;
            boxResul = MessageBox.Show("¿Desea salir sin registrar un vehículo para este cliente?", "Mensaje", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
            if (boxResul == DialogResult.Yes)
            {
                this.Dispose();
            }
            else
            if (boxResul == DialogResult.No)
            {
                return;
            }
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
    }
}
