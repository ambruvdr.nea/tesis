﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class AgregarLocalidad : Form
    {
        public AgregarLocalidad()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        private DataTable datosTable;
        DataTable dt = new DataTable();
        public static string variableGlobalCliente;

        private void AgregarLocalidad_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.provincia' Puede moverla o quitarla según sea necesario.
            this.provinciaTableAdapter.Fill(this.baseDeDatosDataSet10.provincia);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.localidad' Puede moverla o quitarla según sea necesario.
            this.localidadTableAdapter.Fill(this.baseDeDatosDataSet10.localidad);
            conexion = new SqlConnection(ConexionDB.conexiondb);


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {

                SqlCommand alta = new SqlCommand("insert into localidad values (@nombreLocalidad, @idProvincia)", conexion);
                adaptador = new SqlDataAdapter();

                adaptador.InsertCommand = alta;
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombreLocalidad", SqlDbType.VarChar));
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idProvincia", SqlDbType.Int));


                adaptador.InsertCommand.Parameters["@nombreLocalidad"].Value = textBox1.Text;
                adaptador.InsertCommand.Parameters["@idProvincia"].Value = comboBox2.SelectedValue;

                //adaptador.InsertCommand.Parameters["@idCliente"].Value = int.Parse(textBox11.Text);


                try
                {
                    conexion.Open();
                    adaptador.InsertCommand.ExecuteNonQuery();
                    MessageBox.Show("Se ha ingresado la localidad.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    textBox1.Text = "";
                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }
                finally
                {
                    conexion.Close();
                }


            }
            else
            {
                MessageBox.Show("Debe completar la localidad.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
    }
}
