﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class ConsultarCliente : Form
    {
        public ConsultarCliente()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        //String valorRadio;
        public static int idClienteModificar;
        private int bandera;

        private void button4_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                bandera = 0;

                //dataGridView2.Hide();
                dataGridView1.Columns.Clear();
                //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                

                adaptador.SelectCommand = new SqlCommand("PruebaDeConsulta", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@prueba", SqlDbType.VarChar);

                datosTable = new DataTable();
                adaptador.SelectCommand.Parameters["@prueba"].Value = textBox4.Text;
                adaptador.Fill(datosTable);
                dataGridView1.DataSource = datosTable;

                DataGridViewColumn column = dataGridView1.Columns[2];
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column3 = dataGridView1.Columns[3];
                column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                //DataGridViewColumn column3 = dataGridView1.Columns[5];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            }
            else
            {
                bandera = 1;

                //dataGridView2.Hide();
                dataGridView1.Columns.Clear();

                adaptador.SelectCommand = new SqlCommand("PruebaDeConsulta2", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@prueba2", SqlDbType.VarChar);

                datosTable = new DataTable();
                adaptador.SelectCommand.Parameters["@prueba2"].Value = textBox4.Text;
                adaptador.Fill(datosTable);
                dataGridView1.DataSource = datosTable;

                DataGridViewColumn column = dataGridView1.Columns[2];
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                DataGridViewColumn column3 = dataGridView1.Columns[3];
                column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }
        }

        private void actualizarDatos()
        {
            datos.Clear();
            adaptador.Fill(datos, "cliente");
        }

        private void ConsultarCliente_Load(object sender, EventArgs e)
        {

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string Variable = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            idClienteModificar = int.Parse(Variable);

            Form m = new modificarCliente();
            m.Show();
        }


        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (bandera == 1)
            {

                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                bandera = 0;
                textBox4.Text = "";
                textBox4.MaxLength = 50;
            }
            if (radioButton2.Checked)
            {
                bandera = 1;
                textBox4.Text = "";
                textBox4.MaxLength = 11;
            }
        }

    }
}
