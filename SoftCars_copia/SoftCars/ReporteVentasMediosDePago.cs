﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;



namespace SoftCars
{
    public partial class ReporteVentasMediosDePago : Form
    {
        public ReporteVentasMediosDePago()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;

        //private SqlDataAdapter adaptador;
        //private DataSet datos;
        //private DataTable datosTable;

        private SqlCommand sqlcmd;
        private SqlCommand sqlcmd2;
        private SqlDataReader sqldr;
        float efectivo;
        float tarjeta;
        float totalDeVentas = 0;
        float porcenEfvo = 0f; 
        int porcenTarj = 0;
        int efct = 0;
        //var precioTotal = new ArrayList();
        ArrayList tipoPago = new ArrayList();


        private void ReporteMayorMenor_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);

            GraficosTipoPago();

            TotalDeVentas();
            CantidadDeVentas();

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            string var = label3.Text;
            label3.Text = Decimal.Parse(var).ToString("c");

            /*
            conexion.Open();
            SqlCommand cmd2 = new SqlCommand("select sum(totalFactura) as EFECTIVO from venta where idTipoPago = 1", conexion);
            SqlDataReader sqldr4 = cmd2.ExecuteReader();
            sqldr4.Read();
            if (sqldr4.Read())
            {
                string col1Value = sqldr4[0].ToString();
                efectivo = float.Parse(col1Value);

            }

            SqlCommand cmd = new SqlCommand("select sum(totalFactura) as TARJETA from venta where idTipoPago = 2", conexion);
           SqlDataReader sqldr = cmd2.ExecuteReader();
            sqldr4.Read();
            if (sqldr4.Read())
            {
                string col1Value = sqldr4[0].ToString();
                tarjeta = float.Parse(col1Value);

            }
            conexion.Close();

            totalDeVentas = efectivo + tarjeta;
            porcenEfvo = efectivo / totalDeVentas;
            efct = (int)porcenEfvo;
            porcenTarj = 100 - efct;
            */

        }

        ArrayList efec = new ArrayList();
        ArrayList tipo = new ArrayList();


        private void GraficosTipoPago()
        {
        
            sqlcmd = new SqlCommand("TotalPorTipoPago", conexion);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            sqldr = sqlcmd.ExecuteReader();
            while (sqldr.Read())
            {
                efec.Add(sqldr.GetDouble(0));
                tipo.Add(sqldr.GetString(1));
            }
            
            
            chart1.Series[0].Points.DataBindXY(tipo, efec);
            sqldr.Close();
            //dr20.Close();
            conexion.Close();

            /*
            chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            */
            /*
            SqlDataAdapter adaptador6 = new SqlDataAdapter();
            adaptador6.SelectCommand = new SqlCommand("Efectivo", conexion);
            adaptador6.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador6.SelectCommand.Parameters.Add("@efec", SqlDbType.Int);

            DataTable datos10 = new DataTable();
            adaptador6.SelectCommand.Parameters["@efec"].Value = 1;
            adaptador6.Fill(datos10);
            dataGridView1.DataSource = datos10;

            //- Crear Array
            Object[] Datos = new Object[datos10.Rows.Count];

            for (int i = 0; i < datos10.Rows.Count; i++)
            {
                //- Guardar la Columna Nombre en el Arreglo
                Datos[i] = datos10.Rows[i]["EFECTIVO"].ToString();
            }

            SqlDataAdapter adaptador5 = new SqlDataAdapter();
            adaptador5.SelectCommand = new SqlCommand("Tarjeta12", conexion);
            adaptador5.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador5.SelectCommand.Parameters.Add("@tarj", SqlDbType.Int);

            DataTable datos5 = new DataTable();
            adaptador5.SelectCommand.Parameters["@tarj"].Value = 1;
            adaptador5.Fill(datos5);
            dataGridView1.DataSource = datos5;

            //- Crear Array
            Object[] Datos2 = new Object[datos5.Rows.Count];

            for (int i = 0; i < datos5.Rows.Count; i++)
            {
                //- Guardar la Columna Nombre en el Arreglo
                Datos2[i] = datos5.Rows[i]["TARJETA"].ToString();
            }



            chart1.Series[0].Points.DataBindXY(Datos, Datos2);


            //Object[] Datos2 = new Object[datos.Rows.Count];
            //Datos2[i] = datos.Rows[i]["CantidadVendidos"];
            */
        }

        private void CantidadDeVentas()
        {
            sqlcmd = new SqlCommand("CantidadDeVentas", conexion);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlParameter total = new SqlParameter("@cantidadVentas", 0);
            total.Direction = ParameterDirection.Output;
            sqlcmd.Parameters.Add(total);
            conexion.Open();
            sqlcmd.ExecuteNonQuery();
            label4.Text = sqlcmd.Parameters["@cantidadVentas"].Value.ToString();
            conexion.Close();
        }

        private void TotalDeVentas()
        {
            sqlcmd2 = new SqlCommand("TotalDeVentas", conexion);
            sqlcmd2.CommandType = CommandType.StoredProcedure;
            SqlParameter total = new SqlParameter("@totalVentas", SqlDbType.Float);
            total.Direction = ParameterDirection.Output;
            sqlcmd2.Parameters.Add(total);
            conexion.Open();
            sqlcmd2.ExecuteNonQuery();
            label3.Text = sqlcmd2.Parameters["@totalVentas"].Value.ToString();
            conexion.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //PrintPreviewDialog.ActiveForm
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.Height = 800;
            printPreviewDialog1.Width = 900;
            printDocument1.DefaultPageSettings.Landscape = true;

            //printPreviewDialog1;
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string fecha = dateTimePicker1.Value.ToString("dd/MM/yyyy");

            Bitmap obj = new Bitmap(this.chart1.Width, this.chart1.Height);
            chart1.DrawToBitmap(obj, new Rectangle(0, 0, this.chart1.Width, this.chart1.Height));

            //1069; 382 this.dataGridView1.Width, this.dataGridView1.Height

            e.Graphics.DrawImage(obj, 100, 100);

            Font font = new Font("Century Gothic", 16);
            e.Graphics.DrawString("Comparación de Ventas por Medios de Pago", font, Brushes.Black, 400, 20);
            e.Graphics.DrawString("Fecha: " + fecha, font, Brushes.Black, 950, 20);

            //printDocument1.DefaultPageSettings.Landscape = true;

            Image newImage = Image.FromFile("\\SoftCars_copia\\IMAGENES\\Logo2.jpg");
            PointF ulCorner = new PointF(10, 12);
            e.Graphics.DrawImage(newImage, ulCorner);
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //DateTimePicker3.Format = DateTimePickerFormat.Custom;
            //DateTimePicker3.CustomFormat = "dd/MM/yyyy";
            //string a = dateTimePicker3.Value.ToShortDateString();
            //DateTime lafec = DateTime.Parse(a);

            //string b = dateTimePicker3.Value.ToShortDateString();
            //DateTime lafec2 = DateTime.Parse(b);

            try
            {
                conexion.Open();
                SqlCommand cmd20 = new SqlCommand("select sum(venta.totalFactura) as PrecioTotal, (tipo_pago.nombreTipo) as TipoPago " +
                                                  "from venta, tipo_pago " +
                                                  "where venta.idTipoPago = tipo_pago.idTipoPago " +
                                                  "and venta.idTipoPago between 1 and 2 " +
                                                  "and venta.fecha >= "+ dateTimePicker3.Value.ToShortDateString() + ""+
                                                  "and venta.fecha <= "+ dateTimePicker2.Value.ToShortDateString() + ""+
                                                  "group by tipo_pago.nombreTipo", conexion);

                SqlDataReader dr20 = cmd20.ExecuteReader();

                while (dr20.Read())
                {
                    efec.Add(dr20.GetDouble(0));
                    tipo.Add(dr20.GetString(1));
                }

                chart1.Series[0].Points.DataBindXY(tipo, efec);
                //sqldr.Close();
                dr20.Close();
                //conexion.Close();
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }
        }
    }
}
