﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace SoftCars
{
    public partial class menuRecepcionista : Form
    {
        public menuRecepcionista()
        {
            InitializeComponent();
        }


        //CODIGO PARA MOVER VENTANAS ------------------------------------------------------------------

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        //---------------------------------------------------------------------------------------------



        //BOTON DE MENU (IMAGEN) AGRANDA O ACHICA -----------------------------------------------------
        private void pbSlide_Click(object sender, EventArgs e)
        {

            if (MenuVertical.Width == 247)
            {
                MenuVertical.Width = 50;
                pictureBox2.Hide();
            }
            else
            {
                MenuVertical.Width = 247;
                pictureBox2.Visible = true;
            }
        }
        //---------------------------------------------------------------------------------------------



        //BOTONES DE VENTANA (SUPERIORES) IMAGENES-----------------------------------------------------
        private void pbCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pbMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            pbRestaurar.Visible = true;
            pbMaximizar.Visible = false;
        }

        private void pbRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            pbRestaurar.Visible = false;
            pbMaximizar.Visible = true;
        }

        private void pbMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        //---------------------------------------------------------------------------------------------



        //LLAMADA AL CODIGO PARA MOVER VENTANA --------------------------------------------------------
        private void pContenedor_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void pBarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void pMenuVertical_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        //---------------------------------------------------------------------------------------------



        //METODO PARA LLAMAR FORMULARIOS DENTROS DEL PANEL --------------------------------------------

        private void AbrirFormEnPanel(object Formhijo)
        {
            if (this.pContenedor.Controls.Count > 0)
                this.pContenedor.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pContenedor.Controls.Add(fh);
            this.pContenedor.Tag = fh;
            fh.Show();
        }
        //----------------------------------------------------------------------------------------------



        //MENU DEL COSTADO (LOS BOTONES)----------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new Form3());
        }

        private void pMenuVertical_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pBarraTitulo_Paint(object sender, PaintEventArgs e)
        {
            //ControlPaint.DrawBorder(e.Graphics, this.pBarraTitulo.ClientRectangle, Color.Black, ButtonBorderStyle.Solid);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new cambiarPassword());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new abmClientes());
        }

        private void pContenedor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void menuCompra_Load(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
            //AbrirFormEnPanel(new Logo());

            string nom = FormLogin.global_usuario;
            int tipo = FormLogin.tipo_usuario;
            label3.Text = nom;

            //No hacen falta los IF. Por las dudas pase un usuario que no es mientras programo

            if (tipo == 1)
            {
                label2.Text = "Administrador";
            }

            if (tipo == 2)
            {
                label2.Text = "Recepcionista";
            }

            if (tipo == 3)
            {
                label2.Text = "Encargado de Ventas";
            }

            if (tipo == 4)
            {
                label2.Text = "Encargado de Compras";
            }


        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new AltaVehiculo());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new ConsultarCliente());
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new ListadoCliente());
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new AltaVehiculo());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new ConsultaFactura());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new Presupuesto());
        }

        private void button8_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new ConsultarPresupuesto());
        }
    }
}
