﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class ConsultaFactura : Form
    {
        public ConsultaFactura()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private SqlDataAdapter adaptador2;
        private SqlDataAdapter adaptador3;
        private DataTable datos;
        private DataTable datos2;
        private DataTable datos3;
        public static int idDeVentaGrid;

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void ListadoCliente_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.cliente' Puede moverla o quitarla según sea necesario.
            this.clienteTableAdapter.Fill(this.baseDeDatosDataSet10.cliente);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.tipoFactura' Puede moverla o quitarla según sea necesario.
            this.tipoFacturaTableAdapter.Fill(this.baseDeDatosDataSet10.tipoFactura);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.localidad' Puede moverla o quitarla según sea necesario.
            this.localidadTableAdapter.Fill(this.baseDeDatosDataSet10.localidad);
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            adaptador2 = new SqlDataAdapter();
            adaptador2.SelectCommand = new SqlCommand("listadoPorLocalidad", conexion);
            adaptador2.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador2.SelectCommand.Parameters.Add("@idLocalidad", SqlDbType.Int);

            /*
            datos2 = new DataTable();
            adaptador2.SelectCommand.Parameters["@idLocalidad"].Value = comboBox1.SelectedValue;
            adaptador2.Fill(datos2);
            dataGridView1.DataSource = datos2;
            */
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("consultaDeFactura3", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoDeFactura3", SqlDbType.VarChar);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoDeFactura3"].Value = textBox2.Text+"-"+textBox3.Text;
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;

                //DataGridViewColumn column = dataGridView1.Columns[0];
                //column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column1 = dataGridView1.Columns[1];
                column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column2 = dataGridView1.Columns[2];
                column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column5 = dataGridView1.Columns[5];
                //column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column6 = dataGridView1.Columns[6];
                //column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            }

            if (radioButton2.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("consultaDeFactura", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoDeFactura", SqlDbType.Int);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoDeFactura"].Value = comboBox2.SelectedValue;
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;

                DataGridViewColumn column = dataGridView1.Columns[0];
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column1 = dataGridView1.Columns[1];
                //column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column2 = dataGridView1.Columns[2];
                column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column5 = dataGridView1.Columns[5];
                //column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column6 = dataGridView1.Columns[6];
                //column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            }

            if (radioButton3.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("consultaDeFactura2", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoDeFactura2", SqlDbType.VarChar);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoDeFactura2"].Value = textBox1.Text;
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;

                DataGridViewColumn column = dataGridView1.Columns[0];
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column1 = dataGridView1.Columns[1];
                //column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column2 = dataGridView1.Columns[2];
                //column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column3 = dataGridView1.Columns[3];
                column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column5 = dataGridView1.Columns[5];
                //column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column6 = dataGridView1.Columns[6];
                //column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            adaptador3 = new SqlDataAdapter();
            adaptador3.SelectCommand = new SqlCommand("listadoPorNombreCliente", conexion);
            adaptador3.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador3.SelectCommand.Parameters.Add("@idNombreCliente", SqlDbType.VarChar);

            datos3 = new DataTable();
            adaptador3.SelectCommand.Parameters["@idNombreCliente"].Value = textBox1.Text;
            adaptador3.Fill(datos3);
            dataGridView1.DataSource = datos3;
        }

        private void button4_Click_1(object sender, EventArgs e)
        {

            Form c = new Form7();
            c.Show();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string variable = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            idDeVentaGrid = int.Parse(variable);

        }
    }
}
