﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class VerRemitoIngreso : Form
    {
        public VerRemitoIngreso()
        {
            InitializeComponent();
        }
        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        private DataTable datosTable;
        public static int idVerRemitoIngresoGrid;


        private void VerRemitoIngreso_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;

            adaptador.SelectCommand = new SqlCommand("ConsultarDetalleRemito", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@idRemito", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@idRemito"].Value = ConsultarRemitoIngreso.idDetalleRemitoIngresoGrid;
            adaptador.Fill(datosTable);
            dataGridView1.DataSource = datosTable;
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT r.numeroRemitoIngreso," +
                " p.razonSocialProveedor," +
                " r.fechaRemitoIngreso," +
                " p.cuilProveedor " +
                " FROM  remito_ingreso r, proveedor p" +
                " WHERE p.idProveedor = r.idProveedor" +
                " and r.idRemitoIngreso = '" + ConsultarRemitoIngreso.idDetalleRemitoIngresoGrid + "'", conexion);
            SqlDataReader dr = cmd.ExecuteReader();


            if (dr.Read())
            {
                //numeroRemitoIngreso
                string col1Value = dr[0].ToString();
                textBox3.Text = col1Value;
                //RazonSocialProveedor
                string col2Value = dr[1].ToString();
                textBox1.Text = col2Value;
                //fecha
                string col4Value = dr[2].ToString();
                textBox4.Text = col4Value;
                //cuil
                string col3Value = dr[3].ToString();
                textBox2.Text = col3Value;
            }
            conexion.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }



        private void button2_Click_1(object sender, EventArgs e)
        {
            idVerRemitoIngresoGrid = ConsultarRemitoIngreso.idDetalleRemitoIngresoGrid;
            Form c = new ModificarRemitoIngreso();
            c.Show();
        }
    }
}
