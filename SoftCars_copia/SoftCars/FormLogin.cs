﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace SoftCars
{

    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        public static string global_usuario;
        public static int id_global_usuario;
        public static int tipo_usuario;



        //CODIGO PARA MOVER VENTANAS ------------------------------------------------------------------

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        //---------------------------------------------------------------------------------------------



        /*
        //CUANDO ESCRIBO CODIGO (TEXTBOX)--------------------------------------------------------------
        private void txtUser_Enter(object sender, EventArgs e)
        {
            
            if (textBox1.Text == "USUARIO")
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.DimGray;
            }
            

        }

        private void txtUser_Leave(object sender, EventArgs e)
        {
            
            if (textBox1.Text == "")
            {
                textBox1.Text = "USUARIO";
                textBox1.ForeColor = Color.DimGray;
            }
            
        }

        private void txtPass_Enter(object sender, EventArgs e)
        {
            
            if (textBox2.Text == "CONTRASEÑA")
            {
                textBox2.Text = "";
                textBox2.ForeColor = Color.DimGray;
                textBox2.UseSystemPasswordChar = true;
            }
            
        }

        private void txtPass_Leave(object sender, EventArgs e)
        {
            
            if (textBox2.Text == "")
            {
                textBox2.Text = "CONTRASEÑA";
                textBox2.ForeColor = Color.DimGray;
                textBox2.UseSystemPasswordChar = false;
            }
            
        }
        //---------------------------------------------------------------------------------------------
        */


        //BOTONES DE VENTANA (SUPERIORES) IMAGENES-----------------------------------------------------
        private void pbCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pbMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        //---------------------------------------------------------------------------------------------





        //LLAMADA AL CODIGO PARA MOVER VENTANA --------------------------------------------------------
        private void FormLogin_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        //---------------------------------------------------------------------------------------------


        public void logins()
        {



            try
            {

                conexion.Open();
                SqlCommand cmd = new SqlCommand("SELECT nombre_usuario, password, id_tipo_usuario, id_usuario FROM usuario WHERE nombre_usuario='" + textBox1.Text + "' AND password='" + textBox2.Text + "'", conexion);

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                { 
 

                        //Form Menu = new menuCompra();
                        //Menu.Show();
                        
                   global_usuario = textBox1.Text;

                   adaptador.SelectCommand = new SqlCommand("TomarTipoUs", conexion);
                   adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                   adaptador.SelectCommand.Parameters.Add("@tomar", SqlDbType.Int);

                   adaptador.SelectCommand.Parameters["@tomar"].Value = tipo_usuario;

                   string col1Value = dr[2].ToString();
                   tipo_usuario = int.Parse(col1Value);

                   string col4Value = dr[3].ToString();
                   id_global_usuario = int.Parse(col4Value);


                }
                else
                    {
                        MessageBox.Show("Usuario o Password incorrecto.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        this.Dispose();
                    }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                conexion.Close();
            }


        }

        public void loguear(string usuario, string contraseña)
        {

        }


        //BOTON PARA ENTRAR ---------------------------------------------------------------------------
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" & textBox2.Text != "")
            {
                logins();

                if (tipo_usuario == 2)
                {

                    global_usuario = textBox1.Text;

                    Form f = new menuRecepcionista();
                    f.Show();

                }

                if (tipo_usuario == 3)
                {

                    global_usuario = textBox1.Text;

                    Form f = new MenuVenta();
                    f.Show();

                }

                if (tipo_usuario == 4)
                {

                    global_usuario = textBox1.Text;

                    Form f = new EncargadoDeCompra();
                    f.Show();

                }

                if (tipo_usuario == 1)
                {

                    global_usuario = textBox1.Text;

                    Form f = new Form2();
                    f.Show();

                }

                this.Visible = false;

            }
            else
            {
                MessageBox.Show("Complete los campos de texto vacios", "ERROR");
            }

            /*
            if (string.IsNullOrEmpty(textBox1.Text) || string.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show("Por favor ingrese datos");
            }
            else
            {
                try
                {
                    conexion.Open();
                    SqlCommand consulta = new SqlCommand("select * from usuario where nombre_usuario ="+textBox1.Text+"and password ="+textBox2.Text, conexion);


                    SqlDataReader dr = consulta.ExecuteReader();
                    if (dr.Read())
                    {
                        Form VentanaPrincipal = new Form2();
                        VentanaPrincipal.Show();
                    }

                    else
                    {
                        MessageBox.Show("Usuario o Password incorrecto");
                    }

                    //adaptador.SelectCommand.ExecuteNonQuery();
                    //MessageBox.Show("Se ha ingresado con exito");
                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }
                finally
                {
                    conexion.Close();
                }
            }

    */
        }
        //---------------------------------------------------------------------------------------------
        private void FormLogin_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();

            //SqlCommand consulta = new SqlCommand("select * from usuario where nombre_usuario = @nombre and password = @pass", conexion);

            // adaptador.SelectCommand = consulta;
            // adaptador.SelectCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
            // adaptador.SelectCommand.Parameters.Add(new SqlParameter("@pass", SqlDbType.VarChar));

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
