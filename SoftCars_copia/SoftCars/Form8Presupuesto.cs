﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace SoftCars
{
    public partial class Form8Presupuesto : Form
    {
        public Form8Presupuesto()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataTable datos;

        private string dni;
        private string nombre;
        private string domicilio;
        private string localidad;
        private string fechaEmision;
        private string fechaVencimiento;
        private string totalP;
        private string usuario;
        private string idP;
        private string dias;
        private string numPresup;
        private string modoDePago;
        private string porcDesc;
        private string preDesc;

        private void Form8_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'BaseDeDatosDataSet25.detalle_presupuesto' Puede moverla o quitarla según sea necesario.
            this.detalle_presupuestoTableAdapter.Fill(this.BaseDeDatosDataSet25.detalle_presupuesto);

            this.reportViewer1.RefreshReport();
            
            string varP = Presupuesto.idDePresupuesto.ToString();

            ReportParameter peP = new ReportParameter("ReportParameterIDP", varP);
            reportViewer1.LocalReport.SetParameters(peP);
            reportViewer1.RefreshReport();

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();

            String elIdDePresupuesto = Presupuesto.idDePresupuesto.ToString();
            textBox1.Text = elIdDePresupuesto;

            adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = new SqlCommand("rellenoPresupuesto", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@rellenoIdPre", SqlDbType.Int);

            datos = new DataTable();
            adaptador.SelectCommand.Parameters["@rellenoIdPre"].Value = int.Parse(textBox1.Text);
            adaptador.Fill(datos);
            dataGridView1.DataSource = datos;

            //TODO ENCABEZADO
            dni = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            nombre = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            domicilio = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            localidad = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            fechaEmision = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            fechaVencimiento = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            totalP = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            usuario = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            idP = dataGridView1.CurrentRow.Cells[8].Value.ToString();
            dias = dataGridView1.CurrentRow.Cells[9].Value.ToString();
            numPresup = dataGridView1.CurrentRow.Cells[10].Value.ToString();
            modoDePago = dataGridView1.CurrentRow.Cells[11].Value.ToString();
            porcDesc = dataGridView1.CurrentRow.Cells[12].Value.ToString();
            preDesc = dataGridView1.CurrentRow.Cells[13].Value.ToString();

            ReportParameterCollection rp2 = new ReportParameterCollection();
            rp2.Add(new ReportParameter("ReportParameterDNIp", dni));
            rp2.Add(new ReportParameter("ReportParameterNOMBREp", nombre));
            rp2.Add(new ReportParameter("ReportParameterDOMICILIOp", domicilio));
            rp2.Add(new ReportParameter("ReportParameterLOCALIDADp", localidad));
            rp2.Add(new ReportParameter("ReportParameterFECHApEMI", fechaEmision));
            rp2.Add(new ReportParameter("ReportParameterFECHApVEN", fechaVencimiento));
            rp2.Add(new ReportParameter("ReportParameterTOTALp", totalP));
            rp2.Add(new ReportParameter("ReportParameterUSUARIOp", usuario));
            rp2.Add(new ReportParameter("ReportParameterDIASp", dias));
            rp2.Add(new ReportParameter("ReportParameterNUMEROp", numPresup));
            rp2.Add(new ReportParameter("ReportParameterMODOPAGOp", modoDePago));
            rp2.Add(new ReportParameter("ReportParameterPORCENTAJEp", porcDesc));
            rp2.Add(new ReportParameter("ReportParameterPORCENTAJEMONTp", preDesc));
            this.reportViewer1.LocalReport.SetParameters(rp2);
            this.reportViewer1.RefreshReport();

        }
    }
}
