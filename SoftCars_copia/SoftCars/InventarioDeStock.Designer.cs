﻿namespace SoftCars
{
    partial class InventarioDeStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.articuloBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BaseDeDatosDataSet30 = new SoftCars.BaseDeDatosDataSet30();
            this.TablaIventarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BaseDeDatosDataSet33 = new SoftCars.BaseDeDatosDataSet33();
            this.label1 = new System.Windows.Forms.Label();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.articuloTableAdapter = new SoftCars.BaseDeDatosDataSet30TableAdapters.articuloTableAdapter();
            this.TablaIventarioTableAdapter = new SoftCars.BaseDeDatosDataSet33TableAdapters.TablaIventarioTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.articuloBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BaseDeDatosDataSet30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaIventarioBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BaseDeDatosDataSet33)).BeginInit();
            this.SuspendLayout();
            // 
            // articuloBindingSource
            // 
            this.articuloBindingSource.DataMember = "articulo";
            this.articuloBindingSource.DataSource = this.BaseDeDatosDataSet30;
            // 
            // BaseDeDatosDataSet30
            // 
            this.BaseDeDatosDataSet30.DataSetName = "BaseDeDatosDataSet30";
            this.BaseDeDatosDataSet30.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TablaIventarioBindingSource
            // 
            this.TablaIventarioBindingSource.DataMember = "TablaIventario";
            this.TablaIventarioBindingSource.DataSource = this.BaseDeDatosDataSet33;
            // 
            // BaseDeDatosDataSet33
            // 
            this.BaseDeDatosDataSet33.DataSetName = "BaseDeDatosDataSet33";
            this.BaseDeDatosDataSet33.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoEllipsis = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20.25F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(350, 51);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(407, 47);
            this.label1.TabIndex = 38;
            this.label1.Text = "Inventario de Stock";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = System.Windows.Forms.AnchorStyles.None;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.articuloBindingSource;
            reportDataSource2.Name = "DataSet2";
            reportDataSource2.Value = this.TablaIventarioBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "SoftCars.InventarioStock.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(94, 154);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(950, 490);
            this.reportViewer1.TabIndex = 39;
            // 
            // articuloTableAdapter
            // 
            this.articuloTableAdapter.ClearBeforeFill = true;
            // 
            // TablaIventarioTableAdapter
            // 
            this.TablaIventarioTableAdapter.ClearBeforeFill = true;
            // 
            // InventarioDeStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(1150, 705);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "InventarioDeStock";
            this.Text = "Reporte de Mayor a Menor";
            this.Load += new System.EventHandler(this.ReporteMayorMenor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.articuloBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BaseDeDatosDataSet30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TablaIventarioBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BaseDeDatosDataSet33)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource articuloBindingSource;
        private BaseDeDatosDataSet30 BaseDeDatosDataSet30;
        private BaseDeDatosDataSet30TableAdapters.articuloTableAdapter articuloTableAdapter;
        private System.Windows.Forms.BindingSource TablaIventarioBindingSource;
        private BaseDeDatosDataSet33 BaseDeDatosDataSet33;
        private BaseDeDatosDataSet33TableAdapters.TablaIventarioTableAdapter TablaIventarioTableAdapter;
    }
}