﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class ConsultaNotaCredito : Form
    {
        public ConsultaNotaCredito()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private SqlDataAdapter adaptador2;
        private SqlDataAdapter adaptador3;
        private DataTable datos;
        private DataTable datos2;
        private DataTable datos3;
        public static int idDeNC;

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void ListadoCliente_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.cliente' Puede moverla o quitarla según sea necesario.
            this.clienteTableAdapter.Fill(this.baseDeDatosDataSet10.cliente);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.tipoFactura' Puede moverla o quitarla según sea necesario.
            this.tipoFacturaTableAdapter.Fill(this.baseDeDatosDataSet10.tipoFactura);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.localidad' Puede moverla o quitarla según sea necesario.
            this.localidadTableAdapter.Fill(this.baseDeDatosDataSet10.localidad);
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;

        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("consultaDeNotaCredito", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoDeNotaCredito", SqlDbType.VarChar);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoDeNotaCredito"].Value = textBox2.Text + "-" + textBox3.Text;
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;
            }

            if (radioButton3.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("consultaDeNotaCredito2", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoDeNotaCredito2", SqlDbType.VarChar);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoDeNotaCredito2"].Value = textBox1.Text;
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;

                //DataGridViewColumn column = dataGridView1.Columns[0];
                //column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column1 = dataGridView1.Columns[1];
                column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column2 = dataGridView1.Columns[2];
                //column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column4 = dataGridView1.Columns[4];
                column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column5 = dataGridView1.Columns[5];
                //column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column6 = dataGridView1.Columns[6];
                //column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }

            if (radioButton2.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("consultaDeNotaCredito3", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoDeNotaCredito3a", SqlDbType.DateTime);
                adaptador.SelectCommand.Parameters.Add("@listadoDeNotaCredito3b", SqlDbType.DateTime);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoDeNotaCredito3a"].Value = dateTimePicker1.Value;
                adaptador.SelectCommand.Parameters["@listadoDeNotaCredito3b"].Value = dateTimePicker2.Value;
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;

                //DataGridViewColumn column = dataGridView1.Columns[0];
                //column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column1 = dataGridView1.Columns[1];
                //column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column2 = dataGridView1.Columns[2];
                column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column5 = dataGridView1.Columns[5];
                column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column6 = dataGridView1.Columns[6];
                //column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }



        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            string variable;
               
            if (radioButton4.Checked)
            {
                variable = dataGridView1.CurrentRow.Cells[5].Value.ToString();
                idDeNC = int.Parse(variable);
            }

            if (radioButton3.Checked)
            {
                variable = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                idDeNC = int.Parse(variable);
            }

            if (radioButton2.Checked)
            {
                variable = dataGridView1.CurrentRow.Cells[5].Value.ToString();
                idDeNC = int.Parse(variable);
            }

            Form c = new Form10();
            c.Show();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            /*
            string variable = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            idDeVentaGrid = int.Parse(variable);
            */
        }
    }
}
