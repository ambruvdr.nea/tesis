﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace SoftCars
{
    public partial class cambiarPassword : Form
    {
        public cambiarPassword()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador, adaptador2;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        //String valorRadio;

        

        private void cambiarPassword_Load(object sender, EventArgs e)
        {

            textBox4.Text = FormLogin.global_usuario;   

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            adaptador2 = new SqlDataAdapter();


            SqlCommand consulta = new SqlCommand("select password from usuario where nombre_usuario='"+textBox4.Text+"'", conexion);
            conexion.Open();
            SqlDataReader registro = consulta.ExecuteReader();

            if (textBox1.Text != "" & textBox2.Text != "" & textBox3.Text != "")
            {

                Regex r = new Regex(@"^[a-zA-Z][\w]*$");

                //bool isMatch = Regex.IsMatch(textBox2.Text, @"");
                //Regex.Match(textBox2.Text, @"\s");
            
               if (Regex.IsMatch(textBox2.Text, @"[a-zA-Z]") & Regex.IsMatch(textBox2.Text, @"[0-9]"))
               {
                    if (textBox2.TextLength < 6)
                    {
                        MessageBox.Show("La contraseña no cumple las políticas de seguridad. Su contraseña debe tener 6 caracteres como mínimo.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    else
                    {
                       
                        if (textBox2.Text == textBox3.Text)
                        {



                            if (registro.Read())
                            {
                                string passwordActualDB = registro["password"].ToString();
                                if (textBox1.Text == passwordActualDB)
                                {


                                    if (textBox1.Text == passwordActualDB & textBox2.Text == textBox3.Text)
                                    {
                                        SqlCommand modificacion = new SqlCommand("update usuario set password = @pass where nombre_usuario ='" + textBox4.Text + "'", conexion);
                                        adaptador2.UpdateCommand = modificacion;

                                        adaptador2.UpdateCommand.Parameters.Add(new SqlParameter("@pass", SqlDbType.VarChar));

                                        adaptador2.UpdateCommand.Parameters["@pass"].Value = textBox2.Text;

                                        try
                                        {
                                            conexion.Close();
                                            conexion.Open();
                                            adaptador2.UpdateCommand.ExecuteNonQuery();
                                            MessageBox.Show("Se ha modificado la contraseña con exito", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                            textBox1.Text = "";
                                            textBox2.Text = "";
                                            textBox3.Text = "";

                                        }
                                        catch (SqlException excepcion)
                                        {
                                            MessageBox.Show(excepcion.ToString());

                                        }
                                        finally
                                        {
                                            conexion.Close();
                                        }
                                    }

                                }
                                else
                                {
                                    MessageBox.Show("Password incorrecto.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                }

                            }  

                        }
                        else
                        {
                            MessageBox.Show("No coinciden los campos de la nueva contraseña que escribió.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }

               }
               else
               {
                    MessageBox.Show("La contraseña no cumple las políticas de seguridad. Su contraseña debe ser Alfanumerica.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Hand);
               }



            }
            else
            {
                MessageBox.Show("Ingrese todos los datos", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
           
    }
}
