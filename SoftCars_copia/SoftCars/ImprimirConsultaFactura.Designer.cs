﻿namespace SoftCars
{
    partial class ImprimirConsultaFactura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.baseDeDatosDataSet10 = new SoftCars.BaseDeDatosDataSet10();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.DataSetReporteFactura = new SoftCars.DataSetReporteFactura();
            this.PruebaDeConsultaFacturaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PruebaDeConsultaFacturaTableAdapter = new SoftCars.DataSetReporteFacturaTableAdapters.PruebaDeConsultaFacturaTableAdapter();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.baseDeDatosDataSet10BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.baseDeDatosDataSet201 = new SoftCars.BaseDeDatosDataSet20();
            ((System.ComponentModel.ISupportInitialize)(this.baseDeDatosDataSet10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetReporteFactura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PruebaDeConsultaFacturaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDeDatosDataSet10BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDeDatosDataSet201)).BeginInit();
            this.SuspendLayout();
            // 
            // baseDeDatosDataSet10
            // 
            this.baseDeDatosDataSet10.DataSetName = "BaseDeDatosDataSet10";
            this.baseDeDatosDataSet10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "SoftCars.consultarFactura.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(54, 349);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(837, 417);
            this.reportViewer1.TabIndex = 0;
            this.reportViewer1.Load += new System.EventHandler(this.reportViewer1_Load);
            // 
            // DataSetReporteFactura
            // 
            this.DataSetReporteFactura.DataSetName = "DataSetReporteFactura";
            this.DataSetReporteFactura.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PruebaDeConsultaFacturaBindingSource
            // 
            this.PruebaDeConsultaFacturaBindingSource.DataMember = "PruebaDeConsultaFactura";
            this.PruebaDeConsultaFacturaBindingSource.DataSource = this.DataSetReporteFactura;
            // 
            // PruebaDeConsultaFacturaTableAdapter
            // 
            this.PruebaDeConsultaFacturaTableAdapter.ClearBeforeFill = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1274, 634);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 1;
            // 
            // baseDeDatosDataSet10BindingSource
            // 
            this.baseDeDatosDataSet10BindingSource.DataSource = this.baseDeDatosDataSet10;
            this.baseDeDatosDataSet10BindingSource.Position = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(1361, 523);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(768, 69);
            this.dataGridView1.TabIndex = 2;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(45, 153);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(743, 137);
            this.dataGridView2.TabIndex = 3;
            this.dataGridView2.Visible = false;
            // 
            // baseDeDatosDataSet201
            // 
            this.baseDeDatosDataSet201.DataSetName = "BaseDeDatosDataSet20";
            this.baseDeDatosDataSet201.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ImprimirConsultaFactura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1419, 766);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "ImprimirConsultaFactura";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ImprimirConsultaFactura";
            this.Load += new System.EventHandler(this.ImprimirConsultaFactura_Load);
            ((System.ComponentModel.ISupportInitialize)(this.baseDeDatosDataSet10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetReporteFactura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PruebaDeConsultaFacturaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDeDatosDataSet10BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDeDatosDataSet201)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource PruebaDeConsultaFacturaBindingSource;
        private DataSetReporteFactura DataSetReporteFactura;
        private DataSetReporteFacturaTableAdapters.PruebaDeConsultaFacturaTableAdapter PruebaDeConsultaFacturaTableAdapter;
        private System.Windows.Forms.TextBox textBox1;
        private BaseDeDatosDataSet10 baseDeDatosDataSet10;
        private System.Windows.Forms.BindingSource baseDeDatosDataSet10BindingSource;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private BaseDeDatosDataSet20 baseDeDatosDataSet201;
    }
}