﻿namespace SoftCars
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.detalle_ventaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BaseDeDatosDataSet4 = new SoftCars.BaseDeDatosDataSet4();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.detalle_ventaTableAdapter = new SoftCars.BaseDeDatosDataSet4TableAdapters.detalle_ventaTableAdapter();
            this.baseDeDatosDataSet7 = new SoftCars.BaseDeDatosDataSet7();
            this.baseDeDatosDataSet7BindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.detalle_ventaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BaseDeDatosDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDeDatosDataSet7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDeDatosDataSet7BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // detalle_ventaBindingSource
            // 
            this.detalle_ventaBindingSource.DataMember = "detalle_venta";
            this.detalle_ventaBindingSource.DataSource = this.BaseDeDatosDataSet4;
            // 
            // BaseDeDatosDataSet4
            // 
            this.BaseDeDatosDataSet4.DataSetName = "BaseDeDatosDataSet4";
            this.BaseDeDatosDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.baseDeDatosDataSet7BindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "SoftCars.Report1.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(25, 41);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(618, 269);
            this.reportViewer1.TabIndex = 0;
            // 
            // detalle_ventaTableAdapter
            // 
            this.detalle_ventaTableAdapter.ClearBeforeFill = true;
            // 
            // baseDeDatosDataSet7
            // 
            this.baseDeDatosDataSet7.DataSetName = "BaseDeDatosDataSet7";
            this.baseDeDatosDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // baseDeDatosDataSet7BindingSource
            // 
            this.baseDeDatosDataSet7BindingSource.DataSource = this.baseDeDatosDataSet7;
            this.baseDeDatosDataSet7BindingSource.Position = 0;
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 531);
            this.Controls.Add(this.reportViewer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form6";
            this.Text = "Form6";
            this.Load += new System.EventHandler(this.Form6_Load);
            ((System.ComponentModel.ISupportInitialize)(this.detalle_ventaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BaseDeDatosDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDeDatosDataSet7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baseDeDatosDataSet7BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource detalle_ventaBindingSource;
        private BaseDeDatosDataSet4 BaseDeDatosDataSet4;
        private BaseDeDatosDataSet4TableAdapters.detalle_ventaTableAdapter detalle_ventaTableAdapter;
        private System.Windows.Forms.BindingSource baseDeDatosDataSet7BindingSource;
        private BaseDeDatosDataSet7 baseDeDatosDataSet7;
    }
}