﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class ConsultarRemitoIngreso : Form
    {
        public ConsultarRemitoIngreso()
        {
            InitializeComponent();
        }
        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private SqlDataAdapter adaptador2;
        private SqlDataAdapter adaptador3;
        private DataTable datos;
        private DataTable datos2;
        private DataTable datos3;
        public static int idDetalleRemitoIngresoGrid;
        private void ConsultarRemitoIngreso_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = new SqlCommand("ConsultarRemitoIngreso", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@fechaInicio", SqlDbType.DateTime);
            adaptador.SelectCommand.Parameters.Add("@fechaFinal", SqlDbType.DateTime);
            adaptador.SelectCommand.Parameters.Add("@nombre", SqlDbType.VarChar);

            DateTime fechaFinal = dateTimePicker3.Value.AddDays(1);
            datos = new DataTable();
            adaptador.SelectCommand.Parameters["@fechaInicio"].Value = dateTimePicker2.Value;
            adaptador.SelectCommand.Parameters["@fechaFinal"].Value = fechaFinal;
            adaptador.SelectCommand.Parameters["@nombre"].Value = textBox1.Text;
            adaptador.Fill(datos);
            dataGridView1.DataSource = datos;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string variable = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            idDetalleRemitoIngresoGrid = int.Parse(variable);
            Form c = new VerRemitoIngreso();
            c.Show();
            dataGridView1.Columns.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = new SqlCommand("ConsultarRemitoIngreso2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@nroRemito", SqlDbType.VarChar);

            DateTime fechaFinal = dateTimePicker3.Value.AddDays(1);
            string nroRemito = textBox2.Text + "-"+ textBox3.Text;
            datos = new DataTable();
            adaptador.SelectCommand.Parameters["@nroRemito"].Value = nroRemito;
            adaptador.Fill(datos);
            dataGridView1.DataSource = datos;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                groupBox2.Visible = true;
                groupBox1.Visible = false;
            }
            else
            {
                groupBox2.Visible = true;
                groupBox1.Visible = false;
            }
        }

        private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                groupBox1.Visible = true;
                groupBox2.Visible = false;                
            }
            else
            {
                groupBox1.Visible = false;
                groupBox2.Visible = true;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            
        }
    }
}
