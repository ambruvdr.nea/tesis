﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace SoftCars
{
    public partial class ImprimirConsultaFactura : Form
    {
        public ImprimirConsultaFactura()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private SqlDataAdapter adaptador2;
        private SqlDataAdapter adaptador3;
        private DataTable datos;
        private DataTable datos2;
        private DataTable datos3;

        private string dni;
        private string nombre;
        private string domicilio;
        private string tipoDeFactura;
        private string fecha;
        private string iva;
        private string subtotal;
        private string totalf;

        private string codigo;
        private int codigo2;
        private string nombreProducto;
        private string cantidad;
        private string unidadMedida;
        private string precioUnitario;
        private string descuento;
        private string pdescuento;
        private string total;
        
        //private string elIdf;


        private void ImprimirConsultaFactura_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'BaseDeDatosDataSet4.detalle_venta' Puede moverla o quitarla según sea necesario.
            //this.detalle_ventaTableAdapter.Fill(this.BaseDeDatosDataSet4.detalle_venta);
            // TODO: esta línea de código carga datos en la tabla 'BaseDeDatosDataSet4.detalle_venta' Puede moverla o quitarla según sea necesario.
            //this.detalle_ventaTableAdapter.Fill(this.BaseDeDatosDataSet4.detalle_venta);
            // TODO: esta línea de código carga datos en la tabla 'BaseDeDatosDataSet4.detalle_venta' Puede moverla o quitarla según sea necesario.
            //this.detalle_ventaTableAdapter.Fill(this.BaseDeDatosDataSet1.detalle_venta);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.cliente' Puede moverla o quitarla según sea necesario.
            //this.clienteTableAdapter.Fill(this.baseDeDatosDataSet10.cliente);

            // TODO: esta línea de código carga datos en la tabla 'DataSetReporteFactura.PruebaDeConsultaFactura' Puede moverla o quitarla según sea necesario.
            //this.PruebaDeConsultaFacturaTableAdapter.Fill(this.DataSetReporteFactura.PruebaDeConsultaFactura);

            this.reportViewer1.RefreshReport();

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();

            adaptador2 = new SqlDataAdapter();

            //String elIdDeVenta = ConsultaFactura.idDeVenta.ToString();
            //int id = int.Parse(elIdDeVenta);
            
            
            String elIdDeVenta = ConsultaFactura.idDeVentaGrid.ToString();
            textBox1.Text = elIdDeVenta;
            

            adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = new SqlCommand("rellenoFactura3", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@rellenoIdFac3", SqlDbType.Int);

            datos = new DataTable();
            adaptador.SelectCommand.Parameters["@rellenoIdFac3"].Value = int.Parse(textBox1.Text);
            adaptador.Fill(datos);
            dataGridView1.DataSource = datos;

            //TODO ENCABEZADO
            dni = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            nombre = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            domicilio = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            tipoDeFactura = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            fecha = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            iva = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            subtotal = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            totalf = dataGridView1.CurrentRow.Cells[8].Value.ToString();
            //elIdf = dataGridView1.CurrentRow.Cells[10].Value.ToString();

            

            string var = ConsultaFactura.idDeVentaGrid.ToString();

            ReportParameter p = new ReportParameter("ReportParameterID", var);
            reportViewer1.LocalReport.SetParameters(p);
            reportViewer1.RefreshReport();


            
            ReportParameterCollection rp = new ReportParameterCollection();
            rp.Add(new ReportParameter("ReportParameterDNI", dni));
            rp.Add(new ReportParameter("ReportParameterNOMBRE", nombre));
            rp.Add(new ReportParameter("ReportParameterDOMICILIO", domicilio));
            rp.Add(new ReportParameter("ReportParameterTIPOFAC", tipoDeFactura));
            rp.Add(new ReportParameter("ReportParameterFECHA", fecha));
            rp.Add(new ReportParameter("ReportParameterIVA", iva));
            rp.Add(new ReportParameter("ReportParameterSUBTOTAL", subtotal));
            rp.Add(new ReportParameter("ReportParameterTOTALF", totalf));
            //rp.Add(new ReportParameter("ReportParameterID", elIdf));
            this.reportViewer1.LocalReport.SetParameters(rp);
            this.reportViewer1.RefreshReport();
            


            /*
            int var = ConsultaFactura.idDeVentaGrid;
            
            ReportParameterCollection rp5 = new ReportParameterCollection();
            rp5.Add(new ReportParameter("ReportParameterDNI", var));
            this.reportViewer1.LocalReport.SetParameters(rp);
            this.reportViewer1.RefreshReport();
            */

            /*
            //NOMBRE Y APELLIDO
            nombre = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            ReportParameterCollection rp2 = new ReportParameterCollection();
            rp2.Add(new ReportParameter("ReportParameterNOMBRE", dni));
            this.reportViewer1.LocalReport.SetParameters(rp2);
            this.reportViewer1.RefreshReport();

            //DOMICILIO
            domicilio = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            ReportParameterCollection rp3 = new ReportParameterCollection();
            rp3.Add(new ReportParameter("ReportParameterDOMICILIO", dni));
            this.reportViewer1.LocalReport.SetParameters(rp3);
            this.reportViewer1.RefreshReport();
            */
            /*
            adaptador2 = new SqlDataAdapter();
            adaptador2.SelectCommand = new SqlCommand("rellenoFactura2", conexion);
            adaptador2.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador2.SelectCommand.Parameters.Add("@rellenoIdFac2", SqlDbType.Int);

            datos2 = new DataTable();
            adaptador2.SelectCommand.Parameters["@rellenoIdFac2"].Value = int.Parse(textBox1.Text);
            adaptador2.Fill(datos2);
            dataGridView2.DataSource = datos2;

            codigo = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            //codigo2 = int.Parse(codigo);
            nombreProducto = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            cantidad = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            unidadMedida = "unidades";
            precioUnitario = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            descuento = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            pdescuento = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            total = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            ReportParameterCollection rp2 = new ReportParameterCollection();
            rp2.Add(new ReportParameter("ReportParameter_Codigo", codigo));
            this.reportViewer1.LocalReport.SetParameters(rp2);
            this.reportViewer1.RefreshReport();
            */
           
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

           
        }
    }
}
