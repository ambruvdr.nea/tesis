﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class NotaCredito : Form
    {
        public NotaCredito()
        {
            InitializeComponent();
        }


        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        //String valorRadio;
        //DataGridView dgv = new DataGridView();
        int bandera=0;
        float total=0f;
        float descuento = 0f;
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        public static string elTotalGlobalNC;
        float Global_IVA_NC;
        int idGlobalTipoPago_NC;
        int padre2;
        string nAfipC;
        public static int idDeVentaCred;
        int laBanderaC;


        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

       

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == true)
            {
                textBox10.Enabled = true;
                button8.Enabled = true;
                button4.Enabled = false;

                if (checkBox1.Enabled == true & textBox14.Text != "A")
                {
        
                    //float varLocalSubtotal = float.Parse(textBox9.Text);
                    //float varLocalIva = varLocalSubtotal * 0.21f;
                    //float ElTotalConIva = varLocalSubtotal + varLocalIva;

                    //textBox10.Text = ElTotalConIva.ToString();
                    label10.Text = textBox9.Text;
                    label24.Text = textBox9.Text;
                    Global_IVA_NC = 0f;
                }

                if (checkBox1.Enabled == true & textBox14.Text == "A")
                {
                    float varLocalSubtotal = float.Parse(textBox9.Text);
                    float varLocalIva = varLocalSubtotal*0.21f;
                    float ElTotalConIva = varLocalSubtotal + varLocalIva;

                    textBox10.Text = textBox9.Text;
                    //label9.Text = "Saldo con IVA";
                    label10.Text = textBox9.Text;
                    label24.Text = textBox9.Text;

                    //MODIFICO EL FORMATO DEL IVA (elimino el chorizo largo)
                    //string transformacion = varLocalIva.ToString();
                    //string a = String.Format("{0:c}", transformacion);

                    Global_IVA_NC = varLocalIva;
                }

            }
            else
            {
                textBox10.Enabled = false;
                button8.Enabled = false;
            }
        }

        

        private void Form5_Load(object sender, EventArgs e)
        {
            string variableLocal = FormLogin.id_global_usuario.ToString();
            textBox18.Text = variableLocal;

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;
            //dataGridView2.AllowUserToAddRows = false;


            conexion.Open();
            var cmd30 = new SqlCommand("SELECT max(numeroNC) FROM notacredito", conexion);
            var da30 = new SqlDataAdapter(cmd30);
            DataTable dt30 = new DataTable();
            da30.Fill(dt30);
            conexion.Close();

            padre2 = (int)dt30.Rows[0][0];

            try
            {

                conexion.Open();
                SqlCommand cmd20 = new SqlCommand("SELECT afipNC FROM notacredito WHERE numeroNC=" + padre2 + "", conexion);
                SqlDataReader dr20 = cmd20.ExecuteReader();


                if (dr20.Read())
                {
                    //string col0Value = dr20[0].ToString();
                    //elNumeroAfipTratar = col0Value;

                    string col0Value = dr20[0].ToString();
                    //elNumeroAfipTratar = col0Value;
                    string parte1 = col0Value.Split('-')[0];
                    string parte2 = col0Value.Split('-')[1];
                    //textBox8.Text = parte1;


                    string uno = "00001";
                    int cero = int.Parse(parte2);
                    int suma = cero + 1;
                    string dos = suma.ToString("D8");
                    nAfipC = uno + "-" + dos;

                    textBox15.Text = uno;
                    textBox12.Text = dos;

                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }


        }

        private void button4_Click(object sender, EventArgs e)
        {
            /*
            adaptador.SelectCommand = new SqlCommand("ListaArticulo2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art2", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art2"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView2.DataSource = datosTable;
            */
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            /*
            if(textBox3.Text != "")
            {
                textBox5.Enabled = true;
            }
            textBox5.Enabled = true;
            if (dataGridView2.CurrentRow.Index != -1)
            {
                textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();
                textBox6.Text = dataGridView2.CurrentRow.Cells[2].Value.ToString();
                label11.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            }

            */
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /*
            if (textBox3.Text != "")
            {
                textBox5.Enabled = true;
            }
            if (dataGridView2.CurrentRow.Index != -1)
            {
                textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();
                textBox6.Text = dataGridView2.CurrentRow.Cells[2].Value.ToString();
                label11.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            }
            */
        }

        private void textBox5_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox5.Text != "")
            {

                int cantidad = 0;
                float precio = float.Parse(textBox6.Text);
                float acumulado;



                cantidad = int.Parse(textBox5.Text);

                acumulado = cantidad * precio;


                textBox7.Text = acumulado.ToString();
            }

        }

        private void actualizarDatos()
        {
            datos.Clear();
            adaptador.Fill(datos, "articulo");
        }

        private void actualizarDatos2()
        {
            datos.Clear();
            adaptador.Fill(datos);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == "")
            {
                MessageBox.Show("Ingresar articulo y cantidad", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {

                int id = int.Parse(label11.Text);
                String articulo = textBox3.Text;
                int cantidad = int.Parse(textBox5.Text);
                float precio = float.Parse(textBox6.Text);
                float acumulado = float.Parse(textBox7.Text);

                if(bandera < 1)
                { 
                dataGridView1.Columns.Add("id", "Codigo");
                dataGridView1.Columns.Add("articulo", "Articulo");
                dataGridView1.Columns.Add("cantidad", "Cantidad");
                dataGridView1.Columns.Add("precio", "Precio");
                dataGridView1.Columns.Add("acumulado", "Acumuldado");

                bandera++;
                }

                dataGridView1.Rows.Add(id,articulo,cantidad,precio,acumulado);


                total = total + acumulado;

            
                textBox9.Text = total.ToString();
                //label10.Text = total.ToString();

                //Agrega el total de forma global
                elTotalGlobalNC = textBox9.Text;


                textBox3.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
                label11.Text = "";
                textBox5.Enabled = false;

                //DataGridViewColumn column = dataGridView1.Columns[0];
                //column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column1 = dataGridView1.Columns[1];
                column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column2 = dataGridView1.Columns[2];
                //column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column5 = dataGridView1.Columns[5];
                //column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column6 = dataGridView1.Columns[6];
                //column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
        }

        private void textBox4_KeyUp(object sender, KeyEventArgs e)
        {
            /*
            adaptador.SelectCommand = new SqlCommand("ListaArticulo2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art2", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art2"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView2.DataSource = datosTable;
            */
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }
        
        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {

            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            float saldo = float.Parse(label24.Text);


            label10.Text = saldo.ToString();

            saldo = float.Parse(label10.Text);

            if (checkBox1.Checked == true)
            {
                saldo = saldo - float.Parse(textBox10.Text);

                label10.Text = saldo.ToString();

            }

            /*
            if (checkBox2.Checked == true)
            {
                saldo = saldo - float.Parse(textBox11.Text);

                label10.Text = saldo.ToString();

            }
            */
            /*
            if (checkBox3.Checked == true)
            {
                saldo = saldo - float.Parse(textBox12.Text);

                label10.Text = saldo.ToString();

            }
            */

            saldo = float.Parse(textBox9.Text);

            if(float.Parse(label10.Text) == 0)
            {
                button2.Enabled = true;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            /*
            conexion.Open();

            var cmdTP = new SqlCommand("SELECT max(idTipoPago) FROM tipo_pago", conexion);

            var daTP = new SqlDataAdapter(cmdTP);

            daTP.Fill(dt2);
            int padreTP;

            conexion.Close();

            padreTP = (int)dt2.Rows[0][0];

            padreTP++;

            label25.Text = padreTP.ToString();

            */

            //idGlobalTipoPago = int.Parse(label25.Text);



            //textBox2.Enabled = true;
            //textBox3.Enabled = true;
            //textBox4.Enabled = true;
            textBox5.Enabled = true;
            //textBox6.Enabled = true;
            //textBox7.Enabled = true;
            textBox9.Enabled = true;
            textBox4.Enabled = true;
            textBox11.Enabled = true;

            textBox15.Enabled = false;

            dataGridView1.Enabled = true;
            //dataGridView2.Enabled = true;

            button1.Enabled = true;
            button3.Enabled = true;
            //button4.Enabled = true;
            //button6.Enabled = true;
            button7.Enabled = true;
            button6.Enabled = true;
            button12.Enabled = true;

            button11.Enabled = true;
            button4.Enabled = true;


            //label2.Enabled = true;
            label3.Enabled = true; 
            label4.Enabled = true;
            label6.Enabled = true;
            label7.Enabled = true;
            label8.Enabled = true;
            label9.Enabled = true;
            label10.Enabled = true;
            label12.Enabled = true;
            label13.Enabled = true;
            label14.Enabled = true;
            label15.Enabled = true;




            conexion.Open();

            var cmd = new SqlCommand("SELECT max(numeroNC) FROM notacredito", conexion);

            var da = new SqlDataAdapter(cmd);

            da.Fill(dt);

            conexion.Close();

            int padre = (int)dt.Rows[0][0];

            padre++;

            textBox1.Text = padre.ToString();
        }



       

        private void button2_Click(object sender, EventArgs e)
        {
            int contador = 0;

            SqlCommand alta = new SqlCommand("insert into notacredito values (@fechaNC, @ivaNC, @subtotalNC, @totalNC, @afipNC, @idTipoFactura, @IdCliente, @id_usuario, @numeroFacAsocNC)", conexion);

            adaptador.InsertCommand = alta;
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@fechaNC", SqlDbType.DateTime));

            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@ivaNC", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@subtotalNC", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@totalNC", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@afipNC", SqlDbType.VarChar));

            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@IdCliente", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTipoFactura", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@id_usuario", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@numeroFacAsocNC", SqlDbType.VarChar));
            //adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTipoPago", SqlDbType.Int));

            adaptador.InsertCommand.Parameters["@fechaNC"].Value = dateTimePicker1.Value;

            if (textBox14.Text == "A")
            {
                adaptador.InsertCommand.Parameters["@ivaNC"].Value = Math.Round(Global_IVA_NC);
            }
            else
            {
                adaptador.InsertCommand.Parameters["@ivaNC"].Value = 0;
            }

            adaptador.InsertCommand.Parameters["@subtotalNC"].Value = float.Parse(textBox9.Text);
            adaptador.InsertCommand.Parameters["@totalNC"].Value = float.Parse(label24.Text);
            adaptador.InsertCommand.Parameters["@afipNC"].Value = textBox15.Text+"-"+textBox12.Text;


            adaptador.InsertCommand.Parameters["@IdCliente"].Value = int.Parse(textBox16.Text);
            adaptador.InsertCommand.Parameters["@idTipoFactura"].Value = int.Parse(textBox17.Text);
            adaptador.InsertCommand.Parameters["@id_usuario"].Value = int.Parse(textBox18.Text);
            adaptador.InsertCommand.Parameters["@numeroFacAsocNC"].Value = textBox4.Text+"-"+textBox11.Text;
            //adaptador.InsertCommand.Parameters["@idTipoPago"].Value = int.Parse(label25.Text);

            try
            {
                conexion.Open();
                adaptador.InsertCommand.ExecuteNonQuery();
                MessageBox.Show("Se ha registrado el exitosamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }





            SqlCommand alta2 = new SqlCommand("insert into detalle_ncredito values (@idDetalleNC, @nombre, @cantidad, @precio, @acumulado, @descuento, @pdescuento)", conexion);

            adaptador.InsertCommand = alta2;
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idDetalleNC", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@cantidad", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@precio", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@acumulado", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@descuento", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@pdescuento", SqlDbType.Float));

         

                try
                {
                    conexion.Open();

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                   // int codigo = (int)row.Cells["id"].Value;
                  
                   
                    //String nombre = row.Cells["Articulo"].Value.ToString();
                    //int cantidad = (int)row.Cells["cantidad"].Value;
                    //float precio = (float)row.Cells["Precio"].Value;
                    float acumulado =0;
                    
                   
                    float pdescuento = 0f;

                    pdescuento = acumulado * (1 - (descuento / 100));

                    alta2.Parameters.Clear();

                    alta2.Parameters.AddWithValue("@idDetalleNC", Convert.ToInt32(textBox1.Text));
                    alta2.Parameters.AddWithValue("@nombre", Convert.ToString(row.Cells["articulo"].Value));
                    alta2.Parameters.AddWithValue("@cantidad", Convert.ToInt32(row.Cells["cantidad"].Value));
                    alta2.Parameters.AddWithValue("@precio", Convert.ToDecimal(row.Cells["precio"].Value));
                    alta2.Parameters.AddWithValue("@acumulado", Convert.ToDecimal(row.Cells["acumulado"].Value));
                    if (textBox2.Text == "")
                    {
                        descuento = 0;
                    }
                    
                    alta2.Parameters.AddWithValue("@descuento", Convert.ToDecimal(descuento));
                    alta2.Parameters.AddWithValue("@pdescuento", Convert.ToDecimal(pdescuento));

                    alta2.ExecuteNonQuery();




                    //adaptador.InsertCommand.Parameters["@descuento"].Value = textBox2.Text;
                    //adaptador.InsertCommand.Parameters["@pdescuento"].Value = pdescuento;


                    // adaptador.InsertCommand.ExecuteNonQuery();


                    //adaptador.InsertCommand.Parameters["@nombre"].Value = nombre;
                    //adaptador.InsertCommand.Parameters["@cantidad"].Value = cantidad;
                    //adaptador.InsertCommand.Parameters["@precio"].Value = precio;
                    //adaptador.InsertCommand.Parameters["@acumulado"].Value = acumulado;





                    //  SqlCommand modificacion = new SqlCommand("update articulo set cantidad='"+cantidad+"' where id_articulo ='"+codigo+"'", conexion);

                    //adaptador.UpdateCommand = modificacion;
                    //adaptador.UpdateCommand.Parameters.Add(new SqlParameter("cantidad", SqlDbType.Int));
                    laBanderaC = 1;

                }

                  
                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                    laBanderaC = 0;
                }
                finally
                {
                    conexion.Close();
                }

            /*
            
            SqlCommand alta3 = new SqlCommand("insert into tarjeta values (@nombreTarjeta, @marcaTarjeta, @importe, @idTipoTarjeta, @idCuota)", conexion);
            adaptador.InsertCommand = alta3;
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombreTarjeta", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@marcaTarjeta", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@importe", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTipoTarjeta", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idCuota", SqlDbType.Int));

            adaptador.InsertCommand.Parameters["@nombreTarjeta"].Value = label19.Text;
            adaptador.InsertCommand.Parameters["@marcaTarjeta"].Value = label20.Text;
            adaptador.InsertCommand.Parameters["@importe"].Value = float.Parse(label21.Text);
            adaptador.InsertCommand.Parameters["@idTipoTarjeta"].Value = int.Parse(label22.Text);
            if (label22.Text == "idCuota")
            {
                adaptador.InsertCommand.Parameters["@idCuota"].Value = 0;
            }
            else
            {
                adaptador.InsertCommand.Parameters["@idCuota"].Value = int.Parse(label23.Text);
            }
            

            try
            {
                conexion.Open();
                adaptador.InsertCommand.ExecuteNonQuery();
                //MessageBox.Show("Se inserto Ok");
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }



            if (checkBox2.Checked == true)
            {
                SqlCommand alta4 = new SqlCommand("insert into tipo_pago values (@nombreTipo, @idTarjeta)", conexion);
                adaptador.InsertCommand = alta4;
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombreTipo", SqlDbType.VarChar));
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTarjeta", SqlDbType.Int));

                adaptador.InsertCommand.Parameters["@nombreTipo"].Value = "tarjeta";
                adaptador.InsertCommand.Parameters["@idTarjeta"].Value = int.Parse(label18.Text);

                try
                {
                    conexion.Open();
                    adaptador.InsertCommand.ExecuteNonQuery();
                    //MessageBox.Show("joiiia");
                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }

                finally
                {
                    conexion.Close();
                }
                
            }
            */

            if (laBanderaC == 1)
            {

                DialogResult cTexto;
                cTexto = MessageBox.Show("Desea imprimir la Factura?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);

                if (cTexto == DialogResult.Yes)
                {
                    string variable = textBox1.Text;
                    idDeVentaCred = int.Parse(variable);

                    Form formuNC = new Form10NotaCredito();
                    formuNC.Show();
                }
                //button6.Enabled = true;
            }
                //Borro los TextBox
                textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            //textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            //textBox11.Text = "";
            //textBox12.Text = "";

            //Desactivo los CheckBox
            checkBox1.Checked = false;
            //checkBox2.Checked = false;
            //checkBox3.Checked = false;

            //Desabilito todo
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            //textBox4.Enabled = false;
            textBox5.Enabled = false;
            textBox6.Enabled = false;
            textBox7.Enabled = false;
            textBox9.Enabled = false;

            dataGridView1.Enabled = false;
            //dataGridView2.Enabled = false;

            button1.Enabled = false;
            button3.Enabled = false;
            //button4.Enabled = false;
            //button6.Enabled = false;
            button7.Enabled = false;

            //label2.Enabled = false;
            label3.Enabled = false;
            label4.Enabled = false;
            label6.Enabled = false;
            label7.Enabled = false;
            label8.Enabled = false;
            label9.Enabled = false;
            label10.Enabled = false;
            label12.Enabled = false;

            //NUEVO-RESETEO

            label10.Text = "$0.00";
            dataGridView1.Columns.Clear();
            textBox8.Text = "";
            textBox13.Text = "";
            textBox14.Text = "";
            textBox16.Text = "";
            textBox17.Text = "";
            textBox18.Text = "";
            textBox15.Text = "";
            button11.Enabled = false;
            button12.Enabled = false;
            button2.Enabled = false;
            textBox15.Enabled = true;
            button4.Enabled = false;
            elTotalGlobalNC = "";
            Global_IVA_NC = 0f;
            label18.Text = "idTarjeta";
            label19.Text = "nombreTarjeta";
            label20.Text = "marcaTarjeta";
            label21.Text = "importe";
            label22.Text = "idTipoTarjeta";
            label23.Text = "idCuota";
            label24.Text = "$0.00";
            idGlobalTipoPago_NC = 0;

            button4.Enabled = false;
            textBox4.Text = "";
            textBox11.Text = "";

        }

        private void button8_Click(object sender, EventArgs e)
        {
            if(checkBox1.Enabled == true)
            {
                textBox10.Text = textBox9.Text;
            }
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            /*
            if (checkBox2.Enabled == true)
            {
                textBox11.Text = textBox9.Text;
            }
            */
            /*
            Form pTarjeta = new TipoPagoTarjeta();
            pTarjeta.Show();
            */


            //TipoPagoTarjeta tarjeta = new TipoPagoTarjeta();
            //tarjeta.pasado4 += new TipoPagoTarjeta.pasar4(ejecutar4);
            //tarjeta.Show();

            //button9.Enabled = false;
            //checkBox2.Enabled = false;


        }

        public void ejecutar4(string dato, string dato2, string dato3, string dato4, string dato5, string dato6)
        {

            label18.Text = dato;
            label19.Text = dato2;
            label20.Text = dato3;
            label21.Text = dato6;
            label22.Text = dato4;
            label23.Text = dato5;
            //textBox11.Text = dato6;

            /*
            //porcentaje
            textBox2.Text = dato;
            //total
            textBox9.Text = dato2;
            //total
            //label10.Text = dato2;
            //elTotalGlobal = "";
            */
        }

        private void button10_Click(object sender, EventArgs e)
        {
            /*
            if (checkBox1.Enabled == true)
            {
                textBox12.Text = textBox9.Text;
            }
            */
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ConsultarClienteFactura c = new ConsultarClienteFactura();
            c.pasado += new ConsultarClienteFactura.pasar(ejecutar);
            c.Show();
        }

        public void ejecutar(string dato, string dato2, string dato3, string dato4, string dato5)
        {
            //nombre
            textBox13.Text = dato3;
            //cuit
            textBox8.Text = dato2;
            //tipoFactura
            textBox14.Text = dato4;

            //id
            textBox16.Text = dato;

            //idTipoFactura
            textBox17.Text = dato5;


        }


        private void textBox13_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            ConsultarArticuloFactura cArticulo = new ConsultarArticuloFactura();
            cArticulo.pasado2 += new ConsultarArticuloFactura.pasar2(ejecutar2);
            cArticulo.Show();


            textBox5.Enabled = true;
            textBox5.BackColor = Color.IndianRed;
            label3.ForeColor = Color.IndianRed;

        }

        public void ejecutar2(string dato, string dato2, string dato3)
        {
            //id
            label11.Text = dato;
            //Nombre
            textBox3.Text = dato2;
            //Precio
            textBox6.Text = dato3;

            /*
            //id
            textBox16.Text = dato;

            //idTipoFactura
            textBox17.Text = dato5;
            */
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            AgregarDescuentoFactura aDescuentoF = new AgregarDescuentoFactura();
            aDescuentoF.pasado3 += new AgregarDescuentoFactura.pasar3(ejecutar3);
            aDescuentoF.Show();

            /*
            Form agregarD = new AgregarDescuento();
            agregarD.Show();
            */
        }

        public void ejecutar3(string dato, string dato2)
        {
            //porcentaje
            textBox2.Text = dato;
            //total
            textBox9.Text = dato2;
            //total
            //label10.Text = dato2;
            //elTotalGlobal = "";
        }

        private void textBox5_MouseDown(object sender, MouseEventArgs e)
        {
            textBox5.BackColor = Color.White;
            label3.ForeColor = Color.White;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                conexion.Open();
                SqlCommand cmd60 = new SqlCommand("SELECT c.cuilCliente," +
                                                        " c.nombreCliente, " +
                                                        " tf.nombreTipo, " +
                                                        " dv.idDetalleVenta, " +
                                                        " dv.nombre, " +
                                                        " dv.cantidad, " +
                                                        " dv.precio, " +
                                                        " dv.acumulado, " +
                                                        " v.IdCliente, " +
                                                        " v.idTipoFactura, " +
                                                        " v.id_usuario " +
                                                        " FROM cliente c, venta v, tipoFactura tf, detalle_venta dv " +
                                                        " WHERE v.IdCliente = c.IdCliente" +
                                                        " and v.idTipoFactura = tf.idTipoFactura" +
                                                        " and dv.idDetalleVenta = v.id_venta" +
                                                        " and v.afipFactura= '" + textBox4.Text + "'+'-'+'" + textBox11.Text + "'", conexion);

                SqlDataReader dr60 = cmd60.ExecuteReader();

                if (dr60.Read())
                {
                    string col0Value = dr60[0].ToString();
                    string col1Value = dr60[1].ToString();
                    string col2Value = dr60[2].ToString();

                    textBox8.Text = col0Value;
                    textBox13.Text = col1Value;
                    textBox14.Text = col2Value;

                    string col3Value = dr60[3].ToString();
                    string col4Value = dr60[4].ToString();
                    string col5Value = dr60[5].ToString();
                    string col6Value = dr60[6].ToString();
                    string col7Value = dr60[7].ToString();

                    string col8Value = dr60[8].ToString();
                    string col9Value = dr60[9].ToString();
                    string col10Value = dr60[10].ToString();

                    textBox16.Text = col8Value;
                    textBox17.Text = col9Value;
                    textBox18.Text = col10Value;

                    int id = int.Parse(col3Value);
                    String articulo = col4Value;
                    int cantidad = int.Parse(col5Value);
                    float precio = float.Parse(col6Value);
                    float acumulado = float.Parse(col7Value);

                    if (bandera < 1)
                    {
                        dataGridView1.Columns.Add("id", "Codigo");
                        dataGridView1.Columns.Add("articulo", "Articulo");
                        dataGridView1.Columns.Add("cantidad", "Cantidad");
                        dataGridView1.Columns.Add("precio", "Precio");
                        dataGridView1.Columns.Add("acumulado", "Acumuldado");

                        bandera++;
                    }

                    dataGridView1.Rows.Add(id, articulo, cantidad, precio, acumulado);

                    
                    DataGridViewColumn column1 = dataGridView1.Columns[1];
                    column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                    total = total + acumulado;


                    textBox9.Text = total.ToString();
                    //label10.Text = total.ToString();

                    //Agrega el total de forma global
                    elTotalGlobalNC = textBox9.Text;

                }
                else
                {
                    MessageBox.Show("ERROR", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this.Dispose();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                conexion.Close();
            }
        }
    }


    
}

