﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace SoftCars
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataTable datos;

        private string dni;
        private string nombre;
        private string domicilio;
        private string tipoDeFactura;
        private string fecha;
        private string iva;
        private string subtotal;
        private string totalf;
        private string nroAfip;
        private string tipoDelP;

        private void Form7_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'BaseDeDatosDataSet24.detalle_venta' Puede moverla o quitarla según sea necesario.
            this.detalle_ventaTableAdapter.Fill(this.BaseDeDatosDataSet24.detalle_venta);

            this.reportViewer1.RefreshReport();

            string var = ConsultaFactura.idDeVentaGrid.ToString();

            ReportParameter pe = new ReportParameter("ReportParameterIDs", var);
            reportViewer1.LocalReport.SetParameters(pe);
            reportViewer1.RefreshReport();

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();

            String elIdDeVenta = ConsultaFactura.idDeVentaGrid.ToString();
            textBox1.Text = elIdDeVenta;

            adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = new SqlCommand("rellenoFactura3", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@rellenoIdFac3", SqlDbType.Int);

            datos = new DataTable();
            adaptador.SelectCommand.Parameters["@rellenoIdFac3"].Value = int.Parse(textBox1.Text);
            adaptador.Fill(datos);
            dataGridView1.DataSource = datos;

            //TODO ENCABEZADO
            dni = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            nombre = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            domicilio = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            tipoDeFactura = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            fecha = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            iva = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            subtotal = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            totalf = dataGridView1.CurrentRow.Cells[8].Value.ToString();
            nroAfip = dataGridView1.CurrentRow.Cells[11].Value.ToString();
            tipoDelP = dataGridView1.CurrentRow.Cells[12].Value.ToString();

            /*
            string var = ConsultaFactura.idDeVentaGrid.ToString();

            ReportParameter p = new ReportParameter("ReportParameterID", var);
            reportViewer1.LocalReport.SetParameters(p);
            reportViewer1.RefreshReport();
            */

            ReportParameterCollection rp = new ReportParameterCollection();
            rp.Add(new ReportParameter("ReportParameterDNI", dni));
            rp.Add(new ReportParameter("ReportParameterNOMBRE", nombre));
            rp.Add(new ReportParameter("ReportParameterDOMICILIO", domicilio));
            rp.Add(new ReportParameter("ReportParameterTIPOFAC", tipoDeFactura));
            rp.Add(new ReportParameter("ReportParameterFECHA", fecha));
            rp.Add(new ReportParameter("ReportParameterIVA", iva));
            rp.Add(new ReportParameter("ReportParameterSUBTOTAL", subtotal));
            rp.Add(new ReportParameter("ReportParameterTOTALF", totalf));
            rp.Add(new ReportParameter("ReportParameterAFIP", nroAfip));
            rp.Add(new ReportParameter("ReportParameterTIPOPAGO", tipoDelP));
            //rp.Add(new ReportParameter("ReportParameterID", elIdf));
            this.reportViewer1.LocalReport.SetParameters(rp);
            this.reportViewer1.RefreshReport();


        }
    }
}
