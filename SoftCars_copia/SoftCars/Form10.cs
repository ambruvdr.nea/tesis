﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class Form10 : Form
    {
        public Form10()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataTable datos;

        private string dni;
        private string nombre;
        private string domicilio;
        private string tipoDeFactura;
        private string fecha;
        private string iva;
        private string subtotal;
        private string totalf;
        private string afipNC;

        private void Form10_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'BaseDeDatosDataSet28.detalle_ncredito' Puede moverla o quitarla según sea necesario.
            this.detalle_ncreditoTableAdapter.Fill(this.BaseDeDatosDataSet28.detalle_ncredito);

            this.reportViewer1.RefreshReport();

            string varNC = ConsultaNotaCredito.idDeNC.ToString();

            ReportParameter peNC = new ReportParameter("ReportParameterIDnC", varNC);
            reportViewer1.LocalReport.SetParameters(peNC);
            reportViewer1.RefreshReport();

            conexion = new SqlConnection(ConexionDB.conexiondb);

            String elIdDeNC = ConsultaNotaCredito.idDeNC.ToString();
            textBox1.Text = elIdDeNC;

            adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = new SqlCommand("rellenoNotaCredito", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@rellenoIdNC", SqlDbType.Int);

            datos = new DataTable();
            adaptador.SelectCommand.Parameters["@rellenoIdNC"].Value = int.Parse(textBox1.Text);
            adaptador.Fill(datos);
            dataGridView1.DataSource = datos;

            dni = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            nombre = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            domicilio = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            tipoDeFactura = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            fecha = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            iva = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            subtotal = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            totalf = dataGridView1.CurrentRow.Cells[8].Value.ToString();
            afipNC = dataGridView1.CurrentRow.Cells[11].Value.ToString();

            ReportParameterCollection rp4 = new ReportParameterCollection();
            rp4.Add(new ReportParameter("ReportParameterDNInc", dni));
            rp4.Add(new ReportParameter("ReportParameterNOMBREnc", nombre));
            rp4.Add(new ReportParameter("ReportParameterDOMICILIOnc", domicilio));
            rp4.Add(new ReportParameter("ReportParameterTIPOFACnc", tipoDeFactura));
            rp4.Add(new ReportParameter("ReportParameterFECHAnc", fecha));
            rp4.Add(new ReportParameter("ReportParameterIVAnc", iva));
            rp4.Add(new ReportParameter("ReportParameterSUBTOTALnc", subtotal));
            rp4.Add(new ReportParameter("ReportParameterTOTALFnc", totalf));
            rp4.Add(new ReportParameter("ReportParameterAFPFnc", afipNC));
            //rp.Add(new ReportParameter("ReportParameterID", elIdf));
            this.reportViewer1.LocalReport.SetParameters(rp4);
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
