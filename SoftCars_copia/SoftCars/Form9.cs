﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class Form9 : Form
    {
        public Form9()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataTable datos;

        private string dni;
        private string nombre;
        private string domicilio;
        private string tipoDeFactura;
        private string fecha;
        private string iva;
        private string subtotal;
        private string totalf;
        private string afipND;

        private void Form9_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'BaseDeDatosDataSet27.detalle_ndebito' Puede moverla o quitarla según sea necesario.
            this.detalle_ndebitoTableAdapter.Fill(this.BaseDeDatosDataSet27.detalle_ndebito);

            this.reportViewer1.RefreshReport();

            string varND = ConsultaNotaDebito.idDeND.ToString();

            ReportParameter peND = new ReportParameter("ReportParameterIDnD", varND);
            reportViewer1.LocalReport.SetParameters(peND);
            reportViewer1.RefreshReport();

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();

            String elIdDeND = ConsultaNotaDebito.idDeND.ToString();
            textBox1.Text = elIdDeND;

            adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = new SqlCommand("rellenoNotaDebito", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@rellenoIdND", SqlDbType.Int);

            datos = new DataTable();
            adaptador.SelectCommand.Parameters["@rellenoIdND"].Value = int.Parse(textBox1.Text);
            adaptador.Fill(datos);
            dataGridView1.DataSource = datos;

            dni = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            nombre = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            domicilio = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            tipoDeFactura = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            fecha = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            iva = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            subtotal = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            totalf = dataGridView1.CurrentRow.Cells[8].Value.ToString();
            afipND = dataGridView1.CurrentRow.Cells[11].Value.ToString();


            ReportParameterCollection rp3 = new ReportParameterCollection();
            rp3.Add(new ReportParameter("ReportParameterDNInd", dni));
            rp3.Add(new ReportParameter("ReportParameterNOMBREnd", nombre));
            rp3.Add(new ReportParameter("ReportParameterDOMICILIOnd", domicilio));
            rp3.Add(new ReportParameter("ReportParameterTIPOFACnd", tipoDeFactura));
            rp3.Add(new ReportParameter("ReportParameterFECHAnd", fecha));
            rp3.Add(new ReportParameter("ReportParameterIVAnd", iva));
            rp3.Add(new ReportParameter("ReportParameterSUBTOTALnd", subtotal));
            rp3.Add(new ReportParameter("ReportParameterTOTALFnd", totalf));
            rp3.Add(new ReportParameter("ReportParameterAFIPnd", afipND));
            //rp.Add(new ReportParameter("ReportParameterID", elIdf));
            this.reportViewer1.LocalReport.SetParameters(rp3);
            this.reportViewer1.RefreshReport();

        }
    }
}
