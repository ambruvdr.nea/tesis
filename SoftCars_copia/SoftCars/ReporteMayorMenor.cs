﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;


namespace SoftCars
{
    public partial class ReporteMayorMenor : Form
    {
        public ReporteMayorMenor()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;

        //private SqlDataAdapter adaptador;
        //private DataSet datos;
        //private DataTable datosTable;

        SqlCommand sqlcmd;
        SqlCommand sqlcmd2;
        SqlDataReader sqldr;
        //SqlDataReader sqldr3;
        private SqlDataAdapter adaptador;
        private DataTable datos;
        //private SqlDataAdapter adaptador2;
        //private DataTable datos2;

        private void ReporteMayorMenor_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            GraficosCategorias();
            TotalDeVentas();
            CantidadDeVentas();

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            string var = label3.Text;
            label3.Text = Decimal.Parse(var).ToString("c");


        }

        ArrayList Nombre = new ArrayList();
        ArrayList CantidadVendidos = new ArrayList();

        private void GraficosCategorias()
        {
            sqlcmd = new SqlCommand("VendidosMayorMenor", conexion);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            sqldr = sqlcmd.ExecuteReader();
            while (sqldr.Read())
            {
                Nombre.Add(sqldr.GetString(0));
                CantidadVendidos.Add(sqldr.GetInt32(1));
            }
            chart1.Series[0].Points.DataBindXY(Nombre, CantidadVendidos);
            sqldr.Close();
            conexion.Close();

            
            chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            //chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            
        }

      
        private void CantidadDeVentas()
        {
            sqlcmd = new SqlCommand("CantidadDeVentas", conexion);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlParameter total = new SqlParameter("@cantidadVentas", 0);
            total.Direction = ParameterDirection.Output;
            sqlcmd.Parameters.Add(total);
            conexion.Open();
            sqlcmd.ExecuteNonQuery();
            label4.Text = sqlcmd.Parameters["@cantidadVentas"].Value.ToString();
            conexion.Close();
        }

        private void TotalDeVentas()
        {
            sqlcmd2 = new SqlCommand("TotalDeVentas", conexion);
            sqlcmd2.CommandType = CommandType.StoredProcedure;
            SqlParameter total = new SqlParameter("@totalVentas", SqlDbType.Float);
            total.Direction = ParameterDirection.Output;
            sqlcmd2.Parameters.Add(total);
            conexion.Open();
            sqlcmd2.ExecuteNonQuery();
            label3.Text = sqlcmd2.Parameters["@totalVentas"].Value.ToString();
            conexion.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            //PrintPreviewDialog.ActiveForm
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.Height = 800;
            printPreviewDialog1.Width = 900;
            printDocument1.DefaultPageSettings.Landscape = true;

            //printPreviewDialog1;
            printPreviewDialog1.ShowDialog();
        }

        private void printPreviewDialog1_Load(object sender, EventArgs e)
        {

        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string fecha = dateTimePicker1.Value.ToString("dd/MM/yyyy");

            Bitmap obj = new Bitmap(this.chart1.Width, this.chart1.Height);
            chart1.DrawToBitmap(obj, new Rectangle(0, 0, this.chart1.Width, this.chart1.Height));

            //1069; 382 this.dataGridView1.Width, this.dataGridView1.Height

            e.Graphics.DrawImage(obj, 100, 100);

            Font font = new Font("Century Gothic", 16);
            Font font2 = new Font("Arial", 14);
            e.Graphics.DrawString("Top Five de Productos/Servicios más vendidos por período", font, Brushes.Black, 260, 20);
            e.Graphics.DrawString("Fecha: " + fecha, font, Brushes.Black, 950, 40);
            e.Graphics.DrawString("Rango entre "+dateTimePicker1.Value+" y "+ dateTimePicker2.Value, font2, Brushes.Black, 320, 85);

            //printDocument1.DefaultPageSettings.Landscape = true;

            Image newImage = Image.FromFile("\\SoftCars_copia\\IMAGENES\\Logo2.jpg");
            PointF ulCorner = new PointF(10, 12);
            e.Graphics.DrawImage(newImage, ulCorner);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = new SqlCommand("VendidosMayorMenor2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@fechaInicio", SqlDbType.DateTime);
            adaptador.SelectCommand.Parameters.Add("@fechaFinal", SqlDbType.DateTime);

            datos = new DataTable();
            adaptador.SelectCommand.Parameters["@fechaInicio"].Value = dateTimePicker2.Value;
            adaptador.SelectCommand.Parameters["@fechaFinal"].Value = dateTimePicker3.Value;
            adaptador.Fill(datos);
            dataGridView1.DataSource = datos;

            //- Crear Array
            Object[] Datos = new Object[datos.Rows.Count];
            Object[] Datos2 = new Object[datos.Rows.Count];

            for (int i = 0; i < datos.Rows.Count; i++)
            {
                //- Guardar la Columna Nombre en el Arreglo
                Datos[i] = datos.Rows[i]["Nombre"].ToString();
                Datos2[i] = datos.Rows[i]["CantidadVendidos"];
            }

            chart1.Series[0].Points.DataBindXY(Datos, Datos2);
            //sqldr.Close();
            //conexion.Close();


            chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            //chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;    


        }

        ArrayList Nombre2 = new ArrayList();
        ArrayList CantidadVendidos2 = new ArrayList();

        private void GraficosCategorias2()
        {
            /*

            conexion.Open();
            SqlCommand cmd2 = new SqlCommand("SELECT top 5 nombre as Nombre, COUNT(nombre) as CantidadVendidos from detalle_venta inner join venta on detalle_venta.idDetalleVenta = venta.id_venta WHERE venta.fecha between '" + dateTimePicker2.Value + "' AND '" + dateTimePicker3.Value + "' group by nombre order by COUNT(2) desc", conexion);

            SqlDataReader sqldr4 = cmd2.ExecuteReader();


            while (sqldr4.Read())
            {
                Nombre2.Add(sqldr4.GetString(0));
                CantidadVendidos2.Add(sqldr4.GetInt32(1));
            }
            chart1.Series[0].Points.DataBindXY(Nombre2, CantidadVendidos2);
            sqldr4.Close();
            conexion.Close();


            chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            //chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
        
        */ 

        }
    }
}
