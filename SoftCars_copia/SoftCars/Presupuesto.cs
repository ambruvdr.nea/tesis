﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class Presupuesto : Form
    {
        public Presupuesto()
        {
            InitializeComponent();
        }


        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        //String valorRadio;
        //DataGridView dgv = new DataGridView();
        int bandera=0;
        float total=0f;
        float descuento = 0f;
        DataTable dt = new DataTable();
        public static string elTotalGlobal;
        public static int idDePresupuesto;
        private int laBandera;

        private int tipoDePago;
        private float importeEfectivo;
        private float importeTarjeta;

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            /*
            if (checkBox3.Checked == true)
            {
                textBox12.Enabled = true;
                button10.Enabled = true;
                button4.Enabled = false;
            }
            else
            {
                textBox12.Enabled = false;
                button10.Enabled = false;
            }
            */
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == true)
            {
                textBox10.Enabled = true;
                button8.Enabled = true;
                button4.Enabled = false;
                //textBox10.Text = textBox9.Text;
            }
            else
            {
                textBox10.Enabled = false;
                button8.Enabled = false;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                textBox11.Enabled = true;
                button9.Enabled = true;
                button4.Enabled = false;
            }
            else
            {
                textBox11.Enabled = false;
                button9.Enabled = false;
            }
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            string variableLocal = FormLogin.id_global_usuario.ToString();
            textBox18.Text = variableLocal;

            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;
            //dataGridView2.AllowUserToAddRows = false;
            calcularDias();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            /*
            adaptador.SelectCommand = new SqlCommand("ListaArticulo2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art2", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art2"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView2.DataSource = datosTable;
            */
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            /*
            if(textBox3.Text != "")
            {
                textBox5.Enabled = true;
            }
            textBox5.Enabled = true;
            if (dataGridView2.CurrentRow.Index != -1)
            {
                textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();
                textBox6.Text = dataGridView2.CurrentRow.Cells[2].Value.ToString();
                label11.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            }
            */
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /*
            if (textBox3.Text != "")
            {
                textBox5.Enabled = true;
            }
            if (dataGridView2.CurrentRow.Index != -1)
            {
                textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();
                textBox6.Text = dataGridView2.CurrentRow.Cells[2].Value.ToString();
                label11.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            }
            */
        }

        private void textBox5_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox5.Text != "")
            {

                int cantidad = 0;
                float precio = float.Parse(textBox6.Text);
                float acumulado;



                cantidad = int.Parse(textBox5.Text);

                acumulado = cantidad * precio;


                textBox7.Text = acumulado.ToString();
            }

        }

        private void actualizarDatos()
        {
            datos.Clear();
            adaptador.Fill(datos, "articulo");
        }

        private void actualizarDatos2()
        {
            datos.Clear();
            adaptador.Fill(datos);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox5.Text == "")
            {
                MessageBox.Show("Ingresar articulo y cantidad", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                int id = int.Parse(label11.Text);
                String articulo = textBox3.Text;
                int cantidad = int.Parse(textBox5.Text);
                float precio = float.Parse(textBox6.Text);
                float acumulado = float.Parse(textBox7.Text);

                if (bandera < 1)
                {
                    dataGridView1.Columns.Add("id", "Codigo");
                    dataGridView1.Columns.Add("articulo", "Articulo");
                    dataGridView1.Columns.Add("cantidad", "Cantidad");
                    dataGridView1.Columns.Add("precio", "Precio");
                    dataGridView1.Columns.Add("acumulado", "Acumuldado");

                    bandera++;
                }

                //DataGridViewColumn column = dataGridView1.Columns[0];
                //column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column1 = dataGridView1.Columns[1];
                column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column2 = dataGridView1.Columns[2];
                //column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column5 = dataGridView1.Columns[5];
                //column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column6 = dataGridView1.Columns[6];
                //column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                dataGridView1.Rows.Add(id, articulo, cantidad, precio, acumulado);


                total = total + acumulado;


                textBox9.Text = total.ToString();
                label10.Text = total.ToString();

                //Agrega el total de forma global
                elTotalGlobal = textBox9.Text;


                textBox3.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
                label11.Text = "";
                textBox5.Enabled = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
        }

        private void textBox4_KeyUp(object sender, KeyEventArgs e)
        {
            /*
            adaptador.SelectCommand = new SqlCommand("ListaArticulo2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art2", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art2"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView2.DataSource = datosTable;
            */
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }
        
        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {

            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            /*
            descuento = float.Parse(textBox2.Text);
            if (descuento > 0)
            {
                float porcentaje = 1 - (descuento / 100);
                total = total * porcentaje;
            }

            //textBox2.Text = descuento.ToString();
            textBox9.Text = total.ToString();
            label10.Text = total.ToString();
            descuento = 0f;
            */

        }

        private void button7_Click(object sender, EventArgs e)
        {
            float saldo = float.Parse(textBox9.Text);


            label10.Text = saldo.ToString();

            saldo = float.Parse(label10.Text);

            if (checkBox1.Checked == true)
            {
                saldo = saldo - float.Parse(textBox10.Text);

                label10.Text = saldo.ToString();

            }

            if (checkBox2.Checked == true)
            {
                saldo = saldo - float.Parse(textBox11.Text);

                label10.Text = saldo.ToString();

            }
            /*
            if (checkBox3.Checked == true)
            {
                saldo = saldo - float.Parse(textBox12.Text);

                label10.Text = saldo.ToString();

            }
            */
            saldo = float.Parse(textBox9.Text);

            if(float.Parse(label10.Text) == 0)
            {
                button2.Enabled = true;
            }


            //BANDERA Y VALOR PARA TIPO DE PAGO
            if (checkBox1.Checked & checkBox2.Checked)
            {
                tipoDePago = 3;
                importeEfectivo = float.Parse(textBox10.Text);
                importeTarjeta = float.Parse(textBox11.Text);
            }
            else
            if (checkBox1.Checked)
            {
                tipoDePago = 1;
                importeEfectivo = float.Parse(textBox10.Text);
            }
            else
            {
                tipoDePago = 2;
                importeTarjeta = float.Parse(textBox11.Text);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //textBox2.Enabled = true;
            //textBox3.Enabled = true;
            //textBox4.Enabled = true;
            //textBox5.Enabled = true;
            //textBox6.Enabled = true;
            //textBox7.Enabled = true;
            textBox9.Enabled = true;

            dataGridView1.Enabled = true;
            //dataGridView2.Enabled = true;

            button1.Enabled = true;
            button3.Enabled = true;
            //button4.Enabled = true;
            //button6.Enabled = true;
            button7.Enabled = true;
            button12.Enabled = true;
            button6.Enabled = false;

            button11.Enabled = true;
            button4.Enabled = true;

            //label2.Enabled = true;
            label3.Enabled = true; 
            label4.Enabled = true;
            label6.Enabled = true;
            label7.Enabled = true;
            label8.Enabled = true;
            label9.Enabled = true;
            label10.Enabled = true;
            label12.Enabled = true;
            label13.Enabled = true;
            label14.Enabled = true;
            label15.Enabled = true;


            conexion.Open();

            var cmd = new SqlCommand("SELECT max(idPresupuesto) FROM presupuesto", conexion);

            var da = new SqlDataAdapter(cmd);

            da.Fill(dt);

            conexion.Close();

            int padre = (int)dt.Rows[0][0];

            padre++;

            textBox1.Text = padre.ToString();
        }
       

        private void button2_Click(object sender, EventArgs e)
        {
            int contador = 0;

            SqlCommand alta = new SqlCommand("insert into presupuesto values (@fEmisionPresupuesto, @fVencimientoPresupuesto, @tipoPresupuesto, @ivaPresupuesto, @subtotalPresupuesto, @totalPresupuesto, @afipPresupuesto, @cantDias, @descPorcent, @descPrecio, @idCliente, @id_usuario, @idTipoPago)", conexion);

            adaptador.InsertCommand = alta;
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@fEmisionPresupuesto", SqlDbType.DateTime));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@fVencimientoPresupuesto", SqlDbType.DateTime));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@tipoPresupuesto", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@ivaPresupuesto", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@subtotalPresupuesto", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@totalPresupuesto", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@afipPresupuesto", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@cantDias", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTipoPago", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@descPorcent", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@descPrecio", SqlDbType.Float));


            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idCliente", SqlDbType.Int));
            //adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTipoFactura", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@id_usuario", SqlDbType.Int));

            adaptador.InsertCommand.Parameters["@fEmisionPresupuesto"].Value = dateTimePicker1.Value;
            adaptador.InsertCommand.Parameters["@fVencimientoPresupuesto"].Value = dateTimePicker2.Value;
            adaptador.InsertCommand.Parameters["@tipoPresupuesto"].Value = "NULL";
            adaptador.InsertCommand.Parameters["@ivaPresupuesto"].Value = 0;
            adaptador.InsertCommand.Parameters["@subtotalPresupuesto"].Value = 0;
            adaptador.InsertCommand.Parameters["@totalPresupuesto"].Value = float.Parse(textBox9.Text);
            adaptador.InsertCommand.Parameters["@afipPresupuesto"].Value = textBox15.Text;
            adaptador.InsertCommand.Parameters["@cantDias"].Value = label2.Text;
            adaptador.InsertCommand.Parameters["@idTipoPago"].Value = tipoDePago;
            if (textBox2.Text != "")
            {
                adaptador.InsertCommand.Parameters["@descPorcent"].Value = float.Parse(textBox2.Text);
            }
            else
            {
                adaptador.InsertCommand.Parameters["@descPorcent"].Value = 0f;
            }

            float calculo = float.Parse(elTotalGlobal) - float.Parse(textBox9.Text);
            adaptador.InsertCommand.Parameters["@descPrecio"].Value = calculo;

            adaptador.InsertCommand.Parameters["@idCliente"].Value = int.Parse(textBox16.Text);
            //adaptador.InsertCommand.Parameters["@idTipoFactura"].Value = int.Parse(textBox17.Text);
            adaptador.InsertCommand.Parameters["@id_usuario"].Value = int.Parse(textBox18.Text);

            try
            {
                conexion.Open();
                adaptador.InsertCommand.ExecuteNonQuery();
                MessageBox.Show("Se ha registrado exitosamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }


            SqlCommand alta2 = new SqlCommand("insert into detalle_presupuesto values (@id_DetallePresupuesto, @nombre, @cantidad, @precio, @acumulado, @descuento, @pdescuento)", conexion);

            adaptador.InsertCommand = alta2;
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@id_DetallePresupuesto", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@cantidad", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@precio", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@acumulado", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@descuento", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@pdescuento", SqlDbType.Float));

         

                try
                {
                    conexion.Open();

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                   // int codigo = (int)row.Cells["id"].Value;
                  
                   
                    //String nombre = row.Cells["Articulo"].Value.ToString();
                    //int cantidad = (int)row.Cells["cantidad"].Value;
                    //float precio = (float)row.Cells["Precio"].Value;
                    float acumulado =0;
                    
                   
                    float pdescuento = 0f;

                    pdescuento = acumulado * (1 - (descuento / 100));

                    alta2.Parameters.Clear();

                    alta2.Parameters.AddWithValue("@id_DetallePresupuesto", Convert.ToInt32(textBox1.Text));
                    alta2.Parameters.AddWithValue("@nombre", Convert.ToString(row.Cells["articulo"].Value));
                    alta2.Parameters.AddWithValue("@cantidad", Convert.ToInt32(row.Cells["cantidad"].Value));
                    alta2.Parameters.AddWithValue("@precio", Convert.ToDecimal(row.Cells["precio"].Value));
                    alta2.Parameters.AddWithValue("@acumulado", Convert.ToDecimal(row.Cells["acumulado"].Value));
                    if (textBox2.Text == "")
                    {
                        descuento = 0;
                    }
                    
                    alta2.Parameters.AddWithValue("@descuento", Convert.ToDecimal(descuento));
                    alta2.Parameters.AddWithValue("@pdescuento", Convert.ToDecimal(pdescuento));

                    alta2.ExecuteNonQuery();




                    //adaptador.InsertCommand.Parameters["@descuento"].Value = textBox2.Text;
                    //adaptador.InsertCommand.Parameters["@pdescuento"].Value = pdescuento;


                    // adaptador.InsertCommand.ExecuteNonQuery();


                    //adaptador.InsertCommand.Parameters["@nombre"].Value = nombre;
                    //adaptador.InsertCommand.Parameters["@cantidad"].Value = cantidad;
                    //adaptador.InsertCommand.Parameters["@precio"].Value = precio;
                    //adaptador.InsertCommand.Parameters["@acumulado"].Value = acumulado;





                    //  SqlCommand modificacion = new SqlCommand("update articulo set cantidad='"+cantidad+"' where id_articulo ='"+codigo+"'", conexion);

                    //adaptador.UpdateCommand = modificacion;
                    //adaptador.UpdateCommand.Parameters.Add(new SqlParameter("cantidad", SqlDbType.Int));

                    laBandera = 1;

                }

                  
                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                    laBandera = 0;
                }
                finally
                {
                    conexion.Close();
                }


            /*

            if (checkBox1.Checked & checkBox2.Checked)
            {
                
            }
            else
           if (checkBox1.Checked)
            {
                SqlCommand alta10 = new SqlCommand("insert into tabla_efectivo values (@importeEfectivo)", conexion);

                adaptador.InsertCommand = alta10;
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@importeEfectivo", SqlDbType.Float));


                adaptador.InsertCommand.Parameters["@importeEfectivo"].Value = float.Parse(textBox10.Text);

                try
                {
                    conexion.Open();
                    adaptador.InsertCommand.ExecuteNonQuery();
                    //MessageBox.Show("Se ha registrado exitosamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }
                finally
                {
                    conexion.Close();
                }


                SqlCommand alta11 = new SqlCommand("insert into detalle_tipo_pago values (@idTipoPago, @idTarjeta, @idEfectivo)", conexion);

                adaptador.InsertCommand = alta11;
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTipoPago", SqlDbType.Int));
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idTarjeta", SqlDbType.Int));
                adaptador.InsertCommand.Parameters.Add(new SqlParameter("@idEfectivo", SqlDbType.Int));


                adaptador.InsertCommand.Parameters["@idTipoPago"].Value = tipoDePago;
                adaptador.InsertCommand.Parameters["@idTarjeta"].Value = 0;
               // adaptador.InsertCommand.Parameters["@idEfectivo"].Value = ;

                try
                {
                    conexion.Open();
                    adaptador.InsertCommand.ExecuteNonQuery();
                    //MessageBox.Show("Se ha registrado exitosamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch (SqlException excepcion)
                {
                    MessageBox.Show(excepcion.ToString());
                }
                finally
                {
                    conexion.Close();
                }
            }
            else
            {
                
            }

            */


            if (laBandera == 1)
            {
                
                DialogResult cTexto;
                cTexto = MessageBox.Show("Desea imprimir presupuesto?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);

                if (cTexto == DialogResult.Yes)
                {
                    string variable = textBox1.Text;
                    idDePresupuesto = int.Parse(variable);

                    Form formuPres = new Form8Presupuesto();
                    formuPres.Show();
                }
                //button6.Enabled = true;
            }

            //Borro los TextBox
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            //textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            textBox11.Text = "";
            //textBox12.Text = "";

            //Desactivo los CheckBox
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            //checkBox3.Checked = false;

            //Desabilito todo
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            //textBox4.Enabled = false;
            textBox5.Enabled = false;
            textBox6.Enabled = false;
            textBox7.Enabled = false;
            textBox9.Enabled = false;

            dataGridView1.Enabled = false;
            //dataGridView2.Enabled = false;

            button1.Enabled = false;
            button3.Enabled = false;
            //button4.Enabled = false;
            //button6.Enabled = false;
            button7.Enabled = false;

            //label2.Enabled = false;
            label3.Enabled = false;
            label4.Enabled = false;
            label6.Enabled = false;
            label7.Enabled = false;
            label8.Enabled = false;
            label9.Enabled = false;
            label10.Enabled = false;
            label12.Enabled = false;


            //NUEVO-RESETEO

            label10.Text = "$0.00";
            dataGridView1.Columns.Clear();
            textBox8.Text="";
            textBox13.Text = "";
            textBox14.Text = "";
            textBox16.Text = "";
            textBox17.Text = "";
            textBox18.Text = "";
            button11.Enabled = false;
            button12.Enabled = false;
            button2.Enabled = false;
            elTotalGlobal = "";



        //this.Dispose();

        //MenuVenta.AbrirFormEnPanel(new Presupuesto());

    }

    private void button8_Click(object sender, EventArgs e)
        {
            if(checkBox1.Enabled == true)
            {
                textBox10.Text = textBox9.Text;
            }
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (checkBox1.Enabled == true)
            {
                textBox11.Text = textBox9.Text;
            }
            
        }

        private void button10_Click(object sender, EventArgs e)
        {/*
            if (checkBox1.Enabled == true)
            {
                textBox12.Text = textBox9.Text;
            }
            */
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ConsultarClienteFactura c = new ConsultarClienteFactura();
            c.pasado += new ConsultarClienteFactura.pasar(ejecutar);
            c.Show();
        }

        public void ejecutar(string dato, string dato2, string dato3, string dato4, string dato5)
        {
            //nombre
            textBox13.Text = dato3;
            //cuit
            textBox8.Text = dato2;
            //tipoFactura
            textBox14.Text = dato4;

            //id
            textBox16.Text = dato;

            //idTipoFactura
            textBox17.Text = dato5;


        }


        private void textBox13_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            ConsultarArticuloFactura cArticulo = new ConsultarArticuloFactura();
            cArticulo.pasado2 += new ConsultarArticuloFactura.pasar2(ejecutar2);
            cArticulo.Show();

            textBox5.Enabled = true;
            textBox5.BackColor = Color.IndianRed;
            label3.ForeColor = Color.IndianRed;

        }

        public void ejecutar2(string dato, string dato2, string dato3)
        {
            //id
            label11.Text = dato;
            //Nombre
            textBox3.Text = dato2;
            //Precio
            textBox6.Text = dato3;

            /*
            //id
            textBox16.Text = dato;

            //idTipoFactura
            textBox17.Text = dato5;
            */
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            AgregarDescuento aDescuento = new AgregarDescuento();
            aDescuento.pasado3 += new AgregarDescuento.pasar3(ejecutar3);
            aDescuento.Show();

            /*
            Form agregarD = new AgregarDescuento();
            agregarD.Show();
            */
        }

        public void ejecutar3(string dato, string dato2)
        {
            //porcentaje
            textBox2.Text = dato;
            //total
            textBox9.Text = dato2;
            //total
            label10.Text = dato2;
            //elTotalGlobal = "";
        }

        private void textBox5_MouseDown(object sender, MouseEventArgs e)
        {
            textBox5.BackColor = Color.White;
            label3.ForeColor = Color.White;
        }

        private void calcularDias()
        {
            DateTime primeraFecha = dateTimePicker1.Value;

            DateTime segundaFecha = dateTimePicker2.Value;

            int numerodias = (segundaFecha - primeraFecha).Days;
            label2.Text = numerodias.ToString();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            calcularDias();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            calcularDias();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {       
            string variable = textBox1.Text;
            idDePresupuesto = int.Parse(variable);

            Form formuPres = new Form8Presupuesto();
            formuPres.Show();
           
        }
    }
    
}

