﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class ListadoCliente : Form
    {
        public ListadoCliente()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private SqlDataAdapter adaptador2;
        private SqlDataAdapter adaptador3;
        private DataTable datos;
        private DataTable datos2;
        private DataTable datos3;

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void ListadoCliente_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.cliente' Puede moverla o quitarla según sea necesario.
            this.clienteTableAdapter.Fill(this.baseDeDatosDataSet10.cliente);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.tipoFactura' Puede moverla o quitarla según sea necesario.
            this.tipoFacturaTableAdapter.Fill(this.baseDeDatosDataSet10.tipoFactura);
            // TODO: esta línea de código carga datos en la tabla 'baseDeDatosDataSet10.localidad' Puede moverla o quitarla según sea necesario.
            this.localidadTableAdapter.Fill(this.baseDeDatosDataSet10.localidad);
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();

        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador2 = new SqlDataAdapter();
                adaptador2.SelectCommand = new SqlCommand("listadoPorLocalidad", conexion);
                adaptador2.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador2.SelectCommand.Parameters.Add("@idLocalidad", SqlDbType.Int);

                datos2 = new DataTable();
                adaptador2.SelectCommand.Parameters["@idLocalidad"].Value = comboBox1.SelectedValue;
                adaptador2.Fill(datos2);
                dataGridView1.DataSource = datos2;

                DataGridViewColumn column = dataGridView1.Columns[0];
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column1 = dataGridView1.Columns[1];
                column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column2 = dataGridView1.Columns[2];
                column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column3 = dataGridView1.Columns[3];
                column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column4 = dataGridView1.Columns[4];
                column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column5 = dataGridView1.Columns[5];
                column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column7 = dataGridView1.Columns[7];
                column7.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;


            }

            if (radioButton2.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("listadoPorPrueba", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoPr", SqlDbType.Int);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoPr"].Value = comboBox2.SelectedValue;
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;
            }

            if (radioButton3.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador3 = new SqlDataAdapter();
                adaptador3.SelectCommand = new SqlCommand("listadoPorNombreCliente", conexion);
                adaptador3.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador3.SelectCommand.Parameters.Add("@idNombreCliente", SqlDbType.VarChar);

                datos3 = new DataTable();
                adaptador3.SelectCommand.Parameters["@idNombreCliente"].Value = textBox1.Text;
                adaptador3.Fill(datos3);
                dataGridView1.DataSource = datos3;
            }
        }


        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            string fecha = dateTimePicker1.Value.ToString("dd/MM/yyyy");

            Bitmap obj = new Bitmap(this.dataGridView1.Width, this.dataGridView1.Height);
            dataGridView1.DrawToBitmap(obj, new Rectangle(0, 0, this.dataGridView1.Width, this.dataGridView1.Height));

            //1069; 382 this.dataGridView1.Width, this.dataGridView1.Height

            e.Graphics.DrawImage(obj, 100, 100);

            Font font = new Font("Century Gothic", 16);
            e.Graphics.DrawString("Listado de Clientes",font, Brushes.Black, 500, 20);
            e.Graphics.DrawString("Fecha: "+fecha, font, Brushes.Black, 950, 20);

            //printDocument1.DefaultPageSettings.Landscape = true;
            
            Image newImage = Image.FromFile("\\SoftCars_copia\\IMAGENES\\Logo2.jpg");
            PointF ulCorner = new PointF(10, 12);
            e.Graphics.DrawImage(newImage, ulCorner);        
            
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            //PrintPreviewDialog.ActiveForm
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.Height = 800;
            printPreviewDialog1.Width = 900;
            printDocument1.DefaultPageSettings.Landscape = true;

            //printPreviewDialog1;
            printPreviewDialog1.ShowDialog();
            
        }
    }
}
