﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace SoftCars
{
    public partial class ConsultarPresupuesto : Form
    {
        public ConsultarPresupuesto()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private SqlDataAdapter adaptador2;
        private SqlDataAdapter adaptador3;
        private DataTable datos;
        private DataTable datos2;
        private DataTable datos3;
        public static int idDePresupuesto;

        private void ConsultarPresupuesto_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection(ConexionDB.conexiondb);
            adaptador = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (radioButton4.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("consultaDePresupuesto4", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoDePresupuesto4", SqlDbType.Int);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoDePresupuesto4"].Value = int.Parse(textBox2.Text);
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;

                DataGridViewColumn column = dataGridView1.Columns[0];
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column1 = dataGridView1.Columns[1];
                column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column2 = dataGridView1.Columns[2];
                column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column5 = dataGridView1.Columns[5];
                //column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column6 = dataGridView1.Columns[6];
                column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }

            if (radioButton3.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("consultaDePresupuesto", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoDePresupuesto", SqlDbType.VarChar);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoDePresupuesto"].Value = textBox1.Text;
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;

                DataGridViewColumn column = dataGridView1.Columns[0];
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column1 = dataGridView1.Columns[1];
                column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column2 = dataGridView1.Columns[2];
                //column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column5 = dataGridView1.Columns[5];
                column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column6 = dataGridView1.Columns[6];
                column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }

            if (radioButton2.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("consultaDePresupuesto2", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoDePresupuesto2", SqlDbType.DateTime);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoDePresupuesto2"].Value = dateTimePicker1.Value;
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;

                DataGridViewColumn column = dataGridView1.Columns[0];
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column1 = dataGridView1.Columns[1];
                //column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column2 = dataGridView1.Columns[2];
                column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column5 = dataGridView1.Columns[5];
                column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column6 = dataGridView1.Columns[6];
                column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }

            if (radioButton1.Checked)
            {
                dataGridView1.Columns.Clear();

                adaptador = new SqlDataAdapter();
                adaptador.SelectCommand = new SqlCommand("consultaDePresupuesto3", conexion);
                adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
                adaptador.SelectCommand.Parameters.Add("@listadoDePresupuesto3", SqlDbType.DateTime);

                datos = new DataTable();
                adaptador.SelectCommand.Parameters["@listadoDePresupuesto3"].Value = dateTimePicker2.Value;
                adaptador.Fill(datos);
                dataGridView1.DataSource = datos;

                DataGridViewColumn column = dataGridView1.Columns[0];
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column1 = dataGridView1.Columns[1];
                //column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column2 = dataGridView1.Columns[2];
                column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column3 = dataGridView1.Columns[3];
                //column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DataGridViewColumn column4 = dataGridView1.Columns[4];
                //column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column5 = dataGridView1.Columns[5];
                column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                DataGridViewColumn column6 = dataGridView1.Columns[6];
                column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string variable;

            if (radioButton4.Checked)
            {
                variable = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                idDePresupuesto = int.Parse(variable);
            }
            else
            {
                variable = dataGridView1.CurrentRow.Cells[6].Value.ToString();
                idDePresupuesto = int.Parse(variable);
            }

            Form formuPres = new Form8();
            formuPres.Show();
        }
    }
}
