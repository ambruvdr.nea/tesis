﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SoftCars
{
    public partial class notaDebito : Form
    {
        public notaDebito()
        {
            InitializeComponent();
        }

        private SqlConnection conexion;
        private SqlDataAdapter adaptador;
        private DataSet datos;
        //int variable;
        private DataTable datosTable;
        //String valorRadio;
        //DataGridView dgv = new DataGridView();
        int bandera = 0;
        float total = 0f;
        float descuento = 0f;
        DataTable dt = new DataTable();


        private void notaDebito_Load(object sender, EventArgs e)
        {
            conexion = new SqlConnection("Data Source=DESKTOP-9P5R5FD\\sqlexpress;Initial Catalog=BaseDeDatos;Integrated Security=True");
            adaptador = new SqlDataAdapter();
            dataGridView1.AllowUserToAddRows = false;
            dataGridView2.AllowUserToAddRows = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            adaptador.SelectCommand = new SqlCommand("ListaArticulo2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art2", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art2"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView2.DataSource = datosTable;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox15.Enabled = true;
            textBox8.Enabled = true;
            textBox13.Enabled = true;
            textBox14.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            textBox4.Enabled = true;
            textBox6.Enabled = true;
            textBox7.Enabled = true;
            textBox9.Enabled = true;
            textBox10.Enabled = true;
            textBox12.Enabled = true;

            dataGridView1.Enabled = true;
            dataGridView2.Enabled = true;

            button12.Enabled = true;
            button4.Enabled = true;
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            button6.Enabled = true;
            button10.Enabled = true;
            button8.Enabled = true;

            label2.Enabled = true;
            label3.Enabled = true;
            label4.Enabled = true;
            label6.Enabled = true;
            label7.Enabled = true;
            label8.Enabled = true;
            label9.Enabled = true;
            label10.Enabled = true;
            label12.Enabled = true;
            label13.Enabled = true;
            label14.Enabled = true;
            label16.Enabled = true;
            label17.Enabled = true;

            checkBox1.Enabled = true;
            checkBox3.Enabled = true;




            conexion.Open();

            var cmd = new SqlCommand("SELECT max(id_ndebito) FROM ndebito", conexion);

            var da = new SqlDataAdapter(cmd);

            da.Fill(dt);

            conexion.Close();

            int padre = (int)dt.Rows[0][0];

            padre++;

            textBox1.Text = padre.ToString();

            conexion.Close();
        }

        private void textBox4_KeyUp(object sender, KeyEventArgs e)
        {
            adaptador.SelectCommand = new SqlCommand("ListaArticulo2", conexion);
            adaptador.SelectCommand.CommandType = CommandType.StoredProcedure;
            adaptador.SelectCommand.Parameters.Add("@art2", SqlDbType.VarChar);

            datosTable = new DataTable();
            adaptador.SelectCommand.Parameters["@art2"].Value = textBox4.Text;
            adaptador.Fill(datosTable);
            dataGridView2.DataSource = datosTable;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textBox10.Enabled = true;
                button8.Enabled = true;
            }
            else
            {
                textBox10.Enabled = false;
                button8.Enabled = false;
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                textBox12.Enabled = true;
                button10.Enabled = true;
            }
            else
            {
                textBox12.Enabled = false;
                button10.Enabled = false;
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (textBox3.Text != "")
            {
                textBox5.Enabled = true;
            }
            textBox5.Enabled = true;
            if (dataGridView2.CurrentRow.Index != -1)
            {
                textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();
                textBox6.Text = dataGridView2.CurrentRow.Cells[2].Value.ToString();
                label11.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (textBox3.Text != "")
            {
                textBox5.Enabled = true;
            }
            if (dataGridView2.CurrentRow.Index != -1)
            {
                textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();
                textBox6.Text = dataGridView2.CurrentRow.Cells[2].Value.ToString();
                label11.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            }
        }

        private void textBox5_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox5.Text != "")
            {

                int cantidad = 0;
                float precio = float.Parse(textBox6.Text);
                float acumulado;



                cantidad = int.Parse(textBox5.Text);

                acumulado = cantidad * precio;


                textBox7.Text = acumulado.ToString();
            }
        }

        private void actualizarDatos()
        {
            datos.Clear();
            adaptador.Fill(datos, "articulo");
        }

        private void actualizarDatos2()
        {
            datos.Clear();
            adaptador.Fill(datos);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = int.Parse(label11.Text);
            String articulo = textBox3.Text;
            int cantidad = int.Parse(textBox5.Text);
            float precio = float.Parse(textBox6.Text);
            float acumulado = float.Parse(textBox7.Text);

            if (bandera < 1)
            {
                dataGridView1.Columns.Add("id", "Codigo");
                dataGridView1.Columns.Add("articulo", "Articulo");
                dataGridView1.Columns.Add("cantidad", "Cantidad");
                dataGridView1.Columns.Add("precio", "Precio");
                dataGridView1.Columns.Add("acumulado", "Acumuldado");

                bandera++;
            }

            dataGridView1.Rows.Add(id, articulo, cantidad, precio, acumulado);


            total = total + acumulado;


            textBox9.Text = total.ToString();
            label10.Text = total.ToString();


            textBox3.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            label11.Text = "";
            textBox5.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            descuento = float.Parse(textBox2.Text);
            if (descuento > 0)
            {
                float porcentaje = 1 - (descuento / 100);
                total = total * porcentaje;
            }


            textBox2.Text = descuento.ToString();
            textBox9.Text = total.ToString();
            label10.Text = total.ToString();
            descuento = 0f;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            float saldo = float.Parse(textBox9.Text);


            label10.Text = saldo.ToString();

            saldo = float.Parse(label10.Text);

            if (checkBox1.Checked == true)
            {
                saldo = saldo - float.Parse(textBox10.Text);

                label10.Text = saldo.ToString();

            }

            if (checkBox3.Checked == true)
            {
                saldo = saldo - float.Parse(textBox12.Text);

                label10.Text = saldo.ToString();

            }

            saldo = float.Parse(textBox9.Text);

            if (int.Parse(label10.Text) == 0)
            {
                button2.Enabled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int contador = 0;

            SqlCommand alta = new SqlCommand("insert into ndebito values (@fecha)", conexion);

            adaptador.InsertCommand = alta;
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@fecha", SqlDbType.DateTime));

            adaptador.InsertCommand.Parameters["@fecha"].Value = dateTimePicker1.Value;

            try
            {
                conexion.Open();
                adaptador.InsertCommand.ExecuteNonQuery();
            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }





            SqlCommand alta2 = new SqlCommand("insert into detalle_ndebito values (@id_ndebito, @nro_ndebito, @nombre, @cantidad, @precio, @acumulado, @descuento, @pdescuento)", conexion);

            adaptador.InsertCommand = alta;
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@id_ndebito", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nro_ndebito", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@cantidad", SqlDbType.Int));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@precio", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@acumulado", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@descuento", SqlDbType.Float));
            adaptador.InsertCommand.Parameters.Add(new SqlParameter("@pdescuento", SqlDbType.Float));



            try
            {
                conexion.Open();

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    // int codigo = (int)row.Cells["id"].Value;


                    //String nombre = row.Cells["Articulo"].Value.ToString();
                    //int cantidad = (int)row.Cells["cantidad"].Value;
                    //float precio = (float)row.Cells["Precio"].Value;
                    float acumulado = 0;


                    float pdescuento = 0f;

                    pdescuento = acumulado * (1 - (descuento / 100));

                    alta2.Parameters.Clear();

                    alta2.Parameters.AddWithValue("@id_ndebito", Convert.ToInt32(textBox1.Text));
                    alta2.Parameters.AddWithValue("@nro_ndebito", Convert.ToString(textBox15.Text));
                    alta2.Parameters.AddWithValue("@nombre", Convert.ToString(row.Cells["articulo"].Value));
                    alta2.Parameters.AddWithValue("@cantidad", Convert.ToInt32(row.Cells["cantidad"].Value));
                    alta2.Parameters.AddWithValue("@precio", Convert.ToDecimal(row.Cells["precio"].Value));
                    alta2.Parameters.AddWithValue("@acumulado", Convert.ToDecimal(row.Cells["acumulado"].Value));
                    if (textBox2.Text == "")
                    {
                        descuento = 0;
                    }

                    alta2.Parameters.AddWithValue("@descuento", Convert.ToDecimal(descuento));
                    alta2.Parameters.AddWithValue("@pdescuento", Convert.ToDecimal(pdescuento));

                    alta2.ExecuteNonQuery();




                    //adaptador.InsertCommand.Parameters["@descuento"].Value = textBox2.Text;
                    //adaptador.InsertCommand.Parameters["@pdescuento"].Value = pdescuento;


                    // adaptador.InsertCommand.ExecuteNonQuery();


                    //adaptador.InsertCommand.Parameters["@nombre"].Value = nombre;
                    //adaptador.InsertCommand.Parameters["@cantidad"].Value = cantidad;
                    //adaptador.InsertCommand.Parameters["@precio"].Value = precio;
                    //adaptador.InsertCommand.Parameters["@acumulado"].Value = acumulado;





                    //  SqlCommand modificacion = new SqlCommand("update articulo set cantidad='"+cantidad+"' where id_articulo ='"+codigo+"'", conexion);

                    //adaptador.UpdateCommand = modificacion;
                    //adaptador.UpdateCommand.Parameters.Add(new SqlParameter("cantidad", SqlDbType.Int));

                }


            }
            catch (SqlException excepcion)
            {
                MessageBox.Show(excepcion.ToString());
            }
            finally
            {
                conexion.Close();
            }
            //Borro los TextBox
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            textBox12.Text = "";

            //Desactivo los CheckBox
            checkBox1.Checked = false;
            checkBox3.Checked = false;

            //Desabilito todo
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            textBox4.Enabled = false;
            textBox5.Enabled = false;
            textBox6.Enabled = false;
            textBox7.Enabled = false;
            textBox9.Enabled = false;

            dataGridView1.Enabled = false;
            dataGridView2.Enabled = false;

            button1.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = false;

            label2.Enabled = false;
            label3.Enabled = false;
            label4.Enabled = false;
            label6.Enabled = false;
            label7.Enabled = false;
            label8.Enabled = false;
            label9.Enabled = false;
            label10.Enabled = false;
            label12.Enabled = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (checkBox1.Enabled == true)
            {
                textBox10.Text = textBox9.Text;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (checkBox1.Enabled == true)
            {
                textBox12.Text = textBox9.Text;
            }
        }
    }
}
